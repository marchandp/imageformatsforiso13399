//--------------------------------------------------------------------------//
/*              plib filter
 *       realized by POTIER Jean-Claude
 * Copyright (c) 1999 CRCFAO. All Rights Reserved.
 * Permission to use, copy, modify, and distribute this software
 * and its documentation without prior written permission
 * from LISI/ENSMA is not allowed.
 *
 * T�l�port 2, 1 avenue Cl�ment Ader , BP40109
 *        86960 FUTUROSCOPE
 *
 */

//--------------------------------------------------------------------------//

function Filter() {
  this.selectors = new Selectors();
  this.allowed_properties = new Object ();
  this.basket = new Basket();
  this.dictionary = null ;
  this.content = null ;
  this.selections = null ;
  this.is_loaded = false ;
  this.win = null;
  this.available_basket=false;
  this.display_content = 0;
}
//--------------------------------------------------------------------------//

 Filter.prototype.reset = reset_filter;
function reset_filter(){
   if (this.content.table){
        var table = this.content.table ;
		var columns = table.rows[0];
    	for (var i=0;i<columns.length;i++){
     	var code = columns[i].code ;
          selectors[code].widget_selected_value_update();
          selectors[code].widget_operators_update();
    	}
     this.content.reset();
     this.selectors.update_count(table.count());
 }

}
//--------------------------------------------------------------------------//
Filter.prototype.update = update_filter;
function update_filter(all){
     if ((all) && (this.content)) {
  		var columns = this.content.table.rows[0];
	    	for (var i=0;i<columns.length;i++){
     		var code = columns[i].code ;
          	selectors[code].widget_selected_value_update();
               selectors[code].widget_operators_update();
    		}

	}
}

//--------------------------------------------------------------------------//
Filter.prototype.display = display_filter;
function display_filter(){

   var selectors = this.selectors;
   if ((selectors)&&(this.content)){
        var table = this.content.table;
   		var columns = table.rows[0];
          selectors.update_count(table.count());
    		for (var i=0;i<columns.length;i++){
        		var code = columns[i].code ;
               if (selectors[code].wlist){
              	 	var indexs = this.content.indexs(code);
        			var values = table.valuesByIndexs(indexs);
        			selectors[code].widget_values_update(values);
               }
    		}
          var selections = this.selections;
     	if (selections) {
          	for (var j=0;j<selections.length();j++){
                  var selection = selections.value(j);
                  var code = selection.code;
                  var value = selection.value;
                  for (prop in this.allowed_properties)
					{
					  if (code == prop)
						{
						  selectors[code].widget_selected_value_update(value);
						  var operator = selection.operator;
		                  selectors[code].widget_operators_update(operator);
						}	
					}
                  
			}
		}
    }

 }

//--------------------------------------------------------------------------//
Filter.prototype.erase = erase_filter;
function erase_filter(frame){
   
   if (this.is_loaded){
   	frame.document.clear();
   	frame.document.open();
   	frame.document.writeln("<HTML>");
   	frame.document.writeln("<HEAD>");
   	frame.document.writeln( '<LINK rel="stylesheet" type="text/css" href="filter.css">' );
   	frame.document.writeln( '<SCRIPT src="../runtime/parameters.js"></SCRIPT>');
    frame.document.writeln( '<SCRIPT src="../runtime/cookies.js"></SCRIPT>');
    frame.document.writeln( '<SCRIPT src="../runtime/translate.js"></SCRIPT>');
    frame.document.writeln( '<SCRIPT src="../lgdisp.js"></SCRIPT>');
	frame.document.writeln( '<SCRIPT src="./utilities.js"></SCRIPT>');
   	frame.document.writeln("</HEAD>");
    frame.document.writeln("<BODY leftmargin='0' marginwidth='0'>");
  // 	frame.document.writeln("<BODY  background="+'"'+bkgdimg+'"'+" leftmargin='0' marginwidth='0'>");

     frame.document.writeln("<TABLE CLASS='filter' ALIGN='center'>");
     frame.document.writeln(" <TR>");
     frame.document.writeln(" <TD>");
     frame.document.writeln("<TABLE CLASS='propsel' ALIGN='center'>");
     frame.document.writeln(" <TR>");
     frame.document.writeln("<TH align='center' width='260' nowrap>");
     frame.document.writeln("<DIV CLASS='counter' ID='cnt'>");
     frame.document.writeln("<B> "+ languages.keywords.count_instances+ "</B>: " + languages.keywords.no_instances_availables );
     frame.document.writeln("</DIV>");

     frame.document.writeln("</TH>");
     frame.document.writeln(" </TR>");
     frame.document.writeln("</TABLE>");
     frame.document.writeln(" </TD>");
     frame.document.writeln(" </TR>");
     frame.document.writeln("</TABLE>");

   	frame.document.writeln("</BODY>");
   	frame.document.writeln("</HTML>");
   	frame.document.close();
     this.is_loaded = false ;
   }
}

//--------------------------------------------------------------------------//
Filter.prototype.load =load_filter;
function load_filter(frame,parent){
   if (this.is_loaded) this.erase(frame);
  // frame.document.open();
   frame.document.writeln("<HTML>");
   frame.document.writeln("<HEAD>");
   frame.document.writeln( '<LINK rel="stylesheet" type="text/css" href="filter.css">' );
//	frame.document.writeln( '<SCRIPT src="../runtime/parameters.js"></SCRIPT>');
//   frame.document.writeln( '<SCRIPT src="../runtime/cookies.js"></SCRIPT>');
//    frame.document.writeln( '<SCRIPT src="../runtime/translate.js"></SCRIPT>');
//    frame.document.writeln( '<SCRIPT src="../lgdisp.js"></SCRIPT>');
//	frame.document.writeln( '<SCRIPT src="./utilities.js"></SCRIPT>');
   frame.document.writeln("</HEAD>");
   frame.document.writeln("<BODY leftmargin='0' marginwidth='0'>");

  // frame.document.writeln("<BODY  background="+'"'+bkgdimg+'"'+" leftmargin='0' marginwidth='0'>");

   frame.document.writeln("<TABLE CLASS='filter' width='90%' ALIGN='center'>");
   if (this.content) {
      this.allowed_properties = new Object ();
  	  var allowed_properties = this.allowed_properties;
  	  var operators = ['=','<>','>','<=','<','>='] ;
   	  this.selectors.window(frame,parent);
      if (this.content.table){
       	var table = this.content.table ;
        frame.document.writeln(" <TR>");
        frame.document.writeln(" <TD>");
        frame.document.writeln("<TABLE CLASS='propsel' ALIGN='center'>");
        this.selectors.define_count(table.count());
     	var columns = table.rows[0];
  	    for (var i=0; i < columns.length; i++){
        	var code = columns[i].code;
         	if (!allowed_properties[code]){
         		var pfname = columns[i].labels;
          		this.selectors[code]= new Selector(code,pfname,operators);
        		allowed_properties[code]= true;
        	}
      	}
		if (columns.length>0){
		  frame.document.writeln(" <TR>");
		  frame.document.writeln(" <TD colspan='5'>");
	      frame.document.writeln("<TABLE CLASS='command' ALIGN='center'>");
		  frame.document.writeln(" <TR>");
		  frame.document.writeln(" <TD>");
		  frame.document.writeln(" <INPUT type='button' class='button' value='" + languages.keywords.search + "' onclick='Javascript:top.filter.apply_constraints()'>");
		  frame.document.writeln(" </TD>");
		  frame.document.writeln(" <TD>");
		  frame.document.writeln(" <INPUT type='button' class='button' value='" + languages.keywords.reset + "' onclick='Javascript:top.filter.reset_constraints()' ondblclick='Javascript:top.filter.abort_constraints()'>");
		  frame.document.writeln(" </TD>");
		  frame.document.writeln(" </TR>");
		  frame.document.writeln("</TABLE>");
		  frame.document.writeln(" </TR>");
		}
      	frame.document.writeln("</TABLE>");
      	frame.document.writeln(" </TD>");
      	frame.document.writeln(" </TR>");
      }
   }
   frame.document.writeln("</TABLE>");
   frame.document.writeln("</BODY>");
   frame.document.writeln("</HTML>");
   frame.document.close();
   this.is_loaded=true; 
}

//--------------------------------------------------------------------------//

Filter.prototype.activate = Activate_filter;
function Activate_filter() {
    this.display();

	this.content.update(top.filter.selections,true);
	
	for (i=0;i<this.selections.liste.length;i++)
	{
   		this.content.display(this.selections,top.filter.selections.liste[i].code);
	}
	//Mise � jour du compteur
	this.selectors.update_count(this.content.table.count()); 
	var elm = frame.document.getElementById('Zs_71DC0A9B124DC');
}
//--------------------------------------------------------------------------//
Filter.prototype.apply_constraints = apply_constraints ;
function apply_constraints()
{

	 if (this.display_content==0){	 
		 this.display_content = 1;
		 top.text.location=top.text.location;	 
	 }
	this.content.update(top.filter.selections,true);
	for (i=0;i<top.filter.selections.liste.length;i++)
	{
   		this.content.display(this.selections,this.selections.liste[i].code);
	}
	this.selectors.update_count(this.content.table.count());
}

//--------------------------------------------------------------------------//
Filter.prototype.reset_constraints = reset_constraints;
function reset_constraints()
{
     this.display_content = 0;
	 this.reset();
     top.selections.liste=[];
     top.text.location=top.text.location;
}

//--------------------------------------------------------------------------//
Filter.prototype.abort_constraints = abort_constraints;
function abort_constraints()
{
	 this.reset();
     top.selections.liste=[];
}
//--------------------------------------------------------------------------//

Filter.prototype.link= link_components;
function link_components(selections,dictionary,content){
  this.dictionary = (dictionary) ? dictionary : null ;
  this.content = (content) ? content : null ;
  this.selections = (selections) ? selections : null ;
  this.selectors.link(selections,content);
}

//--------------------------------------------------------------------------//
 Filter.prototype.add_basket= add_components;
function add_components(){

	if (this.available_basket){
		this.basket.add(this,this.win);
	} else {
		alert(languages.keywords.unavailable_basket);
	}

}

//--------------------------------------------------------------------------//
 Filter.prototype.order_form= order_form;
function order_form(){
  this.basket.order_form();
}

//--------------------------------------------------------------------------//
Filter.prototype.export_family = DB_Export_family;
function DB_Export_family() {
   if (this.content){
    var file = "../plib-datas/database.p21";
//    alert(" file loaded :"+ file);
    var winFeatures = "width=600,height=500,resizable=yes,menubar=yes,scrollbars=yes";
      this.win=window.open(file,"database",winFeatures);
   } else {
      alert('no selected family ');
   }

}

//--------------------------------------------------------------------------//
Filter.prototype.export_instance = DB_Export_instance ;
function DB_Export_instance() {

  if (this.content){
    var nb_instances = this.content.table.count();
    var file = "./plib-datas/instance.txt";
//    alert(" file loaded :"+ file);
    if (nb_instances>=1){
        var dico = this.dictionary ;
        var cont = this.content;
        this.win = new Instances(dico,cont).open();
     } else {
          alert('only one component must be selected');
    }
  } else {
      alert('no selected component');
  }
}

