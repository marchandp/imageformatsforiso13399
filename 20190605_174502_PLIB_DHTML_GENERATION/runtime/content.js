//--------------------------------------------------------------------------//
/*              plib content
 *       realized by POTIER Jean-Claude
 * Copyright (c) 1999 CRCFAO. All Rights Reserved.
 * Permission to use, copy, modify, and distribute this software
 * and its documentation without prior written permission
 * from LISI/ENSMA is not allowed.
 *
 * T�l�port 2, 1 avenue Cl�ment Ader , BP40109
 *        86960 FUTUROSCOPE
 *
 */
//--------------------------------------------------------------------------//
   var dictionary = null ;
   var content = null ;
   var filter = null ;
   var selections = null ;
   var old_color;
   var SelectableColor = "#FFFFFF"; // guillaume
//   var SelectedColor = "#55FFFF";
   var SelectedColor = "#9999cc";
   var UnSelectableCol = "#999999";
   var WithOutHeader = 0; // 0 avec header ; 1 sans header 

//--------------------------------------------------------------------------//
function select_class_instance( input ){

for (i=0;i<input.length;i++)
	{
		var selection = new Selection(input[i][1],"=",input[i][0],null);
		selections.add(selection);	
	}
	top.filter.selections=selections;
}

//--------------------------------------------------------------------------//
function load_pages(dico_name){
		top.text.location="family.html?family="+dico_name;
}

//--------------------------------------------------------------------------//
function BSU(code,version,element){
 this.code = code;
 this.version = (version) ? version : "001";
 this.element = (element) ? element : null;
}

//--------------------------------------------------------------------------//
Supplier.prototype.bsu = BSU;
function Supplier(code,version){
 this.bsu (code,version,null);
}
//--------------------------------------------------------------------------//
Property.prototype.bsu=BSU;
function Property (code,version,indexs,content,caseofclass,dependencies)
{
  this.bsu(code,version,content);
  this.indexs = indexs ;
  if (content){
  	content.properties[code] = this ;
  }
  this.caseofclass = (caseofclass) ? caseofclass : null;
  this.dependencies = (dependencies) ? dependencies :null;
}
//--------------------------------------------------------------------------//
Property.prototype.code_bsu =Code_bsu;
function Code_bsu ()
{
  var code = this.code;
  var index =  code.indexOf("_nom");
  if ( code.indexOf("_nom") >0){
    code = code.substring(0, code.indexOf("_nom"));
  } else if (code.indexOf("_typ") >0 ){
    code = code.substring(0, code.indexOf("_typ"));
  } else if (code.indexOf("_max") >0 ){
    code = code.substring(0, code.indexOf("_max"));
  } else if (code.indexOf("_min") >0 ){
    code = code.substring(0, code.indexOf("_min"));
  }
  return code ;
}
//--------------------------------------------------------------------------//
function Column(code,labels,css,content,visible ){
// define the header of columns

 this.code = (code)? code : "" ;
 this.labels = (labels) ? labels : null ;
 this.css = (css) ? css : "" ;
 this.visible = (visible) ? visible : true ;
 if (content){
     if (content.table.rows[0]==null){
        content.table.rows[0]=new Array();
	}
     var index = content.table.rows[0].length ;
     content.table.rows[0][index]=this;
 }

}
//--------------------------------------------------------------------------//
Column.prototype.display = Display_Column;

function Display_Column(){
	document.write("<TH id='"+this.code+"' class='"+this.css+"' nowrap> "+ this.labels +"</TH>");
}
//--------------------------------------------------------------------------//
function Instance(values,version,content, defined_class, caseof_classes,
				supplier_id, supplier_des, user_id, user_des, global_id, source_class_of_content) {

  this.selectable = true ;
  this.version = (version) ? version : "001" ;
  this.values = (values) ? values : null ;
  this.defined_class = (defined_class) ? defined_class : null ; // code of defined class
  this.caseof_classes = (caseof_classes) ? caseof_classes : null ; // list of codes of case_of classes
  this.supplier_id = (supplier_id) ?  supplier_id : null ;
  this.supplier_des = (supplier_des) ?  supplier_des : null ;
  this.user_id = (user_id) ?  user_id : null ;
  this.user_des = (user_des) ? user_des : null ;
  this.global_id = (global_id) ? global_id : false ;
  this.source_class_of_content = (source_class_of_content) ?  source_class_of_content : null ;
  if (content){
  	var index = content.table.rows.length ;
  	content.table.rows[index] = this ;
  }
}

//--------------------------------------------------------------------------//
Instance.prototype.display=Display_instance;

function Display_instance(code){

	for (j=0;j<this.values.length ;j++){
		document.write("<TD id=\""+code+"\"  class=\"value\" nowrap>"+this.values[j]+"</TD>");
	}
}

//--------------------------------------------------------------------------//
function Meta_Header(classe,colspan,content) {

  this.classe = classe ;
  this.colspan = colspan;
  if (content){
  	var index = content.meta_headers.length ;
  	content.meta_headers[index] = this ;
  }
}
//--------------------------------------------------------------------------//
Meta_Header.prototype.type=Type;

function Type(){

switch (this.classe) {
	
	case "keyheader": 

	return languages.keywords.key_carateristics;

	break;

	case "charheader": 

	return languages.keywords.carateristics;

	break;

	case "depheader": 

	return languages.keywords.context_carateristics;

	break;

}


}

//--------------------------------------------------------------------------//
Meta_Header.prototype.display=Display_meta_header;

function Display_meta_header(){

	document.write('<TH class=\"'+this.classe+'\" colspan=\"'+this.colspan+'\" nowrap><div class="label" align="center"> ' + this.type() + '</div> </TH>');

}

//--------------------------------------------------------------------------//
function Family_Link (name,link,content){
	this.name=name;
	this.link=link;
	if (content){
  	  var index = content.family_links.length ;
  	  content.family_links[index] = this ;
    }
}
//--------------------------------------------------------------------------//
Family_Link.prototype.display=Display_Link;

function Display_Link(){
	document.write("<TD id='link' class='value' nowrap> <a href='Javascript:load_pages(\""+this.link+"\")'>"+this.name+" </a> </TD>"); 
}
//--------------------------------------------------------------------------//
function Table(element,headers,values,content){
// values is array of instances
   this.rows = new Array();
   this.element = (element) ? element : null ;
   if (headers){
    this.rows[0] = headers;
   }
   if (values){
     for (var i=0; i<values.length;i++){
       this.rows[i+1]= values[i];
     }
   }
   if (content){
      content.table = this ;
   }
 }
 //--------------------------------------------------------------------------//
Table.prototype.valuesByIndex = get_values_by_index;
function get_values_by_index(index){
  var rows = this.rows;
  var values = new Array();
  var allowed_values = new Object();
  for (var i=1;i<rows.length;i++){
     var value =  rows[i].values[index];
     if ((rows[i].selectable)&&
	    (!allowed_values[value])) {
            values[values.length]=value;
            allowed_values[value]= true;
     }
  }
  return values;
}
 //--------------------------------------------------------------------------//
Table.prototype.valuesByIndexs = get_values_by_indexs;
function get_values_by_indexs(indexs){
  var rows = this.rows;
  var values = new Array();
  var allowed_values = new Object();
  for (var i=0; i< indexs.length;i++){
  	for (var j=1;j<rows.length;j++){
     	var value =  rows[j].values[indexs[i]-1];
     	if ((rows[j].selectable) &&
			(!allowed_values[value])){
       		    values[values.length]= value;
                   allowed_values[value]= true;
     	}
      }
  }
  return values;
}



//--------------------------------------------------------------------------//
Table.prototype.reset = reset_table;
function reset_table()
{
     for (var i=1; i<this.rows.length; i++){
       this.rows[i].selectable = true ;
	}
 }

 //--------------------------------------------------------------------------//
Table.prototype.count = count_table;
function count_table()
{
    var cnt =0 ;
     for (var i=1-WithOutHeader; i<this.rows.length; i++){ //guillaume for (var i=0; i<this.rows.length; i++){
       if (this.rows[i].selectable ){
         cnt = cnt + 1;
       }
	}
     return cnt;
 }
//--------------------------------------------------------------------------//
Table.prototype.update = update_table;
function update_table(selection,indexs){

   var is_link = new RegExp('<A');

   var operator = selection.operator ;
   operator = operator.replace("=","==");
   operator = operator.replace("<>","!=");
   operator = operator.replace("<==","<=");
   operator = operator.replace(">==",">=");
   var operand = selection.value;
   operand = operand.replace(/'/g,"");

   var modelereg = new RegExp("[a-zA-Z]",'i'); // guillaume
   
   if (isNaN(parseFloat(operand))
       && isNaN(parseInt(operand))){
   	    operand="'" + operand + "'" ;
   } else {
   		if (modelereg.test(operand)) {
				operand="'" + operand + "'" ;
		}
   }
   operand = operand.replace(/[ ]/g,"");

   for (var i=0;i<indexs.length;i++){
      for (var j=1;j<this.rows.length;j++){
        if (this.rows[j].selectable){
		 var value =  this.rows[j].values[indexs[i]-1] ;
		 value = value.replace(/'/g,"");


// traitement des liens vers les composants pour les proprietes de types class_instance_type afin de permettre la recherche
		if (value.match(is_link))
		{	
			var pre_link = new RegExp('(<A)[^>]+(>){1}','gi');
			value=value.replace(pre_link,'');
			var post_link = new RegExp('(<A>)','gi');
			value=value.replace(post_link,'');
		}

  		 if (isNaN(parseFloat(value))
       		&& isNaN(parseInt(value))){
           	value="'" + value + "'" ;
         } else {	
		 	if (modelereg.test(value)) {
				value="'" + value + "'" ;
			}	
		 }
         value = value.replace(/[ ]/g,"");

 		 var expression = value + " "+ operator + " " + operand ;
		 if (!eval(expression)){
            	this.rows[j].selectable = false ;
		 }
	   }
      }
    }
}
//--------------------------------------------------------------------------//
Table.prototype.redraw = redraw_table;
function redraw_table(indexs,typ){
   var id = this.element ;
   var tbody = document.getElementById(id).tBodies[0];

   for (var i=0;i<(indexs.length-WithLink);i++){
     for (var j = 2-WithOutHeader ; j < tbody.rows.length ; j++) { //guillaume for (var j = 1 ; j < tbody.rows.length ; j++) {
     	var cells =  tbody.rows[j].childNodes ;
          if (this.rows[j-(1-WithOutHeader)].selectable){ //guillaume if (this.rows[j].selectable){
             // alert(""+j+" , "+indexs[i]-1+ " "+ typ);
                cells[indexs[i]-1].className = typ;
		} else {
              cells[indexs[i]-1].className = "value";
             // alert(""+j+" , "+indexs[i]-1+ " "+ "value");
		}
	 }

   }
 }

//--------------------------------------------------------------------------//

Table.prototype.display = display_table;
function display_table(){
	
   var id = this.element ;
   var tbody = document.getElementById(id).tBodies[0];
   for (var i = 2-WithOutHeader ; i < tbody.rows.length ; i++) { 
	   //guillaume for (var i = 1 ; i < tbody.rows.length ; i++) {
       if (this.rows[i-(1-WithOutHeader)].selectable) { // guillaume
//          tbody.rows[i].className = "selectable" ;
          tbody.rows[i].style.backgroundColor = SelectableColor; //guillaume
       } else {
 //         tbody.rows[i].className = "unselected" ;
          tbody.rows[i].style.backgroundColor = UnSelectableCol;//guillaume
	  }
   }
}

//--------------------------------------------------------------------------//
ContentFamily.prototype.bsu = BSU;
ContentFamily.prototype.table = Table;
function ContentFamily(code,version,supplier,properties,table,frame){
  this.bsu(code,version,supplier);
  this.properties = new Object();
  this.meta_headers = new Array();
  this.family_links = new Array();
  if (properties) {
     for (var i=0;i<properties.length;i++ ){
          this.properties[properties[i].code]=properties[i];
	}
  }

  this.table = (table) ? table : null;
  this.frame = (frame) ? (frame): self ;
  eval("content=this;");
 }
//--------------------------------------------------------------------------//
 ContentFamily.prototype.indexs = get_indexs ;
 function get_indexs(code){
  return this.properties[code].indexs;
 }
//--------------------------------------------------------------------------//
 ContentFamily.prototype.code_bsu = get_code_bsu ;
 function get_code_bsu(){
  var code = this.code;
  var i = code.lastIndexOf(".")+1;
  var j = code.lastIndexOf("-");
  if (i<0) { i=0; }
  if (j<i) { j=code.length; }
  code = code.substring(i,j);
  return code ;
 }
 //--------------------------------------------------------------------------//
ContentFamily.prototype.getColumns = get_columns;
function get_columns(){
	return this.table.rows[0];
}
//--------------------------------------------------------------------------//
ContentFamily.prototype.getInstances = get_instances;
function get_instances(){
	var rows = this.table.rows;
	var values = new Array();
  	for (var i=1;i<rows.length;i++){
		values[i]=rows[i];
	}
	return values;
}
//--------------------------------------------------------------------------//
 ContentFamily.prototype.getFamilyLinks = get_family_links ;
 function get_family_links(){
	 return this.family_links;
 }//--------------------------------------------------------------------------//
 ContentFamily.prototype.getMetaHeaders = get_meta_headers ;
 function get_meta_headers(){
	 return this.meta_headers;
 }
//--------------------------------------------------------------------------//
 ContentFamily.prototype.update = update_content ;
 function update_content(selections,all)
 {
     var table = this.table;
	 table.reset();
     	for (var i=0;i<selections.length();i++){
          	var selection = selections.value(i);
			if (this.properties[selection.code])
			{
          		var indexs=this.properties[selection.code].indexs;
				table.update(selection,indexs);
			}
		}
   
   
 }
 //--------------------------------------------------------------------------//
 ContentFamily.prototype.get_header_table = get_header_table ;
 function get_header_table(index){
    var table = this.table;
	var value ="";
    if ((table) && (table.rows[0].length>=index)) {
       value = table.rows[0][index];
	  } 
	return value;
 }
//--------------------------------------------------------------------------//
 ContentFamily.prototype.get_value_table = get_value_table ;
 function get_value_table(indrow,indcol){
    var table = this.table;
	var value ="";
    if ((table) && (table.rows.length>=indrow)&& (table.rows[indrow].length>=indrow)) {
       value = table.rows[indrow][indcol];
	  } 
	return value;
 }

 //--------------------------------------------------------------------------//
 ContentFamily.prototype.display = display_content ;
 function display_content(selections,code){
         
			if (this.properties[code])
			{
				if (selections.is_removed()) {
				var indexs = this.properties[code].indexs;
			    this.table.redraw(indexs,"value");
				}
			}

			    for (var i=0;i<selections.length();i++){
			     	var selection = selections.value(i);
					//var indexs = this.properties[selection.code].indexs;
					if (this.properties[selection.code])
					{
						var indexs = this.properties[selection.code].indexs;
						this.table.redraw(indexs,"selected");
					}
			    }
	
    this.table.display();
 }
 //--------------------------------------------------------------------------//
 ContentFamily.prototype.activate = activate_content ;
 function activate_content()
 {
 	var table = this.table.rows ;
  	var id = this.table.element ;
 	var tbody = document.getElementById(id).tBodies[0];

   for (var i = 1-WithOutHeader ; i < tbody.rows.length ; i++) { 
//guillaume  for (var i = 0 ; i < tbody.rows.length ; i++) {
    	var cells = tbody.rows[i].childNodes ;
    		
   		for (var j = 0; j < (cells.length-WithLink); j++){
               if ( i<2-WithOutHeader ){ 
//guillaume if ( i<1 ) {
                 activate_cellule_header(cells[j]) ;
               } else if ( i <= table.length ) {
                 activate_cellule_value(cells[j],i-(1-WithOutHeader),j); 
//guillaume activate_cellule_value(cells[j],i,j);
			}
          }
     }
 }
 //--------------------------------------------------------------------------//
 ContentFamily.prototype.reset = reset_content ;
 function reset_content()
 {
   var id = this.table.element ;
   var tbody = document.getElementById(id).tBodies[0];
   for (var i = 2-WithOutHeader ; i < tbody.rows.length ; i++) { // guillaume
    		var cells = tbody.rows[i].childNodes ;
   		for (var j = 0; j < cells.length; j++){
            cells[j].className ="value";
          }
     }
   this.table.reset();
   this.table.display();
 }


//--------------------------------------------------------------------------//
function activate_cellule_header (cell) {

	    cell.onselectstart = function(){return false;}
	    cell.onmouseover  = function(){
            				old_color = this.style.backgroundColor;
					this.style.backgroundColor = SelectedColor;
	    				}
	    cell.onmouseout   = function(){
			    		 this.style.backgroundColor = old_color;
	    					}
	    cell.onmousedown = function(){
                     			this.style.backgroundColor = SelectedColor;
	    					}
	    cell.onmouseup = function(){
                     this.style.backgroundColor = SelectedColor;
	    					}
	    cell.onclick = function() {
	    				onclick_displayed_property(this.id);
// alert(" tag :" + this.tagName + "\n id :" + this.id + "\n value :" + this.innerText)
					}
}

//--------------------------------------------------------------------------//
 function activate_onmouseover_value(cell,i) {
     cell.onmouseover  = function(){onmouseover_selected_value(this,i);}
 }
//--------------------------------------------------------------------------//
 function activate_onmouseout_value(cell,i) {
	    cell.onmouseout  = function(){onmouseout_selected_value(this,i);}
  }
//--------------------------------------------------------------------------//
 function activate_onmousedown_value(cell,i) {
    cell.onmousedown = function(){onmousedown_selected_value(this,i);}
 }
//--------------------------------------------------------------------------//
 function activate_onmouseup_value(cell,i) {
	    cell.onmouseup = function(){onmouseup_selected_value(this,i);}
 }
//--------------------------------------------------------------------------//
 function activate_onclick_value(cell,i,j) {
	    cell.onclick = function(){
		onclick_selected_value(this,i,j);}
 }
//--------------------------------------------------------------------------//
function activate_cellule_value(cell,i,j) {
         cell.className = "value";
	//    cell.onselectstart = function(){return false;}
         activate_onmouseover_value(cell,i);
         activate_onmouseout_value(cell,i);
      //   activate_onmousedown_value(cell,i);
      //   activate_onmouseup_value(cell,i) ;
         activate_onclick_value(cell,i,j);
}
//--------------------------------------------------------------------------//
function onclick_displayed_property(iden)
{
   if (dictionary){
      dictionary.dictionary.set_displayed_property(iden);
   }
}

//--------------------------------------------------------------------------//
function onclick_selected_value(cell,i,j)
{
   if (content) {
          if (cell.className=="value") {
               if (content.table.rows[i].selectable){
                   activate_update_value(cell,i,j,"selected");
               }
          }  else {
             activate_update_value(cell,i,j,"value") ;
		}
    }
 }
//--------------------------------------------------------------------------//
 function activate_update_value(cell,i,j,typ){

 	if (selections) {
		var selection = null ;
     	if (typ=="value") {
			selection = selections.search(cell.id);
     	} else if (typ=="selected")  {
			//alert(cell.innerText);
  			//selection = new Selection(cell.id,"=",cell.firstChild.data);
  			selection = new Selection(cell.id,"=",cell.innerText);
			
		}
         selections.update(selection);
		 	
		 if (selections.is_removed()) content.update(selections,true) ;
         if (selections.is_adding()) content.update(selections,false) ;

		 content.display(selections,cell.id); 
	
         if (filter) {
        		if (selections.is_removed()) filter.update(true) ;
         		if (selections.is_adding()) filter.update(false) ;
				filter.display();
		 }
     }
 }

//--------------------------------------------------------------------------//
function onmouseup_selected_value(cell,i){
     if (content) {
          if (content.table.rows[i].selectable){
               cell.style.backgroundColor = old_color;
          } else {
               return false;
          }
     } else {
          return false ;
     }
}
//--------------------------------------------------------------------------//
function onmousedown_selected_value(cell,i){
 	if (content) {
     	if (content.table.rows[i].selectable){
			cell.style.backgroundColor = old_color;
          } else {
          	return false;
               }
     } else {
     	return false ;
	}

}
//--------------------------------------------------------------------------//
function onmouseout_selected_value(cell,i){
     if (content) {
          if (content.table.rows[i].selectable){
               cell.style.backgroundColor = old_color;
          } else {
               return false;
          }
     } else {
          return false ;
     }
}
//--------------------------------------------------------------------------//
function onmouseover_selected_value(cell,i){
     if (content) {
            if (content.table.rows[i].selectable){
                 old_color = cell.style.backgroundColor;
                 cell.style.backgroundColor = SelectedColor;

            } else {
                 return false;
            }
      } else {
            return false ;
     }
}

 //--------------------------------------------------------------------------//
ContentFamily.prototype.draw = draw_content ;
function draw_content(withlink){
	var Meta_Headers = this.getMetaHeaders();
	var Columns = this.getColumns();
	var Instances = this.getInstances();
	var Family_Links = this.getFamilyLinks();

    document.write("<TABLE id=\"T\" border=\"4\" align=\"center\" width=\"100%\">");
  
	// Affichage des MetaHeaders
	document.write("<TR>");
	for (i=0; i<Meta_Headers.length; i++){
		Meta_Headers[i].display();
	}
	//Affichage des liens si description generique
	if (withlink==1){
		document.write("<TH id='link' class='link' rowspan='2' nowrap><div class='label' align='center'> "+ languages.keywords.link +"</div></TH>");
	}
	document.write("</TR>");

	//Affichage des Headers
	document.write("<TR id=\"header\">");
	for (i=0; i<Columns.length; i++){
		Columns[i].display();
	}

	
	document.write("</TR>");


	//Affichage des composants
	for (i=1; i<Instances.length;i++){
		document.write("<TR class='content'>");
		for (j=0;j<Instances[i].values.length ;j++){
			document.write("<TD id=\""+Columns[j].code+"\"  class=\"value\" nowrap>"+Instances[i].values[j]+"</TD>");
		}

		if (withlink==1){
			Family_Links[i-1].display();
		}
		document.write("</TR>");
	}
	document.write("</TABLE>");	

}



//--------------------------------------------------------------------------//
ContentFamily.prototype.link = link_component ;
function link_component(dico,selects,search) {
//alert(dico);
  dictionary = (dico) ? dico : null ;
  selections = (selects) ? selects : null ;
  filter = (search) ? search : null ;
}
//--------------------------------------------------------------------------//
function echo(text) {
	document.write(text);
}
//--------------------------------------------------------------------------//
function d_w(text) {
	document.write(text);
}
//--------------------------------------------------------------------------//
//------------------------------------------------------
// DEGUG TOOLS
//------------------------------------------------------

function display(obj){
 
 var str ='';
 var level = 0;
 
 str += obj.nodeName + ' -> ' + obj.nodeValue + '\n';
 
 	display_attr(obj,level);
	
	if (obj.hasChildNodes())
	{
		for (i=0; i<obj.childNodes.length;i++){

			level++;
			str += '\tChild ' + obj.childNodes[i].nodeName + ' -> ' + obj.childNodes[i].nodeValue + '\n';
			str += display_attr(obj.childNodes[i],level);
		}
	}
	alert(str);
 
 
 }
 
 function display_attr(obj,level){
 	var str = '';
 	if (obj.attributes){
		for (i=0; i<obj.attributes.length;i++){
			if(obj.attributes[i].nodeValue) 
			{
				for(j=0;j<level;j++){
					str += '\t';
				}
				
				str += '\tAttr ' + obj.attributes[i].nodeName + ' -> ' + obj.attributes[i].nodeValue + '\n';
			}
		}
	}
	
	return (str);
 }