
/*
 *       The shopping basket
 *     Realized by Potier jean-claude
 * Copyright (c) 1999 CRCFAO. All Rights Reserved.
 * Permission to use, copy, modify, and distribute this software
 * and its documentation without prior written permission
 * from CRCFAO is not allowed.
 *
 * T�l�port 2, 1 avenue Cl�ment Ader , BP40109
 *        86960 FUTUROSCOPE
 *
 *
 */



 function Basket( elts, index, win){
    this.elts = (elts) ? elts : new Object() ;
}

//***********************************************************************

Basket.prototype.unselect = unselect_basket;
function unselect_basket(code,index ) {

  var dimension = this.elts[code].length;

  if (dimension>2)
  {
    for (var i=index; i<dimension; i++){
	 this.elts[code][i]=this.elts[code][i+1];
    }
    this.elts[code].length=dimension-1;
  } else {
    delete(this.elts[code]);
  }
  
  window.open("basket.html","basket", "scrollbars,resizable=yes,width=600,height=350,menubar=yes,status=yes");
}

//***********************************************************************
Basket.prototype.display_content = Display_Basket;
function Display_Basket(frame) {
	  var elts = this.elts;
      var already_display = false;
      
	  frame.focus();
      frame.document.writeln("<table align='center' class='contener' width='80%'>");
      frame.document.writeln("<tr> ");
      frame.document.writeln("<td> ");

      for ( var i in elts ) {
          frame.document.writeln("<table class='content' align='center'>");

          if (!already_display) {
            frame.document.write("<th nowrap colspan='"+elts[i][0].length+1+"'><p class='label'><b>" + languages.keywords.selected_components +"</b></p></th>") ;
		    already_display =true;
		  }

          frame.document.writeln("<tr> ");
          frame.document.write("<th nowrap></th>");
          for (var j=0; j<elts[i][0].length; j++){
             frame.document.writeln("<th nowrap> " + elts[i][0][j] + " </th>") ;
          }
          frame.document.writeln("</tr>");
          for (var j=1; j <elts[i].length; j++){
            var the_class = '';
            if (j % 2 == 0){
            		the_class = "values_pair";
            } else {
     			the_class = "values_impair";
            }

              frame.document.writeln("<tr>");
              
              frame.document.writeln("<td align='right'>" + "<input  class='destroy' name='remove' TYPE='image' src='../runtime/menu-images/select.gif' onclick='opener.top.filter.basket.unselect("+'"'+ i +'"'+","+ j +");'>" + "</td>");
             // frame.document.writeln( "<td class='quantity' align='center' nowrap> <input name='quantity"+i+j+"' type='text' size='10' value='"+elts[i][j][elts[i][j].length-1]+"' onchange='top.filter.basket.update_basket("+'"'+i+'",'+j+",this.form.quantity"+i+j+".value)' ></td>");

              for (var k=0; k <elts[i][j].length; k++){
                 if (elts[i][j][k]!=''){
                  frame.document.writeln("<td class='" + the_class + "' align='center' nowrap> " + elts[i][j][k] + " </td>") ;
                 }else {
                  frame.document.writeln("<td class='" + the_class + "' align='center' nowrap> &nbsp; </td>") ;
                 }
		      }
              frame.document.writeln("</tr>");
           }
          frame.document.writeln("</table>" );      
      }
	 
	  frame.document.writeln("</td> ");
      frame.document.writeln("</tr> ");

      frame.document.writeln("</table>" );
}

//***********************************************************************
Basket.prototype.add = Add_basket;
function Add_basket() {
  window.open("./component.html","basket", "scrollbars,resizable=yes,width=600,height=350,menubar=yes,status=yes");
}



//***********************************************************************
Basket.prototype.add_component = Add_component;

function Add_component(filter,frame) {
    var selectable = false ;

    if (filter.selections) {
      selectable = filter.selections.length()>0;
    }
    if (!selectable){
 //       	alert(languages.keywords.selectable_component);
            frame.focus();
            frame.document.writeln("<form>" );
            frame.document.writeln("<table class='contener' align='center' width='80%'>");
            frame.document.writeln("<tr> ");
            frame.document.writeln("<th> ");
	        frame.document.writeln("<div align='center'><p class='label'> <b> " + languages.keywords.selectable_component +" </b></p> </div>" );
            frame.document.writeln("</th> ");
            frame.document.writeln("</tr> ");
            frame.document.writeln("<tr> ");
            frame.document.writeln("<th> ");
            frame.document.writeln("<input type='button' class='button' value=' "+ languages.keywords.reset+" ' onclick='javascript : window.close()'> </p>" );
            frame.document.writeln("</th> ");
            frame.document.writeln("</tr> ");
            frame.document.writeln("</table>" );
   	        frame.document.writeln("</form>" )

    } else {
        if (filter.content) {
	   		
			var table = filter.content.table ;
            frame.document.writeln("<form>" );
            frame.document.writeln("<table class='contener' align='center' width='80%'>");
            frame.document.writeln("<tr> ");
            frame.document.writeln("<th> ");
	        frame.document.writeln("<div align='center'><p class='label'> <b> " + languages.keywords.select_quantities +" :</b></p> </div>" );
            frame.document.writeln("</th> ");
            frame.document.writeln("</tr> ");

            frame.document.writeln("<tr> ");
            frame.document.writeln("<td> ");
		    frame.document.writeln("<table class='content' align='center' >");
            frame.document.writeln("<tr> ");
            frame.document.write("<th nowrap> " + languages.keywords.number_quantities +"</th>") ;
             for (var i=1; i<= table.rows[0].length;i++) {
		    

                frame.document.writeln("<th nowrap> " + table.rows[0][i-1].labels + " </th>") ;
            }
            frame.document.writeln("</tr>");
  	
            
            for (var i = 1 ; i < table.rows.length ; i++) {
              if ( table.rows[i].selectable==true ) {
                frame.document.writeln("<tr>");
                frame.document.writeln( "<td align='center' nowrap> <input class='quantity' id='quantity"+i+"' onkeypress='window.opener.top.filter.basket.modify_values(self,"+i+")' type='text' size='10' value='1'></td>");
 	            
	     		for (var k =1; k<= table.rows[i].values.length;k++){
                  frame.document.writeln("<td class='values' align='center' nowrap> " + table.rows[i].values[k-1] + " </td>") ;
                } 
                frame.document.writeln("</tr>");
              }
           }
          
           frame.document.writeln("</table> ");
           frame.document.writeln("<tr> ");
           frame.document.writeln("<th> ");
           frame.document.writeln("<p><input type='button' class='button' value=' "+ languages.keywords.validate_quantities+" ' onclick='window.opener.top.filter.basket.update(window.opener.top.filter,self) ;javascript : window.close()'>" );
           frame.document.writeln("<input type='button' class='button' value=' "+ languages.keywords.reset+" ' onclick='window.opener.top.filter.basket.reset(window.opener.top.filter) ;javascript : window.close()'> </p>" );
           frame.document.writeln("</th> ");
           frame.document.writeln("</tr> ");
           frame.document.writeln("</table>" );
   	       frame.document.writeln("</form>" )
	  }
   }
}

//***********************************************************************

Basket.prototype.update = Update_basket;
function Update_basket(filter,frame){
        if (filter.content) {
        	var table = filter.content.table ;
         	var code = filter.content.code_bsu() ;

            if (!this.elts[code]){
       	     	this.elts[code]= new Array();
              	this.elts[code][0]=new Array();
  		        this.elts[code][0][0] = languages.keywords.number_quantities ;
 
              	for (var i=1; i<=table.rows[0].length;i++) {
          		     this.elts[code][0][i] = table.rows[0][i-1].labels ;
                }
            }

             for (var i = 1 ; i < table.rows.length ; i++) {
                if ( table.rows[i].selectable==true ) {
                   var j = this.elts[code].length;
                   this.elts[code][j] =new Array();
   //                this.elts[code][j][0]=eval(form+".quantity"+i+j+".value");

                   this.elts[code][j][0]=frame.document.getElementById("quantity"+i+"").value;
 
	               for (var k =1; k<=table.rows[i].values.length;k++){
        	           this.elts[code][j][k]=table.rows[i].values[k-1];
                   } 
                }
            }     
		 }
         if(filter) filter.selections.reset();
         if(filter) filter.reset();
 }
//***********************************************************************

Basket.prototype.reset = Reset_basket;
function Reset_basket(filter){
        if(filter) filter.selections.reset();
         if(filter) filter.reset();
 }

Basket.prototype.order_form = Order_form;
function Order_form() {
      window.open("basket.html","basket", "scrollbars,resizable=yes,width=600,height=350,menubar=yes,status=yes");
}

//***********************************************************************
 Basket.prototype.display_address = Address_basket;
 function Address_basket(frame){
 //     frame.document.writeln("<form>" );
      frame.document.writeln("<p><hr align='center' width='100%'>" );
      frame.document.writeln("<table align='center' class='contener' width='80%'>");
      frame.document.writeln("<tr> ");
      frame.document.writeln("<td> ");
      frame.document.writeln("<table class='address' align='center' >" );
      frame.document.writeln("<tr><th novrap colspan=3><p class='label'> <b> "+ languages.keywords.enter_informations +" :</b> </p></td></tr>" );

      frame.document.writeln("<tr><th novrap> " + languages.keywords.first_name + " :  </th> <td class='address'><input NAME='NAME' TYPE='text' VALUE='' SIZE='80'></td><th rowspan=7>&nbsp;&nbsp;&nbsp;&nbsp;</th></tr>" );
      frame.document.writeln("<tr><th novrap> " + languages.keywords.compagny + " :  </th> <td class='address'><input NAME='COMPANY' TYPE='text' VALUE='' SIZE=80></td></tr>" );
      frame.document.writeln("<tr><th novrap> " + languages.keywords.address + " :  </th> <td class='address'><input NAME='ADDRESS' TYPE='text' VALUE='' SIZE=80></td></tr>" );
      frame.document.writeln("<tr><th novrap> " + languages.keywords.email + " :  </th> <td class='address'><input NAME='EMAIL' TYPE='text' VALUE='' SIZE=80></td></tr>" );
      frame.document.writeln("<tr><th novrap> " + languages.keywords.postal_code + " :</th> <td class='address'><input NAME='CODE' TYPE='text' VALUE=''  SIZE=80></td></tr>" );
      frame.document.writeln("<tr><th novrap> " + languages.keywords.city + " :   </th> <td class='address'><input NAME='CITY' TYPE='text' VALUE='' SIZE=80></td></tr>" );
      frame.document.writeln("<tr><th novrap> " + languages.keywords.country + " :   </th> <td class='address'><input NAME='COUNTRY' TYPE='text' VALUE='' SIZE=80></td></tr>" );
      
      frame.document.writeln("<tr> ");
      frame.document.writeln("<th colspan=3> ");
      frame.document.writeln("<div align='center'><input type='button' class='button' value=' "+  languages.keywords.send_order  +  " ' onclick='window.close();'>" );
      frame.document.writeln("<input type='button' class='button' value=' "+  languages.keywords.print_order  +  " ' onclick='self.focus();javascript:window.print();window.close();'></div>" );
      frame.document.writeln("</th>" );
      frame.document.writeln("</tr>" );
     
      frame.document.writeln("</table>" );
      frame.document.writeln("</td>" );
      frame.document.writeln("</tr>" );
      frame.document.writeln("</table>" );
 }

