/************************************************************/

/************************************************************/

function displayClassConfiguration(
		code,
		version,
        revision,
		preferredName,
		shortName,
		synonymousNames,
        icon,
		originalDefinition,
		currentVersion,
		currentRevision,
		definition,
		note,
		remark,
		drawing){
	this.code = (code) ? code : false;
	this.version = (version) ? version : false;
    this.revision = (revision) ? revision : false;
    this.preferredName = (preferredName) ? preferredName : true;
    this.shortName = (shortName) ? shortName : false;
    this.synonymousNames = (synonymousNames) ? synonymousNames : false;
    this.icon = (icon) ? icon : false;
    this.originalDefinition = (originalDefinition) ? originalDefinition : false;
    this.currentVersion = (currentVersion) ? currentVersion : false;
    this.currentRevision = (currentRevision) ? currentRevision : false;
    this.definition = (definition) ? definition : false;
    this.note = (note) ? note : false;
    this.remark = (remark) ? remark : false;
    this.drawing = (drawing) ? drawing : false;
}

/************************************************************/

/************************************************************/

function displayPropertyConfiguration(
		code,
		version,
		revision,
		preferredName,
		shortName,
		synonymousNames,
        icon,
		originalDefinition,
		currentVersion,
		currentRevision,
		domain,
		symbol,
		synonymousSymbols,
		propertyTypeClassification,
		definition,
		note,
		remark,
		condition,
		figure,
		formula,
		propScope){
	this.code = (code) ? code : false;
	this.version = (version) ? version : false;
    this.revision = (revision) ? revision : false;
    this.preferredName = (preferredName) ? preferredName : true;
    this.shortName = (shortName) ? shortName : false;
    this.synonymousNames = (synonymousNames) ? synonymousNames : false;
    this.icon = (icon) ? icon : false;
    this.originalDefinition = (originalDefinition) ? originalDefinition : false;
    this.currentVersion = (currentVersion) ? currentVersion : false;
    this.currentRevision = (currentRevision) ? currentRevision : false;
    this.domain = (domain) ? domain : false;
    this.symbol = (symbol) ? symbol : false;
    this.synonymousSymbols = (synonymousSymbols) ? synonymousSymbols : false;
    this.propertyTypeClassification = (propertyTypeClassification) ? propertyTypeClassification : false;
    this.domain = (domain) ? domain : false;
    this.definition = (definition) ? definition : false;
	this.note = (note) ? note : false;
    this.remark = (remark) ? remark : false;
    this.condition = (condition) ? condition : false;
    this.figure = (figure) ? figure : false;
    this.formula = (formula) ? formula : false;
    this.propScope = (propScope) ? propScope : false;
}

/************************************************************/

/************************************************************/

function displayGeneralConfiguration(
		download,
		full_display,
		showMatches){
	this.download = (download) ? download : false;
	this.full_display = (full_display) ? full_display : false;
	this.showMatches = (showMatches) ? showMatches : 10;
}