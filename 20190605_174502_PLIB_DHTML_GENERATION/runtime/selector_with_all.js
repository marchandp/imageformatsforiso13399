//--------------------------------------------------------------------------//
/*              plib selector
 *       realized by POTIER Jean-Claude
 * Copyright (c) 1999 CRCFAO. All Rights Reserved.
 * Permission to use, copy, modify, and distribute this software
 * and its documentation without prior written permission
 * from LISI/ENSMA is not allowed.
 *
 * T�l�port 2, 1 avenue Cl�ment Ader , BP40109
 *        86960 FUTUROSCOPE
 *
 */
//--------------------------------------------------------------------------//

var selectors = null;

var content = null ;
var selections = null ;

var frame = self ;
var contener = "self" ;


//--------------------------------------------------------------------------//

function Selectors(win,frm){

   this.selectors = new Object();
   this.frame =(win)? win : self;
   this.contener =(frm)? frm : "self";
   eval("selectors = this.selectors;");
   eval("frame = this.frame;");
   eval("contener = this.contener;");
}

//--------------------------------------------------------------------------//

Selectors.prototype.link = link_components;
function link_components(selects,cont){
  selections = (selects) ? selects : null ;
  content = (cont) ? cont : null ;
}
//--------------------------------------------------------------------------//
Selectors.prototype.window = change_window;
function change_window(win,frm){
  this.frame = (win)? win : self;
  this.contener = (frm)? frm : "self";
  eval("frame = this.frame;");
  eval("contener = this.contener;");
}
//--------------------------------------------------------------------------//
Selectors.prototype.define_count = count_define;
function count_define( val ){

  if ( frame.document.all.tags("TABLE").length > 0 ) {
    frame.document.writeln(" <TR>");
    frame.document.writeln("<TH align='center' width='90%' colspan='5' nowrap>");
    frame.document.writeln("<DIV CLASS='counter'  width='90%' ID='cnt'>");
	var language = getCookie('language.cookie');
	if (language!=languages.language) languages.set_language(language);
	frame.document.writeln( " <B> " + languages.keywords.count_instances + " </B> : "+ val 
		                            + " " + languages.keywords.instances_availables );
    frame.document.writeln("</DIV>");
    frame.document.writeln("</TH>");
    frame.document.writeln(" </TR>");
  }
}
//--------------------------------------------------------------------------//
Selectors.prototype.update_count = count_update;
function count_update( val ){
     if ( frame.document.all.tags("TABLE").length > 0 ) {
 //         frame.document.all["cnt"].innerHTML = "<B> <SCRIPT> document.writeln(keywords.count_instances );</SCRIPT> </B>: "
//	  			+ val + " <SCRIPT> document.writeln(languages.keywords.instances_availables);</SCRIPT>";
	    var language = getCookie('language.cookie');
	    if (language!=languages.language) languages.set_language(language);
        frame.document.all["cnt"].innerHTML = "<B> "+ languages.keywords.count_instances + " </B>: "
	  			+ val + " " + languages.keywords.instances_availables ;
     }

}

//--------------------------------------------------------------------------//
Selector.prototype.widget_select_define = widget_select_define;
Selector.prototype.widget_select_display = widget_select_display;
Selector.prototype.widget_select_update = widget_select_update;

Selector.prototype.widget_label_define = widget_label_define;
Selector.prototype.widget_operators_define = widget_operators_define;
Selector.prototype.widget_operators_update = widget_operators_update;

Selector.prototype.widget_values_define = widget_values_define;
Selector.prototype.widget_values_display = widget_values_display;
Selector.prototype.widget_values_update = widget_values_update;
Selector.prototype.widget_selected_value_update = widget_selected_value_update;

Selector.prototype.widget_buttons_define = widget_buttons_define;
Selector.prototype.widget_buttons_display = widget_buttons_display;

//--------------------------------------------------------------------------//

function Selector ( code,name,operators){
     this.wlist = false;
     this.wselect = false;
     this.code = code;
     this.win = new Array();
     if ( frame.document.all.tags("TABLE").length > 0 ) {
          frame.document.writeln("<TR CLASS='selector'>");
          this.widget_select_define();
          this.widget_label_define(name);
          this.widget_operators_define(operators);
          this.widget_values_define();
          this.widget_buttons_define();
          frame.document.writeln("</TR>");
     }
     if (!selectors){
       selectors = new Array();
	}
     selectors[code] = this ;
}

//--------------------------------------------------------------------------//
function widget_display(frm,win,nav,iex,code,htmlcode ){

	   if (nav) {
		win.document.write(htmlcode);
        	win.document.close();
          win.visibility = "visible";
        } else if (iex){
          frm.document.all[code].innerHTML = htmlcode;
          win.visibility = "visible";
        }
}
//--------------------------------------------------------------------------//
 function widget_select_define() {
    var code = "Zs_" + this.code ;
    var nav = frame.document.layers;
	var iex = frame.document.all ;
      if (frame.document.all.tags("TABLE").length > 0 ) {
     	frame.document.writeln("<TD align='center' nowrap>");
     	frame.document.writeln("<DIV ID='"+ code +"'>");
        //  alert("tag :" + eval("frame.document.all."+ code));
         // eval(eval("document." + code));
          this.win[0]= (nav) ? eval("frame.document.all."+ code)
						: eval("frame.document.all."+ code + ".style");
     	this.widget_select_display();
     	frame.document.writeln("</DIV>");
          frame.document.writeln("</TD>");
     }

 }
//--------------------------------------------------------------------------//
 function widget_select_display() {
     var content = "";
     var code = "Zs_" + this.code ;
	var wselect = this.wselect ;
     var nav = frame.document.layers;
	var iex = frame.document.all ;
     var win = this.win[0];
     if (!wselect) {
	      content += "<INPUT TYPE='image' SRC='../runtime/menu-images/unselect.gif' NAME='cancel' "
		             +"onclick="+'"'+contener+".widget_select_doclick('"+this.code+"');"+'"'+">" ;
     } else {
     	content += "<INPUT TYPE='image' SRC='../runtime/menu-images/select.gif' NAME='cancel' "
		             +"onclick="+'"'+contener+".widget_select_doclick('"+this.code+"');"+'"'+">";
     }
     widget_display(frame,win,nav,iex,code,content);
 }
//--------------------------------------------------------------------------//

 function widget_select_update() {

     var selected_value = this.selected_value;
     if (selected_value != ""){
          this.wselect = true;
     } else {
          this.wselect = false;
     }
     this.widget_select_display()
 }

//--------------------------------------------------------------------------//
 function widget_select_doclick(code) {
     if (selectors) {

          var val = selectors[code].selected_value;
		var op = selectors[code].selected_operator;
          var selection = new Selection(code,op,val);
       	if (selections) {
          	selections.remove(selection);
               if (content) {
          		content.update(selections,true);
          		content.display(selections,code);
               }
               count_update(content.table.count());
           	selection = selections.search(code);
               if (selection){
         			selectors[code].widget_operators_update(selection.operator);
       			selectors[code].widget_selected_value_update(selection.value);
              } else {
       			selectors[code].widget_operators_update();
       		     selectors[code].widget_selected_value_update();
 			}
          } else {
   			selectors[code].widget_operators_update();
       		selectors[code].widget_selected_value_update();
		}
           if (selectors[code].wlist) {
        	 	indexs = content.properties[code].indexs;
        		var values = content.table.valuesByIndexs(indexs);
        		selectors[code].widget_values_update(values);
		 }

     }
 }
//--------------------------------------------------------------------------//

function widget_label_define(name){
	frame.document.writeln("<TH align='center' nowrap>");
     frame.document.writeln( " " + name + " ");
     frame.document.writeln("</TH>");
}
//--------------------------------------------------------------------------//

function widget_operators_define(operators){
    var code = "Op_" + this.code;
    this.operators = operators;
    this.selected_operator = "=";
    this.is_selected_operator = false ;
    frame.document.writeln("<TD align='center' nowrap>");
    frame.document.writeln("<SELECT NAME='"+code+"' CLASS='operator' onchange="+'"'+
    						contener+".widget_operators_doclick('"+this.code+"');"+'"'+">");
     var tselect = frame.document.all.tags("SELECT");
     if (tselect.length>0){
          var index = tselect.length-1;
     	for (var i=0; i < operators.length; i++){
      		tselect(index).options[i]= frame.document.createElement("OPTION");
      		tselect(index).options[i].text= operators[i];
      		tselect(index).options[i].value= operators[i];
         }
         tselect(index).options[0].selected = true;
	}
    // alert("fin creation");
     frame.document.writeln("</SELECT>");
     frame.document.writeln("</TD>");

}
//--------------------------------------------------------------------------//

 function widget_operators_doclick(code) {
       var  op = frame.event.srcElement.value;
       if (selectors) {
	    selectors[code].selected_operator= op ;
         selectors[code].is_selected_operator = true ;
       }
  }

//--------------------------------------------------------------------------//
function widget_operators_update(operator){
  var index = 0;
  var operators = this.operators;
  var tselect = frame.document.all.tags("SELECT");
  var code = "Op_" + this.code;
  operator = (operator)? operator : "=";
  this.selected_operator = operator ;
  for (var i=0;  i < operators.length; i++){
  	if (operators[i]==operator){
  		tselect(code).options[i].selected = true;
	}
  }
}
//--------------------------------------------------------------------------//
function widget_values_define() {
     var code = "Zv_" + this.code;
     this.selected_value = "" ;
     var nav = frame.document.layers ;
	var iex = frame.document.all ;
     if (frame.document.all.tags("TABLE").length >0 ) {
     	frame.document.writeln("<TD align='center' nowrap>");
     	frame.document.writeln("<DIV ID='"+ code +"'>");
          this.win[1]= (nav) ? eval("frame.document.all."+ code)
						: eval("frame.document.all."+ code + ".style");

     	this.widget_values_display();
      	frame.document.writeln("</DIV>");
    		frame.document.writeln("</TD>");
    }
}

//--------------------------------------------------------------------------//
function widget_values_display() {
    var content = "" ;
    var wlist = this.wlist ;

	var code = this.code ;
	var values =  this.values ;
	var selected_value = this.selected_value ;

	var nav = frame.document.layers;
	var iex = frame.document.all ;
    var win = this.win[1];
     if (wlist) {
	  	content += "<SELECT NAME='"+code+"' CLASS='values' onchange="+'"'+contener+".widget_values_doclick('"+this.code+"');"
	  		+'"'+ "onkeypress="+'"'+contener+".widget_values_dokeypress('"+this.code+"');"+'"'+">";
       content +="<OPTION VALUE=''></OPTION>";
	  for (var i =0; i<values.length ; i++){
		  if (values[i]==selected_value){
            content +="<OPTION SELECTED VALUE='"+ values[i] + "'>" + values[i] + " </OPTION>";
          } else {
           content +="<OPTION VALUE='"+ values[i] + "'>" + values[i] + " </OPTION>";
          }
       }
       content += "</SELECT>" ;

     } else {

	   content += "<INPUT TYPE='text' NAME='"+code+"' VALUE="+'"'+selected_value
	   		    +'"'+" CLASS='values' onchange="+'"'+contener+".widget_values_doclick('"+this.code+"');"
			    +'"'+ "onkeypress="+'"'+contener+".widget_values_dokeypress('"+this.code+"');"+'"'+">";
     }
     code = "Zv_" + this.code ;
     widget_display(frame,win,nav,iex,code,content);
 }

 //--------------------------------------------------------------------------//
 function widget_selected_value_update(selected_value) {
     this.selected_value = (selected_value) ? selected_value : "";
     this.widget_values_display();
     this.widget_select_update();
 }

//--------------------------------------------------------------------------//
 function widget_values_update(values) {
     this.values = values;
     this.widget_values_display();
     this.widget_select_update();
 }

 //--------------------------------------------------------------------------//
  function widget_values_dokeypress(code) {

      var e = frame.event;
      if(e==null)return;
  // alert("widget_values_dokeypress:"+e.srcElement.onchange);
      if ((e.keyCode==10) || (e.keyCode==13)){
	 	if (e.srcElement.onchange == null) {
        		widget_values_doclick(code);
      	} else {
          	e.srcElement.onchange();
          	e.srcElement.onchange=null;
          }
          e.keyCode=0;

	 }


  }
//--------------------------------------------------------------------------//

 function widget_values_doclick(code) {
 	var  val = frame.event.srcElement.value;
     if (selectors) {
         var is_new_op = selectors[code].is_selected_operator ;
         var old_val = selectors[code].selected_value ;
         if (((old_val!='') || ( val!=''))&& ((old_val!=val)||(is_new_op))){
        		selectors[code].selected_value= val ;
       		selectors[code].widget_select_update();
               var op = selectors[code].selected_operator;
       		if (selections) {
               	var selection = null;
               if (val ==''){
             	   selection = new Selection(code,op,old_val);
			} else {
			   selection = new Selection(code,op,val);
               }
               selections.update(selection);
// alert("additing:"+selections.is_adding());
// alert("remove:"+selections.is_removed());
           /*    if (content) {
                   if (selections.is_removed()) content.update(selections,true);
         		    if (selections.is_adding()) content.update(selections,false);

			    count_update(content.table.count());
                   content.display(selections,code);
               }*/
               if (selectors[code].wlist) {
        			indexs = content.properties[code].indexs;
        			var values = content.table.valuesByIndexs(indexs);
        			selectors[code].widget_values_update(values);
			}
              selectors[code].is_selected_operator =false;
          }
	  }
     }
   }
//--------------------------------------------------------------------------//

function widget_buttons_define () {
     var code = "Zb_" + this.code;
	 var nav = frame.document.layers ;
	 var iex = frame.document.all ;

     if (frame.document.all.tags("TABLE").length >0 ) {
     	frame.document.writeln("<TD align='center' nowrap>");
     	frame.document.writeln("<DIV ID='"+ code +"'>");
          this.win[2]= (nav) ? eval("frame.document.all."+ code)
						: eval("frame.document.all."+ code + ".style");
     	this.widget_buttons_display();
     	frame.document.writeln("</DIV>");
    		frame.document.writeln("</TD>");
    }
}
//--------------------------------------------------------------------------//
  function widget_buttons_display() {
      var content = "";
      var code = "Zb_"+ this.code ;
	 var wlist = this.wlist ;
      this.widget_select_display();

      var nav = frame.document.layers;
	 var iex = frame.document.all ;
      var win = this.win[2];
      if (wlist) {
        content +="<INPUT TYPE='image' SRC='../runtime/menu-images/input.gif' NAME='help'  onclick="+'"'
	   			+contener+".widget_buttons_doclick('"+ this.code+"');"+'"'+"onkeypress='return false;' >";
      } else {
         content +="<INPUT TYPE='image' SRC='../runtime/menu-images/liste1.gif' NAME='help' onclick="+'"'
	    		  +contener+".widget_buttons_doclick('"+ this.code+"');"+'"'+" onkeypress='return false;'>";
      }
     widget_display(frame,win,nav,iex,code,content);
  }
//--------------------------------------------------------------------------//

function widget_buttons_doclick (code) {
 if (selectors) {
 	var val = selectors[code].selected_value ;
 	selectors[code].wlist=!selectors[code].wlist;
     if ((selectors[code].wlist)&&(content)) {
        indexs = content.properties[code].indexs;
        var values = content.table.valuesByIndexs(indexs);
        selectors[code].widget_values_update(values);
	}
 	selectors[code].widget_selected_value_update(val);
 	selectors[code].widget_buttons_display();
  }
}
