//--------------------------------------------------------------------------//
/*              plib selection
 *       realized by POTIER Jean-Claude
 * Copyright (c) 1999 CRCFAO. All Rights Reserved.
 * Permission to use, copy, modify, and distribute this software
 * and its documentation without prior written permission
 * from LISI/ENSMA is not allowed.
 *
 * T�l�port 2, 1 avenue Cl�ment Ader , BP40109
 *        86960 FUTUROSCOPE
 *
 */

//--------------------------------------------------------------------------//
// state = remove || add
//--------------------------------------------------------------------------//

function Selections(){
    this.liste = new Array();
    this.state = "";
}

//--------------------------------------------------------------------------//
 function Selection ( code,operator,value,index ) {
    this.code = code ;
    this.value  = value ;
    this.operator  = (operator)? operator : "=";
    this.index = (index) ? index : null ;
}
//--------------------------------------------------------------------------//
function is_equal ( sel1, sel2 ) {
      return (sel1.code == sel2.code) &&
              (sel1.value == sel2.value) &&
                 (sel1.operator ==  sel2.operator)&&
			  	(sel1.index == sel2.index);
}
//--------------------------------------------------------------------------//

Selections.prototype.add = add_selection ;
function add_selection ( selection ){
     var selections = this.liste ;
     selections[selections.length] = selection ;
     this.state ="add";
}
//--------------------------------------------------------------------------//
Selections.prototype.length = length_selection ;
function length_selection (){
 return this.liste.length ;
}

//--------------------------------------------------------------------------//
 Selections.prototype.value = value_selection ;
function value_selection ( index ){
 	if (index < this.length()) {
 		return this.liste[index] ;
 	} else {
          return null;
	}
}


//--------------------------------------------------------------------------//
 Selections.prototype.state = state_selections ;
function state_selections ( index ){
   return this.state ;
}
//--------------------------------------------------------------------------//

Selections.prototype.is_removed = is_removed_selection ;
function is_removed_selection ( index ){
   return this.state == "remove" ;
}
//--------------------------------------------------------------------------//

Selections.prototype.is_adding = is_adding_selection ;
function is_adding_selection ( index ){
   return this.state == "add" ;
}

//--------------------------------------------------------------------------//
Selections.prototype.remove = remove_selection ;
function remove_selection (selection) {
    var selections = this.liste ;
    var tmp_selections = new Array ();
    var j=0;
    for (var i= 0 ; i < selections.length ; i++) {
          if (!is_equal(selections[i],selection)) {
    			tmp_selections[j] = selections[i];
               j = j + 1;
		}
    }
    this.liste = tmp_selections ;
    this.state ="remove";
}
//--------------------------------------------------------------------------//

Selections.prototype.search = search_selection ;
function search_selection (code) {
     var selections = this.liste ;
     var selection = null ;
	for (var i = 0 ; i < selections.length;i++ ) {
           if ( selections[i].code == code) {
               selection = selections[i] ;
           }
	}
     return selection;
}

//--------------------------------------------------------------------------//
Selections.prototype.index = index_selection ;
function index_selection (selection) {
     var selections = this.liste ;
     var index = -1 ;
	for (var i = 0 ; i < selections.length;i++ ) {
           if ( is_equal( selections[i], selection) ) {
               index = i ;
           }
	}
     return index;
}
//--------------------------------------------------------------------------//
Selections.prototype.update = update_selection ;
function update_selection( selection ){
     var selections = this.liste ;
     var index = this.index(selection);
     if (index>-1) {
          this.remove(selection);
	} else {
          this.add(selection);
	}
}


//--------------------------------------------------------------------------//
Selections.prototype.reset = delete_all_selections ;
function delete_all_selections(){
       this.liste = new Array ();
       this.state="";
}

//--------------------------------------------------------------------------//
Selections.prototype.display = display_all_selections ;
function display_all_selections(){
  var selections = this.liste ;
  document.writeln("<TABLE BORDER='1'>");
  document.writeln("<TR>");
  document.writeln("<TH>");
  document.writeln(" code ");
  document.writeln("</TH>");
  document.writeln("<TH>");
  document.writeln(" operator ");
  document.writeln("</TH>");
  document.writeln("<TH>");
  document.writeln(" value ");
  document.writeln("</TH>");
  document.writeln("</TR>");
  for (var i=0;i<selections.length;i++){
  	document.writeln("<TR>");
     document.writeln("<TD>");
     document.writeln(selections[i].code);
     document.writeln("</TD>");
     document.writeln("<TD>");
     document.writeln(selections[i].operator);
     document.writeln("</TD>");
     document.writeln("<TD>");
     document.writeln(selections[i].value);
     document.writeln("</TD>");
  	document.writeln("</TR>");
  }
  document.writeln("</TABLE>");
}


