
var keywords;

//------------------------------------------------------------------------
// keys words Constructor
//------------------------------------------------------------------------

function motscles(
		lang_code, lang_name,  name, sh_name, version, revision, code,
		d_c_v, d_c_r, d_o_d, def, note, remark, syn_name, subcl, supcl,
		partchar, data, symb, syn_symb, advchar, clsel, proptypecl,
		formula, unit, valfor, value, result, advanced, select, none,
		unavailable, maxpic, datatypeinf, table, simplied_drawing, icon,
		error, no_datatype,time_stamps,no_date_defined,add_in_basket,order_form,
		export_dico,export_family,export_component,count_instances,instances_availables,
		no_instances_availables,search,key_carateristics,carateristics,context_carateristics,
		select_quantities,number_quantities,validate_quantities,
		selected_components,enter_informations,first_name,compagny,address,email,
		postal_code,city,country,send_order,print_order,link,options,config_title,close,with_content,
		without_content,search,reset,administration,del,insert,update,unavailable_basket, not_implemented,
		selectable_component,the_choice,the_categories, applicable_selector, imported_selector,
		visible_selector, inherited_selector,
		number_type, int_type, int_measure_type, int_currency_type, real_type, real_measure_type,
		real_currency_type, non_quantitative_int_type, boolean_type, string_type,
		non_quantitative_code_type, level_type, named_type, aggregate_type, class_instance_type, complex_type,
		optional, unique, of, nature, scope, search_engine, results, of2, no_results, cl, prop, 
		externallyDefined, extImageNotImplemented, prev, next, clResults
		) {

	this.lang_code = lang_code;
	this.lang_name = lang_name;
	this.name = name;
	this.sh_name = sh_name;
	this.version = version;
	this.revision = revision;
	this.code = code;
	this.d_c_v = d_c_v;
	this.d_c_r = d_c_r;
	this.d_o_d = d_o_d;
	this.def = def;
	this.note = note;
	this.remark = remark;
	this.syn_name = syn_name;
	this.subcl = subcl;
	this.supcl = supcl;
	this.partchar = partchar;
	this.data = data;
	this.symb = symb;
	this.syn_symb = syn_symb;
	this.advchar = advchar;
	this.clsel = clsel;
	this.proptypecl = proptypecl;
	this.formula = formula;
	this.unit = unit;
	this.valfor = valfor;
	this.value = value;
	this.result = result;
	this.advanced = advanced;
	this.select = select;
	this.none = none;
	this.unavailable = unavailable;
	this.maxpic = maxpic;
	this.datatypeinf = datatypeinf;
	this.table = table;
	this.simplified_drawing = simplied_drawing;
	this.icon = icon;
//	this.document = document;
	this.error = error;
	this.no_datatype = no_datatype;
	this.time_stamps = time_stamps;
	this.no_date_defined = no_date_defined;
	this.add_in_basket = add_in_basket;
	this.order_form = order_form ;
	this.export_dico = export_dico ;
	this.export_family = export_family ;
	this.export_component = export_component ;
	this.count_instances = count_instances ;
	this.instances_availables = instances_availables ;
	this.no_instances_availables = no_instances_availables ;
	this.search = search;

	this.key_carateristics=key_carateristics; 
	this.carateristics=carateristics; 
	this.context_carateristics=context_carateristics;

	this.select_quantities = select_quantities ;
	this.number_quantities = number_quantities ;
	this.validate_quantities = validate_quantities ;

	this.selected_components = selected_components;
	this.enter_informations = enter_informations ;
	this.first_name = first_name;
	this.compagny = compagny ;
	this.address = address ;
	this.email = email ;
	this.postal_code = postal_code ;
	this.city = city ;
	this.country =  country ;
	this.send_order = send_order ;
	this.print_order =  print_order;
	this.link =  link;
	this.options=options;
	this.config_title=config_title;
	this.close=close;
	this.with_content=with_content;
	this.without_content=without_content;
	this.search=search;
	this.reset=reset;

	this.administration=administration;
	this.del=del;
	this.insert=insert;
	this.update=update;
	this.unavailable_basket=unavailable_basket;

	this.not_implemented = not_implemented;

	this.selectable_component = selectable_component;
	this.the_choice =the_choice ;
	this.the_categories =the_categories;
	
	this.applicable_selector = applicable_selector;
	this.imported_selector = imported_selector;
	this.visible_selector = visible_selector;
	this.inherited_selector = inherited_selector;

	this.number_type = number_type;
	this.int_type = int_type;
	this.int_measure_type = int_measure_type;
	this.int_currency_type = int_currency_type;
	this.real_type = real_type;
	this.real_measure_type = real_measure_type;
	this.real_currency_type = real_currency_type;
	this.non_quantitative_int_type = non_quantitative_int_type;
	this.boolean_type = boolean_type;
	this.string_type = string_type;
	this.non_quantitative_code_type = non_quantitative_code_type;
	this.level_type = level_type;
	this.named_type = named_type;
	this.aggregate_type = aggregate_type;
	this.complex_type = complex_type;

	this.class_instance_type = class_instance_type;
	this.optional = optional;
	this.unique = unique;
	this.of = of;
	this.nature = nature;
	this.scope = scope;
    this.search_engine = search_engine;
    this.results = results;
    this.of2 = of2;
    this.no_results = no_results;
    this.cl= cl;
    this.prop = prop;
	this.externallyDefined = externallyDefined;
	this.extImageNotImplemented = extImageNotImplemented;
	this.prev = prev;
	this.next = next;
	this.clResults = clResults;
}

var keywords_en = new motscles(
	"en",
	"English",
	"Name",
	"Short Name",
	"Version",
	"Revision",
	"Code",
	"Current Version",
	"Current Revision",
	"Original Definition",
	"Definition",
	"Note",
	"Remark",
	"Synonymous Names",
	"Subclasses",
	"Superclass",
	"Part Characteristics",
	"Data Type",
	"Symbol",
	"Synonymous Symbols",
	"Advanced Characteristics",
	"Class Selectors",
	"Property Type Classification",
	"Formula",
	"Unit",
	"Value Format",
	"Value(s)",
	"Result(s)",
	"Advanced",
	"Select",
	"None",
	"Unavailable",
	"Maximised Picture",
	"Data Type Informations",
	"Available Parts",
	"Simplified Drawing",
	"Icon",
	"Error",
	"No data type defined",
	"Time stamps",
	"",
	"Add in basket",
	"Order form",
	"Export dictionary",
	"Export family",
	"Export component",
	"Searching",
	"instance(s) available(s)",
	"no instance(s) available(s)",
	"Search",
	"Key Characteristics",
	"Charac.",
	"Ctxt Dep.",
	"Please enter your quantity(ies)",
	"Quantity",
	"validate",
	"Selected components",
	"Please enter your informations",
	"First Name",
	"Compagny",
	"Address",
	"Email",
	"Postal Code",
	"City",
	"Country",
	"Send purchase order",
	"Print order",
	"Link to family",
	"options",
	"configuration",
	"close",
	"Display content",
	"Do not display content",
	"Search",
	"Reset",
	"Administration",
	"Delete",
	"Insert",
	"Update",
	"Basket accessible only to the level from the basic families",
	"To be implemented",
	"Select at least an article",
	"Choose your paddle",
	"Rivers categories",
	"Applicable",
	"Imported",
	"Visible",
	"Inherited",
	"Number",
	"Integer",
	"Integer measure",
	"Integer currency",
	"Real",
	"Real measure",
	"Real currency",
	"Enumeration (integer)",
	"Boolean",
	"String",
	"Enumeration (code)",
	"Level",
	"Named type",
	"Aggregate",
	"Class",
	"Complex",
	"Unique",
	"Optional",
	"Of",
	"Nature",
	"Scope",
    "Search Engine",
    "Search Results",
    "of",
    "returned no results",
    "Class",
    "Property",
	"Externally defined",
	"Externally defined property image viewer not implemented",
	"Prev NN ResultSS",
	"Next NN ResultSS",
	"NO RESULT"
);

var keywords_fr = new motscles(
	"fr",
	"Fran&#x00E7;ais",
	"Nom",
	"Nom Court",
	"Version",
	"R&#x00E9;vision",
	"Code",
	"Version courante",
	"R&#x00E9;vision courante",
	"D&#x00E9;finition Originale",
	"D&#x00E9;finition",
	"Note",
	"Remarque",
	"Synonymes",
	"Sous-classes",
	"Super-classe",
	"Propri&#x00E9;t&#x00E9;s caract&#x00E9;ristiques",
	"Type de donn&#x00E9;es",
	"Symbole",
	"Symboles synonymes",
	"Propri&#x00E9;t&#x00E9;s avanc&#x00E9;es",
	"S&#x00E9;lecteurs de classe",
	"Classification du type de propri&#x00E9;t&#x00E9;",
	"Formule",
	"Unit&#x00E9;",
	"Format", "Valeur(s)",
	"R&#x00E9;sultat(s)",
	"Avanc&#x00E9;",
	"S&#x00E9;lection",
	"aucun",
	"non disponible",
	"Image agrandie",
	"Informations sur le type de donn&#x00E9;es",
	"Composants Disponibles",
	"Dessin Simplifi&#x00E9;",
	"Icone",
	"Erreur",
	"Pas de type d&#x00E9;fini",
	"Historique",
	"",
	"Ajouter au panier",
	"Bon de commande",
	"Exporter dictionnaire",
	"Exporter famille",
	"Exporter composant",
	"Recherche",
	" instance(s) disponible(s)",
	"pas d'instance(s) disponible(s)",
	"Rechercher",
	"Caract&eacute;ristiques principales",
	"Caract&eacute;ristiques secondaires",
	"Caract&eacute;ristiques contextuelles",
	"Entrer la quantit&eacute; choisie",
	"Quantit&eacute;",
	"Valider",
	"Composants s&eacute;lectionn&eacute;es",
	"Entrer vos coordonn&eacute;s",
	"Nom",
	"Soci&eacute;t&eacute;",
	"Adresse",
	"Email",
	"Code Postal",
	"Ville",
	"Pays",
	"Envoyer la commande",
	"Imprimer la commande",
	"Lien vers la famille",
	"options",
	"configuration du catalogue",
	"fermer",
	"Afficher le contenu",
	"Ne plus afficher le contenu",
	"Recherche",
	"Reset",
	"Administration",
	"D�truire",
	"Ins�rer",
	"Modifier",
	"Panier accessible uniquement � partir de la famille d'origine du composant",
	"A impl&eacute;menter",
	"Selectionnez au moins un article",
	"Choisir sa pagaie",
	"Classes de rivi&egrave;res",
	"Applicable",
	"Import&eacute;es",
	"Visible",
	"H�rit&eacute;es",
	"Nombre",
	"Entier",
	"Entier, mesure",
	"Entier, monnaie",
	"R&eacute;el",
	"R&eacute;el, mesure",
	"R&eacute;el monnaie",
	"Enumeration (entier)",
	"Bool&eacute;en",
	"Cha&icirc;ne&nbsp;de caract�res",
	"Code&nbsp;non quantitatif",
	"Niveau",
	"Type nomm&eacute;",
	"Aggr&eacute;gat",
	"Classe",
	"Complexe",
	"Unique",
	"Optionnel",
	"De",
	"Nature",
	"Port&eacute;e",
    "Moteur de recherche",
    "R&eacute;sultats",
    "de",
    "n'a pas retourn&eacute; de r&eacute;sultats",
    "Classe",
    "Propri&eacute;t&eacute;",
	"D�finie ext�rieurement",
	"Visualiseur d'image de propri&eacute;t&eacute; d&eacute;finie ext&eacute;rieurement non impl&eacute;ment&eacute;.",
	"NN R�sultatSS Pr�c",
	"NN R�sultatSS Suiv",
	"PAS DE RESULTAT"
	);


var keywords_es = new motscles(
	"es",
	"Spanish",
	"Nombre",
	"Nombre",
	"Version",
	"Repaso",
	"Codigo",
	"Version corriente",
	"Repaso corriente",
	"Definicion original",
	"Definicion",
	"Observacion",
	"Sugerencia",
	"Nombres sinonimos",
	"Subcategoria",
	"Super categoria",
	"Caract�risticas",
	"Tipo de datos",
	"Simbolo",
	"Simbolos sinonimos",
	"Caract�risticas avanzadas",
	"Lectura de categoria",
	"Clasificacion del tipo de propiedad",
	"Formula",
	"Unidad",
	"Tamano valor",
	"Valors",
	"Resultados",
	"Adelantos",
	"Seleccion",
	"Ningun",
	"Agotado",
	"Imagen ampliada",
	"Informacion sobre el tipo de datos",
	"Articulos disponibles",
	"Dibujo simplificado",
	"Icono",
	"Error",
	"Tipo indefinido",
	"Historia",
	"",
	"Anadir al carrito",
	"Pedido",
	"Exportar clasificaci�n",
	"Exportar categoria",
	"Exportar articulo",
	"Busqueda",
	"Articula disponible",
	"Articula indisponible",
	"Buscar",
	"Caract�risticas principales",
	"Caract�risticas segundarias",
	"Caract�risticas contextuales",
	"Entrar la cantidad escogida",
	"Cantidad",
	"Confirmar",
	"Articulos seleccionados",
	"Entrar sus senas",
	"Nombre",
	"Sociedad",
	"Dirrecion",
	"Correo electronico",
	"Codigo postal",
	"Ciudad",
	"Pais",
	"Enviar el pedido",
	"Imprimir el pedido",
	"Enlace hacia la familia",
	"Opciones",
	"Configuracion del catalogo",
	"Cerrar",
	"Publicar el contenido",
	"Do not display content",
	"Busqueda",
	"Reset",
	"Administracion",
	"Destruir",
	"Insertar",
	"Modificar",
	"Cesta accesible solamente a partir de la familia de origen del componente",

	"To be implemented",

	"Seleccione al menos un art�culo",
	"Eligir su pala",
	"Categorias de rios",
	"Aplicable",
	"Importado",
	"Visible",
	"Heredado",

	"Number",
	"Integer",
	"Integer measure",
	"Integer currency",
	"Real",
	"Real measure",
	"Real currency",
	"Non quantitative integer",
	"Boolean",
	"String",
	"Non quantitative code",
	"level",
	"Named type",
	"Aggregate",
	"Class",
	"Complex",

	"Unique",
	"Optional",
	"Of",
	"Nature",
	"Scope",
    "Search Engine",
    "Search Results",
    "of",
    "returned no results",
    "Class",
    "Property",
	"Externally defined",
	"Externally defined property image viewer not implemented",
	"Prev NN ResultSS",
	"Next NN ResultSS",
	"NO RESULT"	
);

//------------------------------------------------------------------------
// Translatable Text Constructor
//------------------------------------------------------------------------

function Translatable(text,lang) {
	this.text = (text) ? text : "";
	this.lang = (lang) ? lang : "nolg";
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------

function Languages(list) {
	this.languages = (list instanceof Array) ? list : new Array();
	var langue ="";
	if (getCookie('language.cookie') != null){
		langue = getCookie("language.cookie");
	} else {
		langue = (list.length > 0)? list[0]: "en";
		setCookie('language.cookie',langue);
	}
	this.language = langue; // si les cookies ne marchent pas.
	switch (langue){
		case "en" :
			this.keywords = keywords_en;
		break;
		case "fr" :
			this.keywords = keywords_fr;
		break;
		case "es" :
			this.keywords = keywords_es;
		break;
		default:
			this.keywords = keywords_en;
		break;
	}
	eval("keywords = this.keywords;");
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
Languages.prototype.get_language = get_current_language;
function get_current_language(){

	if (getCookie('language.cookie') != null){
		//alert("get_current_language() - y a un cookie: " + getCookie("language.cookie"));
		return getCookie("language.cookie");
	} else {
		//alert("get_current_language() - y a pas de cookie: " + this.language);
		return this.language;
	}
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
Languages.prototype.set_language = set_current_language;
function set_current_language(langue){
	this.language = langue ;
	setCookie('language.cookie',langue);
	switch (langue.toLowerCase()){
		case "en" :
			this.keywords = keywords_en;
		break;
		case "fr" :
			this.keywords = keywords_fr;
		break;
		case "es" :
			this.keywords = keywords_es;
		break;
		default:
			this.keywords = keywords_en;
		break;
	}
	eval("keywords = this.keywords;");
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------

Languages.prototype.display = display_languages;
function display_languages(){
	var languages = this.languages ;
	//var isIE4up = ((navigator.appName == "Microsoft Internet Explorer") &&
	//	(parseInt(navigator.appVersion.substring(0, 1)) >= 4))? true : false;
	
	var isIE4up = true;
	
	for (i = 0; i <= (languages.length - 1); i++) {
		if (isIE4up) {
			document.write("<img src='../runtime/menu-images/" 
				+ languages[i] + ".gif' width=50 height=40 alt='"
				+ languages[i] + "' onClick=switch_language('"
				+ languages[i] + "')>");
		} else {
			document.write('<A HREF="#" onClick="switch_language("'
				+ languages[i] + '">');
			document.write('<IMG SRC="../runtime/menu-images/'
				+ languages[i] + '.gif" width=30 height=20 alt="'
				+ languages[i] + '" BORDER=0>');
			document.write('</A>');
		}
	}
}
