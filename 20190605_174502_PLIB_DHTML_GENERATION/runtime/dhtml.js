/* Biblioth&egrave;que DHTML
   Original by www.selfhtml.com.fr
*/

var DHTML = 0, DOM = 0, MS = 0, NS = 0, OP = 0;

function DHTML_init() {

 if (window.opera) {
     OP = 1;
 }
 if(document.getElementById) {
   DHTML = 1;
   DOM = 1;
 }
 if(document.all && !OP) {
   DHTML = 1;
   MS = 1;
 }
if(document.layers && !OP) {
   DHTML = 1;
   NS = 1;
 }
}

function getElemById(doc,id){
	if(DOM) {
		if (typeof doc.getElementById(id) == "object")
			{ Elem = doc.getElementById(id); }
		else
			{ Elem = void(0); }
		return(Elem); 
	}
	else {
		if(MS) {
			if (typeof doc.all[id] == "object")
				{ Elem = doc.all[id]; }
			else
				{ Elem = void(0); }
		}
		else {
			if(NS) {
				Elem = (typeof doc[id] == "object")?  doc[id] : void(0);
			}
			else {
				Elem = void(0); 
			}
		}
  	}
	return(Elem);
}

function getElemByTagName(doc,tn,ind) {
	var Elem;
	if(DOM) {
		if (typeof doc.getElementsByTagName(tn) == "object" || (OP && typeof doc.getElementsByTagName(tn) == "function")){
			Elem = doc.getElementsByTagName(tn)[ind];
		}	
		else
		{ 	
			Elem = void(0); 
		}
		
	}
	else{
		if(MS) {
			Elem = (typeof doc.all.tags(tn) == "object")?  doc.all.tags(tn)[ind] : void(0);
		}
		else
		{
			Elem = void(0); 
		}
	}
	return(Elem);
}

function getElemTagsLength(doc,tn) {
	var Elem;
	if(DOM) 
	{
		if (typeof doc.getElementsByTagName(tn) == "object" ||(OP && typeof doc.getElementsByTagName(tn) == "function")){
			Elem = doc.getElementsByTagName(tn).length;
		}	
		else
		{
			Elem = void(0); 
		}
		return(Elem);
	}
	else
	{
		if(MS) {
			if (typeof doc.all.tags(tn) == "object")
			{
				Elem = doc.all.tags(tn).length;
			}
			else
			{ 
				Elem = void(0); 
			}
		}
		else 
		{	
			Elem = void(0);
		}
		return(Elem);
	}
}

function getElem(doc,p1,p2,p3) {
var Elem;
 if(DOM) {
   if(p1.toLowerCase()=="id") {
     if (typeof doc.getElementById(p2) == "object")
     Elem = doc.getElementById(p2); 
     else Elem = void(0);
     return(Elem);
   }
   else if(p1.toLowerCase()=="name") {
     if (typeof doc.getElementsByName(p2) == "object")
     Elem = doc.getElementsByName(p2)[p3];
     else Elem = void(0);
     return(Elem);
   }
   else if(p1.toLowerCase()=="tagname") {
     if (typeof doc.getElementsByTagName(p2) == "object" ||
        (OP && typeof doc.getElementsByTagName(p2) == "function")){
	      	if (p3.toLowerCase() == "length"){
   		Elem = doc.getElementsByTagName(p2).length;}
		else{
		Elem = doc.getElementsByTagName(p2)[p3];}
	}	
	else
	{ Elem = void(0); }
     return(Elem);
   }
   else return void(0);
 }
 else if(MS) {
   if(p1.toLowerCase()=="id") {
     if (typeof doc.all[p2] == "object")
     Elem = doc.all[p2];
     else Elem = void(0);
     return(Elem);
   }
   else if(p1.toLowerCase()=="tagname") {
     if (typeof doc.all.tags(p2) == "object"){
     	if (p3.toLowerCase() == "length")
	     {
	              Elem = doc.all.tags(p2).length;
      		}else
         	{
	      	Elem = doc.all.tags(p2)[p3];
      	      	}
     }
     else
     { Elem = void(0); }
     return(Elem);
   }
   else if(p1.toLowerCase()=="name") {
     if (typeof doc[p2] == "object")
     Elem = doc[p2];
     else Elem = void(0);
     return(Elem);
   }
   else return void(0);
 }
 else if(NS) {
   if(p1.toLowerCase()=="id" || p1.toLowerCase()=="name") {
   if (typeof doc[p2] == "object")
     Elem = doc[p2];
     else Elem = void(0);
     return(Elem);
   }
   else if(p1.toLowerCase()=="index") {
    if (typeof doc.layers[p2] == "object")
     Elem = doc.layers[p2];
    else Elem = void(0);
     return(Elem);
   }
   else return void(0);
 }
}

function getCont(p1,p2,p3) {
   var Cont;
   if(DOM && getElem(p1,p2,p3) && getElem(p1,p2,p3).firstChild) {
     if(getElem(p1,p2,p3).firstChild.nodeType == 3)
       Cont = getElem(p1,p2,p3).firstChild.nodeValue;
     else
       Cont = "";
     return(Cont);
   }
   else if(MS && getElem(p1,p2,p3)) {
     Cont = getElem(p1,p2,p3).innerText;
     return(Cont);
   }
   else return void(0);
}

function getAttr(p1,p2,p3,p4) {
   var Attr;
   if((DOM || MS) && getElem(p1,p2,p3)) {
     Attr = getElem(p1,p2,p3).getAttribute(p4);
     return(Attr);
   }
   else if (NS && getElem(p1,p2)) {
       if (typeof getElem(p1,p2)[p3] == "object")
        Attr=getElem(p1,p2)[p3][p4]
       else
        Attr=getElem(p1,p2)[p4]
         return Attr;
       }
   else return void(0);
}

function setCont(doc,p1,p2,p3,p4) {

   if(DOM && getElem(doc,p1,p2,p3) && getElem(doc,p1,p2,p3).firstChild)
     {getElem(doc,p1,p2,p3).firstChild.nodeValue = p4;
	 alert(getElem(doc,p1,p2,p3).firstChild.nodeName);}
   else if(MS && getElem(doc,p1,p2,p3))
     getElem(doc,p1,p2,p3).innerText = p4;
   else if(NS && getElem(doc,p1,p2,p3)) {
     getElem(doc,p1,p2,p3).document.open();
     getElem(doc,p1,p2,p3).document.write(p4);
     getElem(doc,p1,p2,p3).document.close();
   }
}


DHTML_init();


