//------------------------------------------------------
// DEGUG TOOLS
//------------------------------------------------------

function display(obj){
 var str ='';
 var level = 0;
 
 str += obj.nodeName + ' -> ' + obj.nodeValue + '\n';
 
 	display_attr(obj,level);
	
	if (obj.hasChildNodes())
	{
		for (i=0; i<obj.childNodes.length;i++){

			level++;
			str += '\tChild ' + obj.childNodes[i].nodeName + ' -> ' + obj.childNodes[i].nodeValue + '\n';
			str += display_attr(obj.childNodes[i],level);
		}
	}
	alert(str);
 
 
 }
 
 function display_attr(obj,level){
 	var str = '';
 	if (obj.attributes){
		for (i=0; i<obj.attributes.length;i++){
			if(obj.attributes[i].nodeValue) 
			{
				for(j=0;j<level;j++){
					str += '\t';
				}
				
				str += '\tAttr ' + obj.attributes[i].nodeName + ' -> ' + obj.attributes[i].nodeValue + '\n';
			}
		}
	}
	
	return (str);
 }