var myclass = null;
var selection = new Array();
//var isIE4up = ((navigator.appName == "Microsoft Internet Explorer")&&(parseInt(navigator.appVersion.substring(0, 1)) >= 4))? true : false;
var isIE4up =true;
var old_color;
var ERROR = "#error#";
var popupexiste= new Array();  // array of boolean for the creation of the different popups
var NOT_TRANSLATED = "not translated";

popupexiste["DataType"] = false;
popupexiste["Advanced"] = false;
popupexiste["Selector"] = false;
popupexiste["Imagemax"] = false;

var advanced = parseInt(getCookie("advanced.cookie"));  // State boolean for the advanced characteristics PopUp Window
var seltable = parseInt(getCookie("seltable.cookie"));  // State boolean for the Class Selectors PopUp Window

var WithProperties = true;

var propertyFilterStatus = new Array();
propertyFilterStatus["Applicable"]="1,3";
propertyFilterStatus["InheritedApplicable"]="1,2,3,4";
propertyFilterStatus["Visible"]="5";
propertyFilterStatus["InheritedVisible"]="5,6";
propertyFilterStatus["Imported"]="1";
propertyFilterStatus["InheritedImported"]="1,2";

var lastPropertyFilterStatus = getCookie("lastPropertyFilterStatus.cookie");
var lastSelectedProperty = getCookie("lastSelectedProperty.cookie");
var lastSelectedClass = getCookie("lastSelectedClass.cookie");

//alert("Dictionary: " + lastSelectedProperty)

//--------------------------------------------------------------------------//
// BSU constructor
//--------------------------------------------------------------------------//

function BSU(code, version, element){
	this.code = code;
	this.version = (version) ? version : "001";
	this.element = (element) ? element : null;
}

//--------------------------------------------------------------------------//
// define supplier constructor
//--------------------------------------------------------------------------//

Supplier.prototype.bsu = BSU;
function Supplier(code,organization,address,dates){
	this.bsu(code,"001",null);
	this.organization =(organization) ? organization: null ;
	this.address =(address) ? address : null ;
	this.time_stamps = (dates) ? dates : new Array() ;
}


//--------------------------------------------------------------------------//
// define dates constructor
//--------------------------------------------------------------------------//

function Dates(d_c_v, d_c_r, d_o_d, elt){
	this.date_current_version = (d_c_v) ? d_c_v : "" ;
	this.date_current_revision = (d_c_r) ? d_c_r : "" ;
	this.date_original_definition = (d_o_d) ? d_o_d : "" ;
	if (elt) elt.time_stamps = this;
}
//--------------------------------------------------------------------------//
Dates.prototype.get_date_current_version = getD_C_V;
function getD_C_V(){
	return this.date_current_version;
}
//--------------------------------------------------------------------------//
Dates.prototype.get_date_current_revision = getD_C_R;
function getD_C_R(){
	return this.date_current_revision;
}
//--------------------------------------------------------------------------//
Dates.prototype.get_date_original_definition = getD_O_D;
function getD_O_D(){
	return this.date_original_definition;
}

//--------------------------------------------------------------------------//
// define item_names constructor
//--------------------------------------------------------------------------//

function Item_names(pref_name,sh_name,icon,syn_names,elt){
	this.preferred_name = pref_name ;
	this.short_name = (sh_name) ? sh_name : "";
	this.icon = (icon) ? icon : "";
	this.synonymous_names = (syn_names) ? syn_names : new Array();
	if (elt) elt.names = this;
}
//--------------------------------------------------------------------------//
Item_names.prototype.get_preferred_name = getPreferredName;
function getPreferredName(){
	return this.preferred_name;
}
//--------------------------------------------------------------------------//
Item_names.prototype.get_short_name = getShortName;
function getShortName(){
	return this.short_name;
}
//--------------------------------------------------------------------------//
Item_names.prototype.get_icon = getIcon;
function getIcon(){
	return this.icon;
}
//--------------------------------------------------------------------------//
Item_names.prototype.get_synonymous_names = getSynonymousNames;
function getSynonymousNames(){
	return(this.synonymous_names);
}

//--------------------------------------------------------------------------//
// define CLASS constructor
//--------------------------------------------------------------------------//

Class.prototype.bsu = BSU;
function Class(supplier,code,version,names,dates,revision,definition,note,remark,
		its_superclass,subclasses,selectors,document,drawing,url){
	this.bsu(code,version,supplier);
	this.revision = (revision) ? revision : "001";
	this.names = (names) ? names : null ;
	this.time_stamps = (dates) ? dates : null;
	this.definition = (definition) ? definition : "";
	this.note = (note) ? note : "";
	this.remark = (remark) ? remark : "";
	this.its_superclass = (its_superclass) ? its_superclass : null ;
	this.subclasses = (subclasses) ? subclasses : new Array() ;
	this.document = (document) ? document : "" ;
	this.drawing = (drawing) ? drawing : "" ;
	this.url = (url) ? (url): "";
	this.described_by = new Array();
	this.selectors = (selectors) ? selectors : new Array();
	eval("myclass=this;");
}
//------------------------------------------------------------------------
Class.prototype.get_code_bsu = getCodeBSUClass ;
function getCodeBSUClass(){
	return this.code ;
}
//------------------------------------------------------------------------
Class.prototype.get_version = getVersionClass ;
function getVersionClass(){
	return this.version ;
}
//------------------------------------------------------------------------
Class.prototype.get_revision = getRevisionClass ;
function getRevisionClass(){
	return this.revision ;
}

//------------------------------------------------------------------------
Class.prototype.get_preferred_name = getPreferredNameClass;
function getPreferredNameClass(){

	return (this.names) ? this.names.get_preferred_name(): "";
}
//------------------------------------------------------------------------
Class.prototype.get_short_name = getShortNameClass;
function getShortNameClass(){
	return (this.names) ? this.names.get_short_name():"";
}
//------------------------------------------------------------------------
Class.prototype.get_definition = getDefinitionClass;
function getDefinitionClass(){
	return this.definition ;
}
//------------------------------------------------------------------------
Class.prototype.get_synonymous_names = getSynonymousNamesClass;
function getSynonymousNamesClass(){
	return (this.names) ? this.names.get_synonymous_names() : new Array();
}
//------------------------------------------------------------------------
Class.prototype.get_icon = getIconClass;
function getIconClass(){
	return (this.names)? this.names.get_icon(): "";
}
//------------------------------------------------------------------------
Class.prototype.get_note = getNoteClass;
function getNoteClass(){
	return this.note;
}
//------------------------------------------------------------------------
Class.prototype.get_remark = getRemarkClass;
function getRemarkClass(){
	return this.remark;
}
//------------------------------------------------------------------------
Class.prototype.get_document = getDocumentClass;
function getDocumentClass(){
	return this.document ;
}
//------------------------------------------------------------------------
Class.prototype.get_drawing = getDrawingClass;
function getDrawingClass(){
	return this.drawing ;
}
//------------------------------------------------------------------------
Class.prototype.get_url = getUrlClass;
function getUrlClass(){
	return this.url ;
}
//------------------------------------------------------------------------
Class.prototype.get_described_by = getDescribedByClass;
function getDescribedByClass(){
	return this.described_by ;
}
//------------------------------------------------------------------------
Class.prototype.get_its_superclass = getItsSuperclass;
function getItsSuperclass(){
	return this.its_superclass ;
}
//------------------------------------------------------------------------
Class.prototype.get_subclasses = getSubclasses;
function getSubclasses(){
	return this.subclasses ;
}
//------------------------------------------------------------------------
Class.prototype.get_selectors = getSelectorsClass;
function getSelectorsClass(){
	return this.selectors ;
}
//--------------------------------------------------------------------------//
Class.prototype.get_time_stamps=getTimeStampsClass;
function getTimeStampsClass(){
  return this.time_stamps;
}
//--------------------------------------------------------------------------//
Class.prototype.get_date_current_version = getD_C_VClass;
function getD_C_VClass(){
   return (this.time_stamps) ? this.time_stamps.get_date_current_version() :"";
}
//--------------------------------------------------------------------------//
Class.prototype.get_date_current_revision = getD_C_RClass;
function getD_C_RClass(){
   return (this.time_stamps) ? this.time_stamps.get_date_current_revision(): "";
}
//--------------------------------------------------------------------------//
Class.prototype.get_date_original_definition = getD_O_DClass;
function getD_O_DClass(){
   return (this.time_stamps) ? this.time_stamps.get_date_original_definition() : "";
}

//------------------------------------------------------------------------
// A valued class according to the available SCPs of the current family
// classvalues: SCPs values in the valued class
//------------------------------------------------------------------------

//function valuedclass(id, code, pref,classvalues){
function valuedclass(id, pref, classvalues){
	this.id = id;
//	this.code = code;
	this.pref = pref ;
	this.classvalues = (classvalues)? classvalues : new Array();
}

//------------------------------------------------------------------------
//valuedclass.prototype.get_code=getCodeValuedClass;
//function getCodeValuedClass(){
//	return this.code;
//}
//------------------------------------------------------------------------
valuedclass.prototype.get_preferred_name=getPreferredNameValuedClass;
function getPreferredNameValuedClass(){
	return this.pref;
}
//------------------------------------------------------------------------
valuedclass.prototype.get_id=getIdValuedClass;
function getIdValuedClass(){
	return this.id ;
}
//------------------------------------------------------------------------
valuedclass.prototype.get_class_values=getClassValuesValuedClass;
function getClassValuesValuedClass(){
	return this.classvalues ;
}

//------------------------------------------------------------------------
// Class Selector Values Constructor
//------------------------------------------------------------------------

function SelectorValue(valuecode, meaning) {
	this.valuecode = valuecode;
	this.meaning = meaning;
}
//------------------------------------------------------------------------

SelectorValue.prototype.get_value_meaning =getMeaningSelectorValue;
function getMeaningSelectorValue(){
	return this.meaning;
}
//------------------------------------------------------------------------
SelectorValue.prototype.get_value_code = getValueCodeSelectorValue;
function getValueCodeSelectorValue(){
	return this.valuecode;
}
//------------------------------------------------------------------------
// Class Selector Constructor
//------------------------------------------------------------------------

function ClassSelector(code, name, values, elt) {
	this.code = code;
	this.name = name;
	this.values = values;
	if (elt) {
		var index = elt.selectors.length;
		elt.selectors[index]= this;
	}
}
//------------------------------------------------------------------------
ClassSelector.prototype.get_code=getCodeClassSelector;
function getCodeClassSelector(){
	return this.code ;
}
//------------------------------------------------------------------------
SelectorValue.prototype.get_name =getNameClassSelector;
function getNameClassSelector(){
	return this.name;
}
//------------------------------------------------------------------------
SelectorValue.prototype.get_values =getValuesClassSelector;
function getValuesClassSelector(){
	return this.values;
}

//--------------------------------------------------------------------------//
// define property constructor
//--------------------------------------------------------------------------//
Property.prototype.bsu = BSU;
function Property (classe, code, version, names, domain, revision, pref_symbol, 
		syn_symbols, det_class, definition, note, remark, condition, dates, 
		document, figure, formula, level, prop_scope, class_id, prop_scopeCode, isExternal){
	this.bsu(code,version,classe);
	this.revision= (revision) ? revision : "001";
	this.names = (names) ? names : null ;
	this.preferred_symbol = (pref_symbol) ? pref_symbol : "" ;
	this.synonymous_symbols = (syn_symbols) ? syn_symbols : new Array() ;
	this.det_classification = (det_class) ? det_class : "";
	this.time_stamps = (dates) ? dates : null;
	this.definition = (definition) ? definition : "";
	this.note = (note) ? note : "";
	this.remark = (remark) ? remark : "";
	this.document = (document) ? document : "" ;
	this.figure = (figure) ? figure : "";
	this.domain = (domain) ? domain : null ;
	this.depend_on = (condition) ? condition : new Array();
	this.formula = (formula) ? formula : "";
	if (classe) {
		classe[code] = this;
		var index = classe.described_by.length;
		classe.described_by[index]= this;
	}
	this.level= (level) ? level : "";
	this.prop_scope = (prop_scope) ? prop_scope : "";
	this.class_id = (class_id) ? class_id : "";
	this.prop_scopeCode = (prop_scopeCode) ? prop_scopeCode : "";
	this.isExternal = (isExternal) ? isExternal : false;
}

//------------------------------------------------------------------------
Property.prototype.get_code_bsu = getCodeBSUProperty ;
function getCodeBSUProperty(){
	return this.code ;
}
//------------------------------------------------------------------------
Property.prototype.get_version = getVersionProperty ;
function getVersionProperty(){
	return this.version ;
}
//------------------------------------------------------------------------
Property.prototype.get_preferred_name = getPreferredNameProperty;
function getPreferredNameProperty(){
	return (this.names) ? this.names.get_preferred_name() : "";
}
//------------------------------------------------------------------------
Property.prototype.get_short_name = getShortNameProperty;
function getShortNameProperty(){
	return (this.names) ? this.names.get_short_name(): "";
}
//------------------------------------------------------------------------
Property.prototype.get_synonymous_names = getSynonymousNamesProperty;
function getSynonymousNamesProperty(){
	return (this.names) ? this.names.get_synonymous_names(): new Array();
}
//------------------------------------------------------------------------
Property.prototype.get_icon = getIconProperty;
function getIconProperty(){
	return (this.names)? this.names.get_icon() : "";
}
//--------------------------------------------------------------------------//
Property.prototype.get_time_stamps=getTimeStampsProperty;
function getTimeStampsProperty(){
	return this.time_stamps;
}
//--------------------------------------------------------------------------//
Property.prototype.get_date_current_version = getD_C_VProperty;
function getD_C_VProperty(){
	return (this.time_stamps) ? this.time_stamps.get_date_current_version(): "";
}
//--------------------------------------------------------------------------//
Property.prototype.get_date_current_revision = getD_C_RProperty;
function getD_C_RProperty(){
	return (this.time_stamps) ? this.time_stamps.get_date_current_revision(): "";
}
//--------------------------------------------------------------------------//
Property.prototype.get_date_original_definition = getD_O_DProperty;
function getD_O_DProperty(){
	return (this.time_stamps) ? this.time_stamps.get_date_original_definition() : "";
}
//------------------------------------------------------------------------
Property.prototype.get_definition = getDefinitionProperty;
function getDefinitionProperty(){
	return this.definition ;
}
//------------------------------------------------------------------------
Property.prototype.get_note = getNoteProperty;
function getNoteProperty(){
	return this.note ;
}
//------------------------------------------------------------------------
Property.prototype.get_remark = getRemarkProperty;
function getRemarkProperty(){
	return this.remark ;
}

//------------------------------------------------------------------------
Property.prototype.get_revision = getRevisionProperty ;
function getRevisionProperty(){
	return this.revision ;
}
//------------------------------------------------------------------------
Property.prototype.get_preferred_symbol = getPreferredSymbol ;
function getPreferredSymbol(){
	return this.preferred_symbol ;
}
//------------------------------------------------------------------------
Property.prototype.get_synonymous_symbols = getSynonymousSymbols ;
function getSynonymousSymbols(){
	return this.synonymous_symbols ;
}
//------------------------------------------------------------------------
Property.prototype.get_det_classification = getDetClassificationProperty ;
function getDetClassificationProperty(){
	return this.det_classification ;
}
//------------------------------------------------------------------------
Property.prototype.get_figure = getFigureProperty;
function getFigureProperty(){
	return this.figure;
}
//------------------------------------------------------------------------
Property.prototype.get_domain = getDomainProperty;
function getDomainProperty(){
	return this.domain;
}
//------------------------------------------------------------------------
Property.prototype.get_depend_on = getDependOnProperty;
function getDependOnProperty(){
  return this.depend_on;
}
//------------------------------------------------------------------------
Property.prototype.get_formula = getFormulaProperty;
function getFormulaProperty(){
	return this.formula;
}

//--------------------------------------------------------------------------//
// define domain constructor
//--------------------------------------------------------------------------//
function Domain(data_type, data_type_adv,elt)
{
	this.data_type = (data_type) ? data_type : "String" ;
	this.data_type_adv = (data_type_adv) ? data_type_adv : new Array();
	if (elt) elt.domain = this;
}
//------------------------------------------------------------------------
Domain.prototype.get_data_type = get_dataType;
function get_dataType(){
	return this.data_type;
}
//------------------------------------------------------------------------
Domain.prototype.get_data_type_adv = get_dataTypeAdv;
function get_dataTypeAdv(){
	return this.data_type_adv;
}
//------------------------------------------------------------------------
Domain.prototype.get_unit_value = getUnit;
function getUnit(){
	var unit = "";
	if ((this.data_type == "Integer Measure")
			|| (this.data_type == "Real Measure")
			|| (this.data_type == "Currency Integer")
			|| (this.data_type == "Currency Real")) {
		unit = this.data_type_adv[0];
	}

	if (this.data_type == "Level"){
		unit = this.data_type_adv[1];
	}

	if ((this.data_type == "Named") || (this.data_type == "Aggregate")){
		var the_data_type = this.data_type_adv[1][0];
		var the_type_adv = this.data_type_adv[1][1];
		unit = get_unit_from_datatype(the_data_type, the_type_adv);
	}
	return(unit);
}
//------------------------------------------------------------------------
//------------------------------------------------------------------------

Domain.prototype.get_domain_value = getDomainValue;
function getDomainValue(){
	return get_property_datatype(this.data_type,this.data_type_adv);
}

//------------------------------------------------------------------------

function get_unit_from_datatype(data_type, data_type_adv){
	var unit = "";
	var the_data_type = data_type;
	var the_type_adv = data_type_adv;

	if ((the_data_type == "Integer Measure")
			|| (the_data_type == "Real Measure")
			|| (the_data_type == "Currency Integer")
			|| (the_data_type == "Currency Real")) {
		unit = the_type_adv[0];
	}

	if (the_data_type == "Level"){
		unit = the_type_adv[1];
	}

	if ((the_data_type == "Named") || (the_data_type == "Aggregate")){
		the_data_type = the_type_adv[1][0];
		the_type_adv = the_type_adv[1][1];
		unit = get_unit_from_datatype(the_data_type, the_type_adv);
	}
	return(unit);
}

//------------------------------------------------------------------------

function get_unit(frame, idcl){
	var unit = "";
	var myclass = (frame.dictionary) ? frame.dictionary.classe[idcl] : frame.myclass;
	var domain = myclass.get_domain();
	if (!((domain.data_type == "Boolean")
			|| (domain.data_type == "String")
			|| (domain.data_type == "Integer")
			|| (domain.data_type == "Real")
			|| (domain.data_type == "Number")
			|| (domain.data_type == "Non Quantitative Code")
			|| (domain.data_type == "Non Quantitative Integer")
			|| (domain.data_type == "Class")
			|| (domain.data_type == "Complex"))) {
		unit = get_unit_from_datatype(domain.data_type,domain.data_type_adv);
	}
	return(unit);
}

//------------------------------------------------------------------------
// Display the summarized property datatype
//------------------------------------------------------------------------
//data_type = frame.dictionary.classe[go].data_type
//data_type_adv = frame.dictionary.classe[go].data_type_adv

function get_property_datatype(data_type, data_type_adv){
	var domain_name = "";
	var str = "";
	var the_type = data_type;
	var the_type_adv = data_type_adv;

	if (the_type == "Aggregate"){
		str += compute_aggregate_string(data_type_adv[0]);
		the_type = data_type_adv[1][0];
		the_type_adv = data_type_adv[1][1];
	}

	if (the_type != "Named"){
		if (the_type == ERROR) {
			the_type = languages.keywords.error;
		}
		str += "<A HREF='#' onClick='DataTypeSelector(self)'>" + get_datatype_name_in_current_language(the_type) + '</A>';
	} else {
		if (the_type_adv[1][0] == "Aggregate"){
			str += get_property_datatype(the_type_adv[1][0],the_type_adv[1][1])
		} else {
			str += "<A HREF='#' onClick='DataTypeSelector(self)' onDblClick=''>" + get_datatype_name_in_current_language(the_type) + '</A>';
		}
		domain_name = the_type_adv[0];
		if (domain_name != "") {
			str += ' (<I>' + domain_name + '</I>)';
		}
	}
	return(str);
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------

function get_datatype_name_in_current_language(a_type){
	var res = "";
	if (a_type == "Number") {
		res = languages.keywords.number_type;
	}
	if (a_type == "Integer") {
		res = languages.keywords.int_type;
	}
	if (a_type == "Integer Measure") {
		res = languages.keywords.int_measure_type;
	}
	if (a_type == "Integer Currency") {
		res = languages.keywords.int_currency_type;
	}
	if (a_type == "Non Quantitative Integer") {
		res = languages.keywords.non_quantitative_int_type;
	}
	if (a_type == "Real") {
		res = languages.keywords.real_type;
	}
	if (a_type == "Real Measure") {
		res = languages.keywords.real_measure_type;
	}
	if (a_type == "Real Currency") {
		res = languages.keywords.real_currency_type;
	}
	if (a_type == "Boolean") {
		res = languages.keywords.boolean_type;
	}
	if (a_type == "String") {
		res = languages.keywords.string_type;
	}
	if (a_type == "Non Quantitative Code") {
		res = languages.keywords.non_quantitative_code_type;
	}
	if (a_type == "String") {
		res = languages.keywords.string_type;
	}
	if (a_type == "Level") {
		res = languages.keywords.level_type;
	}
	if (a_type == "Named") {
		res = languages.keywords.named_type;
	}
	if (a_type == "Aggregate") {
		res = languages.keywords.aggregate_type;
	}
	if (a_type == "Class") {
		res = languages.keywords.class_instance_type;
	}
	if (a_type == "Complex") {
		res = languages.keywords.complex_type;
	}
	return res;
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------

function getSelectedProperty(frame, form){
	var idprop = getSelectedIdProperty(frame, form);

	if ((frame.dictionary)|| (frame.myclass)) {
		if (idprop) {
			return (frame.dictionary) ? frame.dictionary.classe[idprop]: frame.myclass[idprop]
		} else {
			return null;
		}
		return (frame.dictionary) ? frame.dictionary.classe[idprop]: frame.myclass[idprop];
	} else {
		return null ;
	}
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------

function getSelectedIdProperty(frame, form){
	var index = form.SelectMenu2.selectedIndex;

	if ((index>-1)&&(form.SelectMenu2)){
		return form.SelectMenu2.options[index].value;
	} else {
		return 0 ;
	}
}

//------------------------------------------------------------------------
// PopUp a table to show advanced properties characteristics
//------------------------------------------------------------------------

function DisplayPropertiesPopUp(frame, form) {
	var selprop = getSelectedProperty(frame,form);
	if (selprop) {

		var poptxt = "<TABLE id='advancedproperties' class='advproperty' WIDTH=95% ALIGN=CENTER>";
		poptxt += "<TR><TD COLSPAN=2 class='proptitle'><center>";

		if (isIE4up) {
			poptxt += "<A HREF='#' class='advproplink' onClick='SwitchAdvanced(self,document.propform)'>" + languages.keywords.advchar + "</A></center></FONT></TD></TR>";
		} else {
			poptxt += languages.keywords.advchar + "</center></TD></TR>";
		}

		if (advanced) {

            poptxt += '<TR>';

			var figure = selprop.get_figure();
			
            if ((displayPropConfigurationData["figure"]) && (figure != "")) {
				if (!selprop.isExternal) {
                var popuptitle = "Imagemax";
					poptxt += '<TD class="code">';
					poptxt += "<a href=\"#\" onClick=\"DisplayImagePopUp('" + popuptitle + "', '" + figure + "')\">";
					poptxt += '<img src="' + figure + '" width="150" height="150" border="0" alt="Simplified Drawing"></a>';
					poptxt += "</TD>";
				} else {
					poptxt += '<TD class="code"><CENTER><B><I>' + languages.keywords.extImageNotImplemented + '</I></B></CENTER></TD>';
				}
            }
			
            poptxt += '<TD class="code" width="70%" valign="top">';

            if (displayPropConfigurationData["propScope"]){
				var targetFile = "../model/family.html?family=" + selprop.class_id;
				poptxt += "<B>" + languages.keywords.scope + " : </B>";
				if (selprop.isExternal){	
					poptxt += ISO_to_chars(selprop.prop_scope) + " (<I>" + languages.keywords.externallyDefined + "</I>)";
				} else {
					poptxt += '<A href="' + targetFile + '" target="text">' + ISO_to_chars(selprop.prop_scope) + "</A>";
				}
				poptxt += "<BR/>";
			}
            if (displayPropConfigurationData["code"]){
                poptxt += "<B>" + languages.keywords.code + " : </B>" + selprop.get_code_bsu() + "<BR/>";
            }

			var version = selprop.get_version() ;
            if (displayPropConfigurationData["version"]){
				poptxt += "<B>" + languages.keywords.version + " : </B>"  + version + "<BR/>";
            }

			var revision = selprop.get_revision();
            if (displayPropConfigurationData["revision"]){
				poptxt += "<B>" + languages.keywords.revision + " : </B>"  + revision + "<BR/>";
            }

            var sh = selprop.get_short_name();
			sh = sh.replace(/^\s+|\s+$/g, '');
            if ((displayPropConfigurationData["shortName"]) && (sh != "") && (sh.toLowerCase() != NOT_TRANSLATED.toLowerCase())){
                poptxt += "<B>" + languages.keywords.sh_name + " : </B>"  + sh + "<BR/>";
            }

		    var syn_names = selprop.get_synonymous_names();
            if ((displayPropConfigurationData["synonymousNames"]) && (syn_names.length > 0)){
        		var val_syn = syn_names[0].replace(/^\s+|\s+$/g, '');
				if ((val_syn != "") && (val_syn.toLowerCase() != NOT_TRANSLATED.toLowerCase())){
					poptxt += "<B>" + languages.keywords.syn_name + ": </B>" + val_syn + "<BR/>";
				}
            }

            var symb = selprop.get_preferred_symbol();
			symb = symb.replace(/^\s+|\s+$/g, '');
            if ((displayPropConfigurationData["symbol"]) && (symb != "")){				
                 poptxt += "<B>" + languages.keywords.symb + " : </B>"  + symb + "<BR/>";
            }

		    var syn_symbs = selprop.get_synonymous_symbols();
            if ((displayPropConfigurationData["synonymousSymbols"]) && (syn_symbs.length > 0)){
        		var val_symb = syn_symbs[0];
				val_symb = val_symb.replace(/^\s+|\s+$/g, '');
				if (val_symb != ""){
					poptxt += "<B>" + languages.keywords.syn_symb + ": </B>" + val_symb + "<BR/>";
				}
            }

			var d_o_d = selprop.get_date_original_definition();
			d_o_d = d_o_d.replace(/^\s+|\s+$/g, '');
			var d_c_v = selprop.get_date_current_version();
			d_c_v = d_c_v.replace(/^\s+|\s+$/g, '');
			var d_c_r = selprop.get_date_current_revision();
			d_c_r = d_c_r.replace(/^\s+|\s+$/g, '');

            if (displayPropConfigurationData["originalDefinition"]){
				if (d_o_d != ""){
					poptxt += "<B>" + languages.keywords.d_o_d + " : </B>" + d_o_d + "<BR/>";
				}
            }

            if ((displayPropConfigurationData["currentVersion"]) && (d_c_v != "")){
				if (d_c_v != ""){
					poptxt += "<B>" + languages.keywords.d_c_v + " : </B>" + d_c_v + "<BR/>";
				}
            }

            if ((displayPropConfigurationData["currentRevision"]) && (d_c_r != "")){
				if (d_c_r != ""){
					poptxt += "<B>" + languages.keywords.d_c_r + " : </B>" + d_c_r + "<BR/>";
				}
            }

            var formula = selprop.formula;
			formula = formula.replace(/^\s+|\s+$/g, '');
            if ((displayPropConfigurationData["formula"]) && (formula != "")){
				poptxt += "<B>" + languages.keywords.formula + " : </B>" + formula + "<BR/>";

            }

            var ptc = selprop.get_det_classification();
			ptc = ptc.replace(/^\s+|\s+$/g, '');
            if ((displayPropConfigurationData["propertyTypeClassification"]) && (ptc != "")){
				poptxt += "<B>" + languages.keywords.proptypecl + " : </B>" + ptc + "<BR/>";
            }
            poptxt += "</TD>";

            poptxt += "</TR>";

		}
		poptxt += "</TABLE>";

		if (isIE4up) {
			if (frame.document.all.advancedproperties){
				frame.document.all.advancedproperties.outerHTML = poptxt;
			} else {
				if (advanced) {
					DisplayPopUp("Advanced", poptxt, keywords.advchar, screen.width*98/100, 200, "yes", '', '');
				}
			}
		}
	}

}
//------------------------------------------------------------------------
// Display the characteristics of a given property
//------------------------------------------------------------------------

function DisplayProperty(frame, form, bool){
	var prop = getSelectedProperty(frame,form);

	if (prop){
		setCookie('lastSelectedProperty.cookie', prop.get_code_bsu());
	} else {
//		setCookie('lastSelectedProperty.cookie', "");
	}

	if (prop){

		if (isIE4up) {
			var str = "<TABLE id='propertycharacteristics' class='property' WIDTH=100% HEIGHT=100%>";
			
			var domain = prop.get_domain();
			if (displayPropConfigurationData["domain"]){
				str += "<TR HEIGHT='25%'>";
				str += "<TD class='propcell' WIDTH='100%'><B>" + languages.keywords.data + " : </B>";
				str += get_property_datatype(domain.data_type,domain.data_type_adv);
				str += "</TD>";
				str = str + "</TR>";
            }

			var definition = prop.get_definition();
			definition = definition.replace(/^\s+|\s+$/g, '');
			if (displayPropConfigurationData["definition"]){
				if ((definition != "") && (definition.toLowerCase() != NOT_TRANSLATED.toLowerCase())){
					str += "<TR HEIGHT='25%'><TD class='propcell' COLSPAN=4><B>" + languages.keywords.def + " : </B>" +
						definition + "</TD></TR>";
				}
            }

			var note = prop.get_note();
			note = note.replace(/^\s+|\s+$/g, '');
			if ((displayPropConfigurationData["note"]) && (note != "") && (note.toLowerCase() != NOT_TRANSLATED.toLowerCase())){
				str = str + "<TR HEIGHT='25%'><TD class='propcell' COLSPAN=4>";
				str = str + "<B>" + languages.keywords.note+ " : </B>" + note ;
				str = str + "</TD></TR>";
            }

			var remark = prop.get_remark();
			remark = remark.replace(/^\s+|\s+$/g, '');
			if ((displayPropConfigurationData["remark"]) && (remark != "") && (remark.toLowerCase() != NOT_TRANSLATED.toLowerCase())){
				str = str + "<TR HEIGHT='25%'><TD class='propcell' COLSPAN=4>";
				str = str + "<B>"+languages.keywords.remark + " : </B>" + remark ;
				str = str + "</TD></TR>";
			}

			str = str + "</TABLE>";
			frame.document.all.propertycharacteristics.outerHTML = str;

		} else {
			frame.document.propertycharacteristics.symbol.value = prop.get_code_bsu();
			frame.document.propertycharacteristics.data.value = prop.get_domain_value();
			frame.document.propertycharacteristics.unit.value = prop.get_unit();
			frame.document.propertycharacteristics.version.value = prop.get_version ()+ "." + prop.get_revision();
			frame.document.propertycharacteristics.def.value = prop.get_definition();
			frame.document.propertycharacteristics.note.value = prop.get_note();
			frame.document.propertycharacteristics.remark.value = prop.get_remark();
		}
		if (bool) {
			DisplayPropertiesPopUp(frame, form);
		}
	} else {
		if (isIE4up) {
			var str = "<TABLE id='propertycharacteristics' class='property' WIDTH=100% HEIGHT=100%>";
			str = str + "</TABLE>";
			frame.document.all.propertycharacteristics.outerHTML = str;
		}
	}
}

//------------------------------------------------------------------------
// PopUp a window to show explaination and details (str=html body)
//------------------------------------------------------------------------

function DisplayPopUp(name, str, title, width, height, scrollbar, stylesheet, script, autoresize){

	var doc ='<HTML>';
	doc += '<HEAD><TITLE>' + title + '</TITLE>';
	doc += '<LINK rel="stylesheet" type="text/css" href="./datatype.css">';
	if (script != ""){
		doc += script;
	}

    if (autoresize) {
        doc += '<SCRIPT>';
        doc += 'function redim() {';
        doc += 'ok=0;';
        doc += 'if (document.all) {';
        doc += '   document.body.scrollLeft=2;';
        doc += '   if ((document.body.scrollLeft>0) && (ww<(screen.width-step))) {';
        doc += '      ww+=step;';
        doc += '      self.window.resizeTo(ww,hh);';
        doc += '      ok=1; ';
        doc += '   }       ';
        doc += '   document.body.scrollTop=2;     ';
        doc += '   if (document.body.scrollTop>0) { ';
        doc += '      hh+=step;  ';
        doc += '      self.window.resizeTo(ww,hh);  ';
        doc += '      ok=1;                      ';
        doc += '   }                              ';
        doc += '   if (ok==1) {setTimeout("redim();",10)}; ';
        doc += '}         ';
        doc += '}    ';

        doc += '</SCRIPT>';

    }
	doc += '</HEAD>';
	doc += '<BODY>';
	doc += '<FONT face="Helvetica, Helv, Arial">';

	var top  = (screen.height - height) / 2;
	var left = (screen.width  - width)  / 2;

	if (popupexiste[name] == true) {
		//Closes it first if not done
		if (window[name].closed == false) {
			window[name].close();
		}
		// then opens it to create the popup effect
		window[name] = window.open("",
			name, "toolbar=no, location=no, directories=no, status=no, scrollbars="
			+ scrollbar + ", copyhistory=no, resizable=yes, height="
			+ height + ", width=" + width + ", left="+ left +", top=" +top);
	} else {
	// Creates the PopUp Window
		window[name] = window.open("",
		   	name, "toolbar=no, location=no, directories=no, status=no, scrollbars="
			+ scrollbar + ", copyhistory=no, resizable=yes, height="
			+ height + ", width=" + width + ", left="+ left +", top=" +top);
		popupexiste[name] = true;
	}

	doc += str;
	doc += "</FONT>";

    if (autoresize) {

        doc += '<script language="Javascript">';
        doc += 'var ww=30;';
        doc += 'var hh=50;';
        doc += 'var step=15;';
        doc += 'if (document.all) {';
        doc += '   x=document.body.scrollWidth;';
        doc += '   ww=(x<(screen.width-step) && x>ww) ? x : ww;';
        doc += '   x=document.body.scrollHeight;';
        doc += '   hh=(x<(screen.height-step) && x>hh) ? x : hh;';
        doc += '   redim();';
        doc += '}else if (document.layers) {';
        doc += '   ww=(document.width>ww) ? (document.width+10):ww;';
        doc += '   hh=(document.height>hh) ? (document.height+10):hh;';
        doc += '   setTimeout("self.window.resizeTo(ww,hh);",100);';
        doc += '}  ';
        doc += '</script> ';
    }
    doc += "</BODY></HTML>";

	window[name].document.open();
	window[name].document.write(doc);
}

//------------------------------------------------------------------------
// PopUp a window to show Class Selectors under NS4
//------------------------------------------------------------------------

function DisplayPopUpSel(frame,name,str,title,width,height) {

//	if (seltable==1) {
		window[name] = window.open("",name,"location=no,status=yes,scrollbars=yes,resizable=yes,height="+height+",width="+width+",left="+(screen.width/2)+",top="+(screen.height/2)+"");
		window[name].focus();
		window[name].document.open();
		window[name].document.writeln('<HTML><HEAD>');
		window[name].document.writeln('<TITLE>'+title+'</TITLE>');
		window[name].document.writeln('<SCRIPT src="../runtime/cookies.js"></SCRIPT>');
		window[name].document.writeln('<LINK rel="stylesheet" type="text/css" href="./selectors.css">');
		window[name].document.writeln('<SCRIPT language="JavaScript">');
		window[name].document.writeln('');

		// Constructors generation ...
//		window[name].document.writeln('function valuedclass(id, code, pref, classvalues){');
		window[name].document.writeln('function valuedclass(id, pref, classvalues){');
		window[name].document.writeln('this.id = id;');
//		window[name].document.writeln('this.code = code;');
		window[name].document.writeln('this.pref = pref;');
		window[name].document.writeln('this.classvalues = classvalues;}');

		window[name].document.writeln('function SelectorValue(valuecode, meaning) {');
		window[name].document.writeln('this.valuecode = valuecode;');
		window[name].document.writeln('this.meaning = meaning;}');

		window[name].document.writeln('function ClassSelector(code, name, values) {');
		window[name].document.writeln('this.code = code;');
		window[name].document.writeln('this.name = name;');
		window[name].document.writeln('this.values = values;}');

		// Tools generation ...
		window[name].document.writeln('');
		window[name].document.writeln('function Compute(form){');
		window[name].document.writeln('var val, ind; var nb = 0; var l; var found = true; var trouve = false; var res = new Array();');
		window[name].document.writeln('for (var i = 0; i <= (selection.length - 1); i++){');
		window[name].document.writeln('	for (var j = 0; j <= (selectors.length - 1); j++){');
		window[name].document.writeln('		ind = form[selectors[j].code].selectedIndex;');
		window[name].document.writeln('		val = form[selectors[j].code].options[ind].value;');
		window[name].document.writeln('		if (val != ""){');
		window[name].document.writeln('            if (selection[i].classvalues) {');
		window[name].document.writeln('				for (var k = 0; k <= (selection[i].classvalues.length - 1); k++){');
		window[name].document.writeln('					if (selection[i].classvalues[k] == val) {');
		window[name].document.writeln('						trouve = true;');
		window[name].document.writeln('					}');
		window[name].document.writeln('				}');
		window[name].document.writeln('            }');
		window[name].document.writeln('			found = found && trouve;trouve = false;');
		window[name].document.writeln('		}');
		window[name].document.writeln('	}');
		window[name].document.writeln('	if (found){');
		window[name].document.writeln('		res[nb] = selection[i];');
		window[name].document.writeln('		nb++;');
		window[name].document.writeln('	}');
		window[name].document.writeln('	found = true;');
		window[name].document.writeln('}');
		window[name].document.writeln('	Update(res,selectors.length);');
		window[name].document.writeln('}');

		if (isIE4up) {
			window[name].document.writeln('');
			window[name].document.writeln('function Update(res,lg){');
			window[name].document.writeln('var str = "";');
			window[name].document.writeln('str = "<SELECT id=result size="+(lg*2)+" onClick=setGo(this)>";');
			window[name].document.writeln('if(res.length == 0){');
			window[name].document.writeln('str = str + "<OPTION>** ' + languages.keywords.clResults + ' **</OPTION>"	}');
			window[name].document.writeln('for (var i = 0; i <= (res.length - 1); i++) {');
			window[name].document.writeln('str = str + "<OPTION  value=tmp.html>";');
			window[name].document.writeln('str = str + res[i].pref;');
			window[name].document.writeln('str = str + "</OPTION>";}');
			window[name].document.writeln('str = str + "</SELECT>";');
			window[name].document.writeln('document.all["result"].outerHTML = str;');
			window[name].document.writeln('if (res.length == 1){document.clselectors.sel.value = res[0].pref;');
			window[name].document.writeln('} else {document.clselectors.sel.value = "";}}');
			window[name].document.writeln('');

/*
			window[name].document.writeln('function getCodeFrame(){');
			window[name].document.writeln('	var codeFrame;');
			window[name].document.writeln('	codeFrame = window.opener.top.frames[\'code\'];');
			window[name].document.writeln('	return(codeFrame);');
			window[name].document.writeln("}");
			window[name].document.writeln("");
			window[name].document.writeln('function findSubMenuRef(classCode){');
			window[name].document.writeln('	var menuRef = getCodeFrame().menu;');
			window[name].document.writeln('	getCodeFrame().searchHighlight(menuRef, classCode, \'casesensitive\', \'code\');');
			window[name].document.writeln("}");
			window[name].document.writeln("\n");
*/
			window[name].document.writeln('function setGo(selmenu){');
			window[name].document.writeln('var id = selmenu.selectedIndex;');
			window[name].document.writeln('var go = selmenu.options[id].text;');
			window[name].document.writeln('document.clselectors.sel.value = go;');
			window[name].document.writeln('}');

		} else {
			window[name].document.writeln('function Update(res,lg){var str = "";');
			window[name].document.writeln('for (var i = 0; i <= (res.length - 1); i++) {');
			window[name].document.writeln("str = str + res[i].pref+'\\n';}");
			window[name].document.writeln('if (str == ""){	str = "**NO CLASSES**";}');
			window[name].document.writeln('document.clselectors.maZone.value = str;');
			window[name].document.writeln('if (res.length == 1){var tmp = new String(str);');
			window[name].document.writeln('document.clselectors.sel.value = tmp.substring(0, tmp.length - 1);');
			window[name].document.writeln('} else {document.clselectors.sel.value = "";}');
			window[name].document.writeln('}');
		}

		window[name].document.writeln('');
		window[name].document.writeln('function go(){var URL;var i = 0;');
		window[name].document.writeln('	var class_name = document.clselectors.sel.value;var trouve = false;');
		window[name].document.writeln('	if (class_name != "") {while ((i <= (selection.length - 1)) && (! trouve)){');
		window[name].document.writeln('		if (selection[i].pref == class_name) {');
		window[name].document.writeln('			trouve = true;');
		window[name].document.writeln('		} else {');
		window[name].document.writeln('			i++;');
		window[name].document.writeln('		}');
		window[name].document.writeln('	}');
		window[name].document.writeln('	if (trouve) {');
		window[name].document.writeln('		URL = "../model/family.html?family=" + selection[i].id;');
		window[name].document.writeln('		window.opener.top.frames.text.location.href = URL;');
		window[name].document.writeln('		setCookie("seltable.cookie", 1);');
		window[name].document.writeln('		window.close();');
		window[name].document.writeln('	} else {');
		window[name].document.writeln('		alert("Class " + class_name + " does not exist");}');
		window[name].document.writeln('	}');
		window[name].document.writeln('}');

		// G�n�ration des selecteurs et classes valu�es...
/* EXAMPLE:
selection =
	new Array(
                             clId            clPref     Selectors
		new valuedclass("TEST_CodeC121_001", "C121", new Array("B")),
		new valuedclass("TEST_CodeC111_001", "C111", new Array("A"))
	);
*/
		window[name].document.writeln('selection = new Array(');

		var selection = (frame.selection) ? frame.selection : new Array();

		for (i = 0; i <= selection.length - 1; i++) {
			window[name].document.write('new valuedclass(');
			window[name].document.write('"' + selection[i].get_id());
			window[name].document.write('", ');
//			window[name].document.write('"' + selection[i].get_code());
//			window[name].document.write('", "');
            window[name].document.write('"');
			window[name].document.write(ISO_to_chars(selection[i].get_preferred_name().toString()));
			// window[name].document.write(get_text(selection[i].pref));
			window[name].document.write('", new Array(');

			for (j = 0; j <= selection[i].get_class_values().length - 1; j++) {
//				window[name].document.writeln('');
				window[name].document.write('"' + selection[i].get_class_values()[j]);

				if (j < (selection[i].get_class_values().length - 1)) {
					window[name].document.write('", ');
				} else {
					if (i < (selection.length-1)) {
						window[name].document.writeln('")),');
					} else {
						window[name].document.writeln('"))');
					}
				}
			}
		}

/* EXAMPLE:
selectors =
	new Array(
		new ClassSelector("CodeP1_001", "P1",
			new Array(
				new SelectorValue("A","A"),
				new SelectorValue("B","B"),
				new SelectorValue("C","C")
			)
		)
	);
*/
		window[name].document.writeln(');');

		window[name].document.writeln('selectors = new Array(');

		var myclass =(frame.dictionary)? frame.dictionary.get_class():frame.myclass ;
		var selectors = (myclass) ? myclass.get_selectors(): new Array();

		for (i=0; i<=selectors.length-1; i++) {
			window[name].document.writeln('new ClassSelector(');
			window[name].document.write('"'+selectors[i].code+'", ');

			window[name].document.write('"'+selectors[i].name+'", new Array(');

			for (j=0; j<=selectors[i].values.length-2; j++) {
				window[name].document.writeln('new SelectorValue("'+selectors[i].values[j].get_value_code()+'", ');
				window[name].document.write('"'+ selectors[i].values[j].get_value_meaning()+'"), ');
			}

			window[name].document.writeln('new SelectorValue("'+ selectors[i].values[selectors[i].values.length-1].get_value_code()+'", ');
			window[name].document.write('"'+ selectors[i].values[selectors[i].values.length-1].get_value_meaning()+'")))');

			if (i == (selectors.length-1)) {
				window[name].document.writeln(');');
			} else {
				window[name].document.writeln(', ');
			}
		}

		// Fin des g�n�rations...

		window[name].document.writeln('</SCRIPT></HEAD><BODY background="runtime/'+bkgdimg+'">');
		window[name].document.writeln(str+"</BODY></HTML>");
		window[name] = null;
//	}
}

//------------------------------------------------------------------------
// PopUp the Class Selectors
//------------------------------------------------------------------------

function DisplaySelectorsPopUp(frame) {

	var table = (frame.selection) ? frame.selection : new Array();
	var str = '<TABLE WIDTH=100% id="ClassSelectors" class="selector">';
	var height = 300;
	str += "<TR>\n";
	str += '<TD HEIGHT=10% class="proptitle" COLSPAN=3 ALIGN=CENTER><FONT size=+2>';
	str += languages.keywords.clsel + "</FONT></TD>\n";
	str += "</TR>\n";

//	if (seltable) {
		str += "<TR HEIGHT='40'>\n";
		str += "<TD CLASS='header' WIDTH=66%><CENTER><B>" + languages.keywords.name + "</B></CENTER></TD>";
		str += "<TD CLASS='header' WIDTH=33%><CENTER><B>" + languages.keywords.result + "</B></CENTER></TD>\n";
		str += "</TR>\n";
		str += "<FORM NAME='clselectors'>\n";

		var myclass =(frame.dictionary)? frame.dictionary.get_class():frame.myclass ;
		var selectors = (myclass) ? myclass.get_selectors(): new Array();

		for (i = 0; i <= selectors.length - 1; i++) {
			if (i > 2) {
				height += 40;
			}

			str += "<TR HEIGHT='40'>\n";
			str += "<TD CLASS='aselector'>";
			str += "<TABLE WIDTH=100%>\n";
			str += "<TR HEIGHT='40'>\n";

			str += "<TD WIDTH=50% ALIGN=CENTER class='seltitle' novrap><B>" +  selectors[i].name + "</B></TD>\n";
			str += "<TD WIDTH=50% ALIGN=CENTER>\n";
			str += '<SELECT NAME="' + selectors[i].code + '" onChange="Compute(this.form)">' + "\n";

			str += "<OPTION>" + languages.keywords.none + "</OPTION>\n";

			for (j = 0; j <= selectors[i].values.length - 1; j++) {
				str += '<OPTION VALUE="' + selectors[i].values[j].get_value_code()+ '">' + selectors[i].values[j].get_value_meaning ()+ "</OPTION>\n";
			}
			str += "</SELECT>\n";
			str += "</TD>\n";
			str += "</TR>\n";
			str += "</TABLE>\n";
			str += "</TD>\n";

			if (i == 0) {
				str += "<TD CLASS='result' ROWSPAN=" + selectors.length + " ALIGN=CENTER>\n";
				str += "<TABLE WIDTH=100% HEIGHT=100%>\n";
				str += "<TR>\n";
				str += "<TD ALIGN=CENTER>\n";
				if (isIE4up) {
					str += '<SELECT ID="result" SIZE=' + (selectors.length * 2) + ' onChange="setGo(this)">' + "\n";
					for (k = 0; k <= table.length - 1; k++) {
						str += "<OPTION>" + ISO_to_chars(table[k].get_preferred_name().toString()) + "</OPTION>\n";
					}

					str += "</SELECT>\n";
				} else {
					str += '<TEXTAREA name="maZone" cols="30" rows="4">' + "\n";
					for (k = 0; k <= selection.length - 1; k++) {
						str += selection[k].get_preferred_name() + "\n";
					}
					str += "</TEXTAREA>\n";
				}

				str += "</TD>\n";
				str += "</TR>\n";
				str += "<TR>\n";
				str += "<TD ALIGN=CENTER>\n";

				if (isIE4up) {
					str += '<INPUT TYPE="HIDDEN" NAME="sel" SIZE="15">' + "\n";
				} else {
					str += '<INPUT TYPE="TEXT" NAME="sel" SIZE="30">' + "\n";
					str += "</TD>\n";
					str += "</TR>\n";
					str += "<TR>\n";
					str += "<TD ALIGN=CENTER>" + "\n";
				}

				str += '<INPUT TYPE="BUTTON" NAME="Go" VALUE="' + languages.keywords.select + '" onClick="go()">' + "\n";
				str += "</TD>\n";
				str += "</TR>\n";
				str += "</TABLE>\n";
				str += "</TD>\n";
				str += "</TR>\n";
			}
		}

		str += "</FORM>\n";
		str += "</TABLE>\n";
		DisplayPopUpSel(frame,"Selector", str, languages.keywords.clsel, 750, height);
//	}
}

//------------------------------------------------------------------------
// Switch the state of the Advanced Table
//------------------------------------------------------------------------
function SwitchAdvanced(frame,form) {
	if (advanced) {
		advanced = 0;

		if (isIE4up == false && window["Advanced"].closed == false) {
			window["Advanced"].close();
		}
	} else {
		advanced = 1;
	}
	setCookie("advanced.cookie", advanced);
	DisplayPropertiesPopUp(frame, form);
}

//------------------------------------------------------------------------
// Switch the state of the Selectors Table
//------------------------------------------------------------------------
function SwitchSelectors(frame,name) {
	var frame = (frame) ? frame : self ;
	if (seltable) {
		seltable = 0;
		setCookie("seltable.cookie", seltable);
		DisplaySelectorsPopUp(frame);
//		if (window[name]){
//			window[name].close();
//		}
	} else {
		if (window[name] == null){
			DisplaySelectorsPopUp(frame);
		}
//	setCookie("seltable.cookie",seltable);
//	DisplaySelectorsPopUp(frame);
        }
}

//------------------------------------------------------------------------
// PopUp the picture
//------------------------------------------------------------------------

function DisplayImagePopUp(title, filename) {
	var str = "";
	var img = new Image();
	img.src = filename
	var width = img.width-40;
	var height = img.height-40;
	var r1 = img.height/ img.width ;

	if (r1>0.75){
		if (img.height>screen.height){
			var r = screen.height/img.height;
			width = img.width * r;
			height = screen.height;
		}
	} else {
		if (img.width>screen.width){
			var r = screen.width/img.width;
			height = img.height * r;
			width = screen.width;
		}
	}

	str += "<DIV align='center'><IMG id='imageId' src='" + filename + "' width='"+width+"' height='"+height+"' border=0 >";
    str += "<BR/><BR/>";
    str += "<a href='#' onclick='zoom(\"-\",\"imageId\"," + (img.height / img.width) + ")'>Zoom --</a> |";
	str += "<a href='#' onclick='zoom(\"+\",\"imageId\"," + (img.height / img.width) + ")'>Zoom ++</a> ";
	str += "</DIV>";

	var style = "<LINK  rel='stylesheet' type='text/css' href='drawing.css'>\n";

    var script = "<SCRIPT>\n";
    script += "function findDOM(objectId) {\n";
    script += "if (document.getElementById) {\n";
    script += "return (document.getElementById(objectId));}\n";
    script += "if (document.all) {\n";
    script += "return (document.all[objectId]);}\n";
    script += "}\n";
    script += "\n";
    script += "function zoom(type,imgx,sz) {\n";
    script += "var imgd = findDOM(imgx);\n";
    script += "var deltaWidth = 100;\n";
    script += "if (type=='+' && imgd.width < screen.width) {\n";
    script += "imgd.width += deltaWidth;imgd.height += (deltaWidth*sz);\n";
    script += "redim();\n";
	script += "}\n";
    script += "if (type=='-' && imgd.width > 50) {\n";
    script += "imgd.width -= deltaWidth;imgd.height -= (deltaWidth*sz);}\n";
    script += "}\n";
    script += "</SCRIPT>\n";

//function DisplayPopUp(name, str, title, width, height, scrollbar, stylesheet, script, autoresize){
	DisplayPopUp(title, str, languages.keywords.simplified_drawing, width + 100, height+40, 'yes', style, script, true);
}

//------------------------------------------------------------------------
// Data type selector
//------------------------------------------------------------------------

function DataTypeSelector(frame) {
	var prop = getSelectedProperty(frame,frame.document.propform);
	DataTypePopUp(prop.domain.data_type,prop.domain.data_type_adv);
}

//------------------------------------------------------------------------
// PopUp the data type
//------------------------------------------------------------------------

function DataTypePopUp(data_type, data_type_adv) {
	var str = '';
	var height = 40;
	var width = 400;
	var scrollbar = "no";
	var i;
	var script = "";

	if (data_type == ERROR){
		str = error_datatype();
		width = 200; height = 20;
	}

	if ((data_type == "Boolean") || (data_type == "String")	|| (data_type == "Integer")
			|| (data_type == "Real") || (data_type == "Number")){
		str = simple_datatype_popup(data_type_adv[0]);
		width = 200; height = 20;
	}

	if ((data_type == "Non Quantitative Code") || (data_type == "Non Quantitative Integer")) {
		str = enumeration_datatype_popup(data_type_adv[0], data_type_adv[1]);
		width = 350; height = 30 + data_type_adv[1].length * 30;
		if (height == 0) {
			height = 150
		}
		if (height > 600) {
			height = 600
		}
		scrollbar = "yes";
	}

	if ((data_type == "Integer Measure") || (data_type == "Real Measure") || (data_type == "Currency Integer")
			|| (data_type == "Currency Real")) {
// ES: dans le generateur, changer la structure de data_type_adv:
// Array(value_format, Array(Array(codei, namei, iconi), ..., Array(codej, namej, iconj)))
		str = measure_or_currency_datatype_popup(data_type_adv[0], data_type_adv[1]);
		width = 350; height = 40;
	}

	if (data_type == "Level"){
		str = level_datatype_popup(data_type_adv[0], data_type_adv[1], data_type_adv[2], data_type_adv[3]);
		width = 350; height = 175;
	}

	if (data_type == "Class"){
//		script = getClassInstanceScript();
		str = class_instance_datatype_popup(data_type_adv[0], data_type_adv[1], data_type_adv[2]);
		width = 350; height = 40;
	}

	if (data_type == "Named"){
		str = named_datatype_popup(data_type_adv[1][0], data_type_adv[1][1]);
		width = 350; height = 40;
	}

	if (data_type == "Aggregate"){
		str = aggregate_datatype_popup(data_type_adv[1]);
		width = 350; height = 40;
	}

	if (data_type == "Complex"){
		str = not_implemented();
		width = 350; height = 40;
	}
//ES: 2006-05-11
	if (data_type == "Unknown"){
		str = not_defined();
		width = 350; height = 40;
	}

	if ((data_type != "Named") && (data_type != "Aggregate")){
		DisplayPopUp("DataType", str, languages.keywords.datatypeinf, width, height, scrollbar, datatype_stylesheet(), script, true);
	}
}

//
//ES: 2006-05-11
//

function not_defined(){
	var str = '<TABLE border="0" width="100%">\n';
	str += '<TR><TH>'+ languages.keywords.not_implemented + '</TH>\n';
	str += '</TR></TABLE>\n';
	return str;
}

//
//
//

function getClassInstanceScript(){
	var script = "<SCRIPT src='../runtime/cookies.js'></SCRIPT>\n";
	script += "\n";

	script += "<SCRIPT>\n";

	script += "function getCodeFrame(){\n";
	script += "	var codeFrame;\n";
	script += "	codeFrame = window.opener.top.frames['code'];\n";
	script += "	return(codeFrame);\n";
	script += "}\n";
	script += "\n";

	script += "function findSubMenuRef(classCode){\n";
	script += "	var menuRef = getCodeFrame().menu;\n";
	script += "	getCodeFrame().searchHighlight(menuRef, classCode, 'casesensitive', 'code');\n";
	script += "	window.close()";
	script += "}";
	script += "\n";

	script += "</SCRIPT>";
	return script;
}
//------------------------------------------------------------------------
// No data type defined: error
//------------------------------------------------------------------------

function error_datatype(){

	var str = '<TABLE border="0" width="100%">\n';
	str += '<TR><TH><IMG src="menu-images/interdit.gif"></TH><TH>' + languages.keywords.no_datatype + ' !!</TH>';
	str += '</TR></TABLE>\n';
	return str;
}

//------------------------------------------------------------------------
// Style sheet for data types
//------------------------------------------------------------------------

function datatype_stylesheet(){
	var style = ''

	style += '<style><!--\n';

	style += 'table   {padding: 2em;\n';
	style += '			border: 2px dashed #CCCCCC;\n';
	style += '			font-size: 8pt;\n';
	style += '}\n';

	style += 'th      {text-align: center;   background-color: #CCFFFF;\n';
	style += '			padding: 0.7em; border: 0 solid black;  font-family: arial;\n';
	style += '}\n';

	style += 'td      {text-align: center;   background-color: #CCFFCC;\n';
	style += '     		padding: 0.7em; border: 0 solid black;  font-family: arial;\n';
	style += '}\n';

	style += '.code   {text-align: center;   background-color: #FFFFCC;\n';
	style += '		    padding: 0.7em; border: 0 solid black;  font-family: arial;\n';
	style += '}\n';

	style += '--></style>\n';
	return(style)
 }

//------------------------------------------------------------------------
// Functions for data types
//------------------------------------------------------------------------


function datatype_functions(){
	var code = "";
	code += "\n";
	code += "function OpenWindow(doc, WinName){ \n";
	code += "	var my_window; \n";
	code += "	var img = new Image(); \n";
	code += "	var the_width = 0 \n";
	code += "	var the_height = 0 \n\n";

	code += "	img.src = doc; \n";
	code += "	the_width = img.width + 50; \n";
	code += "	the_height = img.height + 50; \n";
	code += " 	my_window = window.open(doc, WinName, 'toolbar=no, location=no, directories=no, status=no'); \n";
	code += "	my_window.resizeTo(the_width, the_height)\n";

	code += "}\n";
	code += "\n";

	code += "function myPopUp(title, doc) { \n";
	code += "	OpenWindow(doc, title);";
	code += "} \n\n";

	return(code);
}

//------------------------------------------------------------------------
// Display a data type that is not implemented.
//------------------------------------------------------------------------

function not_implemented(){
	var str = '<TABLE border="0" width="100%">\n';
	str += '<TR><TH>'+ languages.keywords.not_implemented + '</TH>\n';
	str += '</TR></TABLE>\n';
	return str;
}

//------------------------------------------------------------------------
// Display a very simple data type.
//------------------------------------------------------------------------

function simple_datatype_popup(value_format) {
	var str = '<TABLE border="0" width="100%">\n';

	str += '<TR><TH>' + languages.keywords.valfor + ' :</TH><TD>';
	if (value_format != "") {
		str += value_format;
	} else {
		str += languages.keywords.unavailable;
	}

	str += '</TD></TR></TABLE>\n';
	return str;
}


//------------------------------------------------------------------------
// Enumeration sort
//------------------------------------------------------------------------

function enumeration_sort(a, b){
	var str_a = a[0];
	var str_b = b[0];

	if (str_a < str_b) {return(-1)}
	if (str_a > str_b) {return(1)}
	if (str_a = str_b) {return(0)}
}

//------------------------------------------------------------------------
// Display an enumeration data type
//------------------------------------------------------------------------

function enumeration_datatype_popup(value_format, codes) {
	var str = '';
	var i;
	var img = '';
	var popuptitle = "Imagemax";
	var unknown_icon = "menu-images/no_icon.gif"

	// codes.sort(enumeration_sort);

	str += '<TABLE border="0" width="300">\n';
	str += '<TR><TH>' + languages.keywords.valfor + ' :</TH><TD>\n';
	if (value_format != '') {
		str += value_format;
	} else {
		str += languages.keywords.unavailable;
	}
	str += '</TD></TR></TABLE>\n';

	str += '<script>\n';
	str += datatype_functions();
	str += '</script>\n';

	str += '<TABLE border="1" width="300">\n';
	str += '<TR><TH>' + languages.keywords.code + '</TH>\n';
	str += '<TH>' + languages.keywords.def + '</TH>\n';
	str += '<TH>' + languages.keywords.icon + '</TH></TR>\n';

	if (codes.length > 0) {
		for (i = 0; i <= codes.length - 1; i++){
			str += '<TR><TD class="code">\n';
			// the code
			str += codes[i][0];
			str += '</TD><TD>\n';
			//the translated name
			str += codes[i][1];
			str += '</TD><TD>\n';
			//the possible icon
			img = codes[i][2];
			if (img != ''){
				if (img != ERROR){
					str += ("<a href=\"javascript:myPopUp('" + popuptitle + "', '" + img + "')\">");
				} else {
					img = unknown_icon;
				}
				str += '<img src="' + img + '" width=20 height=20 border=0 alt="Icon">';

				if (img != ERROR){
					str += '</a>';
				}
			}
			str += '</TD></TR>\n';
		}
	} else {
		str += '<TR><TD colspan=3 style="text-decoration:blink; color:red">' + languages.keywords.unavailable + '</TD></TR>\n';
	}
	str += '</TABLE>\n';
	return str;
}

//------------------------------------------------------------------------
// Display a measure data type.
//------------------------------------------------------------------------

function measure_or_currency_datatype_popup(unit, value_format){
	var str = '';

	str += '<TABLE border="0" width="300">\n';
	str += '<TR><TH>' + languages.keywords.valfor + ' :</TH><TD>';

	if (value_format != '') {
		str += value_format;
	} else {
		str += languages.keywords.unavailable;
	}
	str += '</TD></TR>\n';
	str += '<TR><TH>' + languages.keywords.unit + ' :</TH><TD>';

	if (unit != ERROR){
		str += unit;
	} else {
		str += '<IMG src="menu-images/interdit.gif" width="30" height="30">'
	}
	str += '</TD></TR>\n';
	str += '</TABLE>';

	return str;
}

//------------------------------------------------------------------------
// Display a level type
//------------------------------------------------------------------------

function level_datatype_popup(nature, unit, value_format, levels){
	var str = '';
	var i;

	str += '<TABLE border="0" width="300">\n';
	str += '<TR>';
	// levels
	for(i = 0; i <= levels.length - 1; i++){
		str += '<TD class="code">' + levels[i] +'</TD>';
	}
	str += '</TR>\n';
	str += '</TABLE>';
	str += '<TABLE border="0" width="300">';
	str += '<TR>';
	str += '<TH>' + languages.keywords.nature + ' :' + '</TH>';
	str += '<TD>' + get_datatype_name_in_current_language(nature) + '</TD>';
	str += '</TR>\n';
	str += '<TR><TH>' + languages.keywords.valfor + ' :</TH><TD>';

	if (value_format != '') {
		str += value_format;
	} else {
		str += languages.keywords.unavailable;
	}
	str += '</TD></TR>\n';
	str += '<TR><TH>' + languages.keywords.unit + ' :</TH><TD>';

	str += unit;

	str += '</TD></TR>\n';
	str += '</TABLE>';

	return str;
}

//------------------------------------------------------------------------
// Returns the reference of the frame containing the menu description
//------------------------------------------------------------------------

function getCodeFrame(){
	var codeFrame;
	codeFrame = window.opener.top.frames['code'];
	return(codeFrame);
}
//------------------------------------------------------------------------
// Display a class instance type
//------------------------------------------------------------------------

function class_instance_datatype_popup(path, name, targetClassCode){

	var str = '';
	var i;
	var targetFile;
	var reg=new RegExp(".html","gi");

	str += '<TABLE border="0" width="300">\n';
	str += '<TR><TD>';
	// class

	targetFile = "../model/family.html?family=" + path.replace(reg, "");

//	str += '<A href="' + targetFile + '" target="text" onClick="findSubMenuRef(\'' + targetClassCode + '\');">';
	str += '<A href="' + targetFile + '" target="text">';
	str += ISO_to_chars(name.toString());
	str += '<A>';

	str += '</TD></TR>\n';
	str += '</TABLE>';
	return str;
}

//------------------------------------------------------------------------
// Display a named type
//------------------------------------------------------------------------

function named_datatype_popup(data_type, data_type_adv){
	DataTypePopUp(data_type, data_type_adv)
}

//------------------------------------------------------------------------
// Display an aggregate type
//------------------------------------------------------------------------

function aggregate_datatype_popup(data_type_adv){
	DataTypePopUp(data_type_adv[0], data_type_adv[1]);
}

//------------------------------------------------------------------------
// Compute a string representation of an aggregate specification
//------------------------------------------------------------------------

function compute_aggregate_string(agg_spec){
	var str = '';

	for(i = 0; i <= agg_spec.length - 1; i++){
		// type
		str += agg_spec[i][0].toUpperCase();
		str += '[';
		// bound_1
		if (agg_spec[i][1] != ""){
			str += agg_spec[i][1];
		} else {
			str += 0;
		}
		str += ':'
		//bound_2
		if (agg_spec[i][2] != ""){
			str += agg_spec[i][2];
		} else {
			str += '?';
		}
		str += '] ' + languages.keywords.of.toUpperCase() + ' ';
		//optional
		if (agg_spec[i][4] == "TRUE"){
			str += languages.keywords.optional.toUpperCase() + " ";
		}
		//unique
		if (agg_spec[i][3] == "TRUE"){
			str += languages.keywords.unique.toUpperCase() + " ";
		}
	}

	return(str);
}

//------------------------------------------------------------------------
// Recursive search of the submenu element reference corresponding to a
// given class code
//------------------------------------------------------------------------

function findSubMenuRefFromCode(refRoot, code){
	var i;
	var refRes = null;

	i = 0;
	while((i < refRoot.items.length) && (refRes == null)){
		if (refRoot.items[i].code == code){
			refRes = refRoot.items[i];
		} else {
			if (refRoot.items[i].submenu) {
				refRes = findSubMenuRefFromCode(refRoot.items[i].submenu, code);
			}
		}
		i++;
	}
	return refRes;
}

//------------------------------------------------------------------------
// Search the current submenu reference
//------------------------------------------------------------------------

function findSubMenuRef(classCode){
	var menuRef = top.frames["code"].menu;
//	var classCode = getCookie("lastSelectedClass");
//	return(findSubMenuRefFromCode(menuRef, classCode))

	top.frames["code"].searchHighlight(menuRef, classCode, 'casesensitive', 'code')
}

//------------------------------------------------------------------------
// Display the Class Information Table
//------------------------------------------------------------------------
Class.prototype.display_class = DisplayClass;
function DisplayClass(frame) {

	var popuptitle = "Imagemax";
	var unknown_picture = "../runtime/menu-images/Unknown.gif";
	var time_stamps_col_span = 0;
	var full_col_span = 0;
	var icon_file = this.get_icon();
	var frame = (frame)?(frame):self;

	lastSelectedClass = getCookie("lastSelectedClass.cookie");
	var newSelectedClass = this.get_code_bsu();

	// Gestion du dernier s�lectionn� pour mise � jour quand changement de langue
	if (lastSelectedClass != newSelectedClass) {
		setCookie("lastSelectedClass.cookie", this.get_code_bsu());
//		setCookie("lastSelectedProperty.cookie", "");
	}

	frame.document.write('<table class="family" width=95% align="center" background="' + bkgdimg + '">');

	// FIRST ROW
	frame.document.write('<tr>');

    var colspan = 1;
    if (displayClassConfigurationData["code"] || displayClassConfigurationData["currentRevision"] ||
		displayClassConfigurationData["version"] || displayClassConfigurationData["currentVersion"] ||
        displayClassConfigurationData["revision"] || displayClassConfigurationData["shortName"] ||
		displayClassConfigurationData["synonymousNames"] || displayClassConfigurationData["originalDefinition"]){
          colspan = 2;
    }

	// Preferred name
	frame.document.write('<td class="prefname" width=66% align="center" valign="middle" colspan=' + colspan + '>');
	frame.document.write('<b><font size=+2>');
/*
	var file_doc = this.get_document();
	if ((file_doc != '') && ( file_doc.substr(0, 2) != '__')){
		frame.document.write("<a href=\"" + file_doc +  "\" target=\"blank\">");
	}
*/

	frame.document.write(this.get_preferred_name());

    frame.document.write('&nbsp;');

  	var icon_file = this.get_icon();
	if (icon_file != '') {
		frame.document.write('<img src="' + icon_file + '" width=40 height=40 alt="Icon">');
	}

	frame.document.write('</font></b>');
	frame.document.write('</td>');
 	frame.document.write('</tr>');

	var definition = this.get_definition();
	definition = definition.replace(/^\s+|\s+$/g, '');
    if ((displayClassConfigurationData["definition"]) && (definition != "") && (definition.toLowerCase() != NOT_TRANSLATED.toLowerCase())){
		frame.document.write('<tr>');
		frame.document.write('<td class="definition" width=100% align="left" valign="middle" colspan=' + colspan + '><b>' + languages.keywords.def + ' : </b> ');
		frame.document.write(definition);
		frame.document.write('</td>');
		frame.document.write('</tr>');
    }

	frame.document.write('<tr>');


    var simp_drawing = this.get_drawing();
	simp_drawing = simp_drawing.replace(/^\s+|\s+$/g, '');
	if ((displayClassConfigurationData["drawing"]) && (simp_drawing != "")) {
        //frame.document.write('<td class="drawing" width=25% align="center" valign="middle" rowspan=4>');
        frame.document.write('<th class="drawing" align="center" valign="middle">');
		frame.document.write("<a href=\"#\" onClick=\"DisplayImagePopUp('" + popuptitle + "', '" + simp_drawing + "')\">");
		frame.document.write('<img src="' + simp_drawing + '" width="150" height="150" border="0" alt="Simplified Drawing"></a>');
		frame.document.write('</th>');
    }

    if (colspan == 2) {

		if ((displayClassConfigurationData["drawing"]) && (simp_drawing != "")) {
			frame.document.write('<td class="code" width="70%" align="left" valign="top">');
		} else {
			frame.document.write('<td class="code" align="left" valign="top" colspan="2">');
		}

		if (displayClassConfigurationData["code"]){
			frame.document.write('<b>' + languages.keywords.code + ' : </b>');
			frame.document.write(this.get_code_bsu());
			frame.document.write('<br/>');
		}

		if (displayClassConfigurationData["version"]){
			frame.document.write('<b>' + languages.keywords.version + ' : </b>');
			frame.document.write(this.get_version());
			frame.document.write('<br/>');
		}

		if (displayClassConfigurationData["revision"]){
			frame.document.write('<b>' + languages.keywords.revision + ' : </b>');
			frame.document.write(this.get_revision());
			frame.document.write('<br/>');
		}

		var sh = this.get_short_name();
		sh = sh.replace(/^\s+|\s+$/g, '');
		if ((displayClassConfigurationData["shortName"]) && (sh != "") && (sh.toLowerCase() != NOT_TRANSLATED.toLowerCase())){
			frame.document.write('<b>' + languages.keywords.sh_name + ' : </b>');
			frame.document.write(sh);
			frame.document.write('<br/>');
		}

		var syn_names = this.get_synonymous_names();

		if ((displayClassConfigurationData["synonymousNames"]) && (syn_names.length > 0)){
			var val_syn = syn_names[0];
			val_syn = val_syn.replace(/^\s+|\s+$/g, '');
			if ((val_syn != "") && (val_syn.toLowerCase() != NOT_TRANSLATED.toLowerCase())){
				frame.document.write('<b>' + languages.keywords.syn_name + ' : </b>');
				frame.document.write(val_syn);
				frame.document.write('<br/>');
			}
		}

		var d_o_d = this.get_date_original_definition();
		d_o_d = d_o_d.replace(/^\s+|\s+$/g, '');
		var d_c_v = this.get_date_current_version();
		d_c_v = d_c_v.replace(/^\s+|\s+$/g, '');
		var d_c_r = this.get_date_current_revision();
		d_c_r = d_c_r.replace(/^\s+|\s+$/g, '');

		if ((displayClassConfigurationData["originalDefinition"]) && (d_o_d != "") && (d_o_d.toLowerCase() != NOT_TRANSLATED.toLowerCase())){
			frame.document.write('<b>' + languages.keywords.d_o_d + ' : </b>');
			frame.document.write(d_o_d);
			frame.document.write('<br/>');
		}

		if ((displayClassConfigurationData["currentVersion"]) && (d_c_v != "") && (d_c_v.toLowerCase() != NOT_TRANSLATED.toLowerCase())){
			frame.document.write('<b>' + languages.keywords.d_c_v + ' : </b>');
			frame.document.write(d_c_v);
			frame.document.write('<br/>');
		}

		if ((displayClassConfigurationData["currentRevision"]) && (d_c_r != "") && (d_c_r.toLowerCase() != NOT_TRANSLATED.toLowerCase())){
			frame.document.write('<b>' + languages.keywords.d_c_r + ' : </b>');
			frame.document.write(d_c_r);
			frame.document.write('<br/>');
		}
		frame.document.write('</td>');
	}

	frame.document.write('</tr>');

    var note = this.get_note();
	note = note.replace(/^\s+|\s+$/g, '');
    if ((displayClassConfigurationData["note"]) && (note != "") && (note.toLowerCase() != NOT_TRANSLATED.toLowerCase())){
		frame.document.write('<tr>');
		frame.document.write('<td class="note" width=100% align="left" valign="middle" colspan=' + colspan + '><b>' + languages.keywords.note + ' : </b>');
		frame.document.write(note);
		frame.document.write('</td>');
		frame.document.write('</tr>');
    }

    var remark = this.get_remark();
	remark = remark.replace(/^\s+|\s+$/g, '');
    if ((displayClassConfigurationData["remark"]) && (remark != "") && (remark.toLowerCase() != NOT_TRANSLATED.toLowerCase())){
		frame.document.write('<tr>');
		frame.document.write('<td class="remark" width=100% align="left" valign="middle" colspan=' + colspan + '><b>' + languages.keywords.remark + ' : </b>');
		frame.document.write(remark);
		frame.document.write('</td>');
		frame.document.write('</tr>');
    }

	frame.document.write('</table>');
}

//------------------------------------------------------------------------
// Decimal to Hexadecomal converter
//------------------------------------------------------------------------


function decToHex(dec){
	var hexStr = "0123456789ABCDEF";
	var low = dec % 16;
	var high = (dec - low)/16;
	hex = "" + hexStr.charAt(high) + hexStr.charAt(low);
	return hex;
}

//------------------------------------------------------------------------
// Replace predefined entities (&#xxx;) to the corresponding character
//------------------------------------------------------------------------


function unescape_first_character(aString){

    var dec = "";
	var res = ""
    var entityRegExp = new RegExp("^&#[0-9]+;", "g");

    if (entityRegExp.test(aString)) {
        var tmp = aString.match(entityRegExp);  // &#xxxx;blablabla => [&#xxxx];
        dec = tmp[0].substring(2, tmp[0].length - 1);  // [&#xxxx;] => xxxx
        res = unescape("%" + decToHex(dec)) + aString.replace(entityRegExp, "");
//        alert("tmp[0].length = " + tmp[0].length + "\n" + "dec = " + dec + "\n" + "hex = " + decToHex(dec) + "\n" + "res = " + res);
    } else {
        res = aString;
    }
    return(res);
}

//------------------------------------------------------------------------
// Sort function for properties display
//------------------------------------------------------------------------

function properties_order(a, b) {
	var str_a = unescape_first_character(a.get_preferred_name());
	var str_b = unescape_first_character(b.get_preferred_name());
	if (str_a < str_b) {return(-1)}
	if (str_a > str_b) {return(1)}
	if (str_a = str_b) {return(0)}
}

//------------------------------------------------------------------------
// Display the Properties Information Table
//------------------------------------------------------------------------
//------------------------------------------------------------------------
function ISO_to_chars(str) {
	 if (str.length>0){
		str = str.replace(/&quot;/g,'\"');	// 34 22
		str = str.replace(/&amp;/gi,'&');	// 38 26
		str = str.replace(/&#39;/g,'\'');	// 39 27
		str = str.replace(/&lt;/g,'<');		// 60 3C
		str = str.replace(/&gt;/g,'>');		// 62 3E
		str = str.replace(/&circ;/g,'^');	// 94 5E
		str = str.replace(/&lsquo;/g,'�');	// 145 91
		str = str.replace(/&rsquo;/g,'�');	// 146 92

		str = str.replace(/&ldquo;/g,'�');	// 147 93
		str = str.replace(/&rdquo;/g,'"');	// 148 94
		str = str.replace(/&bull;/g,'"');	// 149 95
		str = str.replace(/&ndash;/g,'�');	// 150 96
		str = str.replace(/&mdash;/g,'�');	// 151 97
		str = str.replace(/&tilde;/g,'�');	// 152 98
		str = str.replace(/&trade;/g,'�');	// 153 99
		str = str.replace(/&scaron;/g,'�');	// 154 9A
		str = str.replace(/&rsaquo;/g,'�');	// 155 9B
		str = str.replace(/&oelig;/g,'�');	// 156 9C
		str = str.replace(/&#357;/g,'�');	// 157 9D
		str = str.replace(/&#382;/g,'�');	// 158 9E
		str = str.replace(/&Yuml;/g,'�');	// 159 9F
		
		str = str.replace(/&nbsp;/g,' ');	// 160 A0
		str = str.replace(/&iexcl;/g,'�');	// 161 A1
		str = str.replace(/&cent;/g,'�');	// 162 A2
		str = str.replace(/&pound;/g,'�');	// 163 A3
		str = str.replace(/&curren;/g,'�');	// 164 A4
		str = str.replace(/&yen;/g,'�');	// 165 A5
		str = str.replace(/&brvbar;/g,'�');	// 166 A6
		str = str.replace(/&sect;/g,'�');	// 167 A7
		str = str.replace(/&uml;/g,'�');	// 168 A8
		str = str.replace(/&copy;/g,'�');	// 169 A9
		str = str.replace(/&ordf;/g,'�');	// 170 AA
		str = str.replace(/&laquo;/g,'�');	// 171 AB
		str = str.replace(/&not;/g,'�');	// 172 AC
		str = str.replace(/&shy;/g,'�');	// 173 AD
		str = str.replace(/&reg;/g,'�');	// 174 AE
		str = str.replace(/&macr;/g,'�');	// 175 AF
		str = str.replace(/&deg;/g,'�');	// 176 B0
		str = str.replace(/&plusmn;/g,'�');	// 177 B1
		str = str.replace(/&sup2;/g,'�');	// 178 B2
		str = str.replace(/&sup3;/g,'�');	// 179 B3
		str = str.replace(/&acute;/g,'�');	// 180 B4
		str = str.replace(/&micro;/g,'�');	// 181 B5
		str = str.replace(/&para/g,'�');	// 182 B6
		str = str.replace(/&middot;/g,'�');	// 183 B7
		str = str.replace(/&cedil;/g,'�');	// 184 B8
		str = str.replace(/&sup1;/g,'�');	// 185 B9
		str = str.replace(/&ordm;/g,'�');	// 186 BA
		str = str.replace(/&raquo;/g,'�');	// 187 BB
		str = str.replace(/&frac14;/g,'�');	// 188 BC
		str = str.replace(/&frac12;/g,'�');	// 189 BD
		str = str.replace(/&frac34;/g,'�');	// 190 BE
		str = str.replace(/&iquest;/g,'�');	// 191 BF
		str = str.replace(/&Agrave;/g,'�');	// 192 C0
		str = str.replace(/&Aacute;/g,'�');	// 193 C1
		str = str.replace(/&Acirc;/g,'�');	// 194 C2
		str = str.replace(/&Atilde;/g,'�');	// 195 C3
		str = str.replace(/&Auml;/g,'�');	// 196 C4
		str = str.replace(/&Aring;/g,'�');	// 197 C5
		str = str.replace(/&AElig;/g,'�');	// 198 C6
		str = str.replace(/&Ccedil;/g,'�');	// 199 C7
		str = str.replace(/&Egrave;/g,'�');	// 200 C8
		str = str.replace(/&Eacute;/g,'�');	// 201 C9
		str = str.replace(/&Ecirc;/g,'�');	// 202 CA
		str = str.replace(/&Euml;/g,'�');	// 203 CB
		str = str.replace(/&Igrave;/g,'�');	// 204 CC
		str = str.replace(/&Iacute;/g,'�');	// 205 CD
		str = str.replace(/&Icirc;/g,'�');	// 206 CE
		str = str.replace(/&Iuml;/g,'�');	// 207 CF
		str = str.replace(/&ETH;/g,'�');	// 208 D0
		str = str.replace(/&Ntilde;/g,'�');	// 209 D1
		str = str.replace(/&Ograve;/g,'�');	// 210 D2
		str = str.replace(/&Oacute;/g,'�');	// 211 D3
		str = str.replace(/&Ocirc;/g,'�');	// 212 D4
		str = str.replace(/&Otilde;/g,'�');	// 213 D5
		str = str.replace(/&Ouml;/g,'�');	// 214 D6
		str = str.replace(/&times;/g,'�');	// 215 D7
		str = str.replace(/&Oslash;/g,'�');	// 216 D8
		str = str.replace(/&Ugrave;/g,'�');	// 217 D9
		str = str.replace(/&Uacute;/g,'�');	// 218 DA
		str = str.replace(/&Ucirc;/g,'�');	// 219 DB
		str = str.replace(/&Uuml;/g,'�');	// 220 DC
		str = str.replace(/&Yacute;/g,'�');	// 221 DD
		str = str.replace(/&THORN;/g,'�');	// 222 DE
		str = str.replace(/&szlig;/g,'�');	// 223 DF
		str = str.replace(/&aacute;/g,'�');	// 224 E0
		str = str.replace(/&aacute;/g,'�');	// 225 E1
		str = str.replace(/&acirc;/g,'�');	// 226 E2
		str = str.replace(/&atilde;/g,'�');	// 227 E3
		str = str.replace(/&auml;/g,'�');	// 228 E4
		str = str.replace(/&aring;/g,'�');	// 229 E5
		str = str.replace(/&aelig;/g,'�');	// 230 E6
		str = str.replace(/&ccedil;/g,'�');	// 231 E7
		str = str.replace(/&egrave;/g,'�');	// 232 E8
		str = str.replace(/&eacute;/g,'�');	// 233 E9
		str = str.replace(/&ecirc;/g,'�');	// 234 EA
		str = str.replace(/&euml;/g,'�');	// 235 EB
		str = str.replace(/&igrave;/g,'�');	// 236 EC
		str = str.replace(/&iacute;/g,'�');	// 237 ED
		str = str.replace(/&icirc;/g,'�');	// 238 EE
		str = str.replace(/&iuml;/g,'�');	// 239 EF
		str = str.replace(/&eth;/g,'�');	// 240 F0
		str = str.replace(/&ntilde;/g,'�');	// 241 F1
		str = str.replace(/&ograve;/g,'�');	// 242 F2
		str = str.replace(/&oacute;/g,'�');	// 243 F3
		str = str.replace(/&ocirc;/g,'�');	// 244 F4
		str = str.replace(/&otilde;/g,'�');	// 245 F5
		str = str.replace(/&ouml;/g,'�');	// 246 F6
		str = str.replace(/&divide;/g,'�');	// 247 F7
		str = str.replace(/&oslash;/g,'�');	// 248 F8
		str = str.replace(/&ugrave;/g,'�');	// 249 F9
		str = str.replace(/&uacute;/g,'�');	// 250 FA
		str = str.replace(/&ucirc;/g,'�');	// 251 FB
		str = str.replace(/&uuml;/g,'�');	// 252 FC
		str = str.replace(/&yacute;/g,'�');	// 253 FD
		str = str.replace(/&thorn;/g,'�');	// 254 FE
		str = str.replace(/&yuml;/g,'�');
		str = str.replace(/&#224;/g,"�");
		str = str.replace(/&#226;/g,"�");
		str = str.replace(/&#231;/g,"�");
		str = str.replace(/&#232;/g,"�");
		str = str.replace(/&#233;/g,"�");
		str = str.replace(/&#234;/g,"�");
		str = str.replace(/&#235;/g,"�");
		str = str.replace(/&#238;/g,"�");
		str = str.replace(/&#244;/g,"�");
		str = str.replace(/&#249;/g,"�");
		str = str.replace(/&#251;/g,"�");
		}
	return str;
}

//------------------------------------------------------------------------


function UpdatePropertiesApplicable(form){

	var listObj = document.getElementById('SelM2'); 
	var inherited = document.getElementById('ProLev').checked; 
	var props = myclass.get_described_by();
	var status ="";
	var newOpt;
	var str;
//ES
	var showPropList = false;

	if (listObj){
		while (listObj.options.length) {
			listObj.options.remove(0);
		}
	}
	if (inherited){
//ES
		status = propertyFilterStatus["InheritedApplicable"];
		setCookie("lastPropertyFilterStatus.cookie", "InheritedApplicable");
	} else {
//ES
		status = propertyFilterStatus["Applicable"];
		setCookie("lastPropertyFilterStatus.cookie", "Applicable");
	}

//ES  	if (!listObj){
//ES		listObj=createElement('SELECT');
//ES  	}


	for (var i = 0; i <= props.length-1; i++) {

		if (status.indexOf(props[i].level)!=-1 ){
			showPropList = true;
			newOpt = document.createElement('OPTION');
			newOpt.value = props[i].get_code_bsu();
			str = props[i].get_preferred_name();
			if (str.length > 70 ) {
				str = str.substr(0, 70);
			}
			newOpt.text =ISO_to_chars(str.toString());
			listObj.options.add(newOpt, i);
		}
	}
//	if (listObj.options.length>0) listObj.options[0].selected = true;
//    alert("last = " + lastSelectedProperty + "\ntest = " + listObj.options[1].value);
    var found = false;
    for (var i = 0; i < listObj.options.length; i++) {
//            alert("value[" + i +  " = " + listObj.options[i].value);
         if (listObj.options[i].value == lastSelectedProperty) {
            found = true;
            listObj.options[i].selected = found;
         }
    }

    if (!found){
	    if (listObj.options.length > 0) {
    	    listObj.options[0].selected = true;
        }
    }

//ES
	setPropertiesListDisplay(showPropList);
	setAdvancedPropertiesDisplay(showPropList);
	DisplayProperty(self, form, true);
}

//------------------------------------------------------------------------
function UpdatePropertiesImported(form){

	var listObj = document.getElementById('SelM2');
	var inherited = document.getElementById('ProLev').checked;
	var props = myclass.get_described_by();
	var status = "";
	var newOpt;
	var str;
//ES
	var showPropList = false;

	if (listObj){
		while (listObj.options.length) {
			listObj.options.remove(0)
		}
	}
	if (inherited){
		status = propertyFilterStatus["InheritedImported"];
		setCookie("lastPropertyFilterStatus.cookie", "InheritedImported");

	} else {
		status = propertyFilterStatus["Imported"];
		setCookie("lastPropertyFilterStatus.cookie", "Imported");
	}
	if (!listObj){
		listObj=document.createElement("SELECT");
	}
	for (var i = 0; i <= props.length-1; i++) {
		if (status.indexOf(props[i].level)!=-1 ){
			showPropList = true;
			newOpt = document.createElement("OPTION");
			newOpt.value = props[i].get_code_bsu();
			str = props[i].get_preferred_name();
			if (str.length > 70 ) {
				str = str.substr(0, 70);
			}
			newOpt.text =ISO_to_chars(str.toString());
			listObj.options.add(newOpt, i);
		}
	}
//	if (listObj.options.length>0) listObj.options[0].selected = true;

	var found = false;
        for (var i = 0; i < listObj.options.length; i++) {
//            alert("value[" + i +  " = " + listObj.options[i].value);
            if (listObj.options[i].value == lastSelectedProperty) {
                found = true;
                listObj.options[i].selected = found;
            }
        }

    if (!found){
		if (listObj.options.length>0) {
    	    listObj.options[0].selected = true;
        }
    }


//ES
	setPropertiesListDisplay(showPropList);
	setAdvancedPropertiesDisplay(showPropList);
	DisplayProperty(self, form, true);
}


//------------------------------------------------------------------------
function UpdatePropertiesVisible(form){

	var listObj = document.getElementById('SelM2');
	var inherited = document.getElementById('ProLev').checked;
	var props = myclass.get_described_by();
	var status = "";
	var newOpt;
	var str;
//ES
	var showPropList = false;
	if (listObj){
		while (listObj.options.length) {
			listObj.options.remove(0)
		}
	}
	if (inherited){
		status = propertyFilterStatus["InheritedVisible"];
		setCookie("lastPropertyFilterStatus.cookie", "InheritedVisible");
	} else {
		status = propertyFilterStatus["Visible"];
		setCookie("lastPropertyFilterStatus.cookie", "Visible");
	}
	if (!listObj){
		listObj=document.createElement("SELECT");
	}

	for (var i = 0; i < props.length; i++) {

		if (status.indexOf(props[i].level) != -1 ){
			showPropList = true;
			newOpt = document.createElement('OPTION');
			newOpt.value = props[i].get_code_bsu();
			str = props[i].get_preferred_name();
			if (str.length > 70 ) {
				str = str.substr(0, 70);
			}
			newOpt.text =ISO_to_chars(str.toString());
			listObj.options.add(newOpt, i);
		}
	}
//    alert(listObj.options[0].value + "--" + lastSelectedProperty);
    var found = false;
    for (var i = 0; i < listObj.options.length; i++) {
        if (listObj.options[i].value == lastSelectedProperty) {
            found = true;
            listObj.options[i].selected = found;
        }
    }
    
	if (!found){
		if (listObj.options.length > 0) {
    	    listObj.options[0].selected = true;
        }
    }

//ES
	setPropertiesListDisplay(showPropList);
	setAdvancedPropertiesDisplay(showPropList);
	DisplayProperty(self, form, true);
}

//------------------------------------------------------------------------
function UpdateProperties(form){

	var prop_typ = form.prop_typ;
	
	for (var i=0; i<prop_typ.length;i++) {
		if (prop_typ[i].checked) {
			if (prop_typ[i].value =="applicable"){
				UpdatePropertiesApplicable(form);
			}
			if (prop_typ[i].value =="imported"){
				UpdatePropertiesImported(form);
			}
			if (prop_typ[i].value =="visible"){
				UpdatePropertiesVisible(form);
			}
		}
	}
}


//------------------------------------------------------------------------

Class.prototype.display_properties = DisplayProperties;

function DisplayProperties(frame) {
	var max_length = 0;
	var col = 0;
	var str = "";
	// get all properties:
	var props = this.get_described_by();
	var frame = (frame)?(frame):self;

	lastPropertyFilterStatus = getCookie("lastPropertyFilterStatus.cookie");
	if (lastPropertyFilterStatus == null) {
		lastPropertyFilterStatus = "InheritedApplicable"
		setCookie("lastPropertyFilterStatus.cookie", lastPropertyFilterStatus);
	}

	var applicableFilter = "";
	var visibleFilter = "";
	var importedFilter ="";
	var inheritedFilter = "";

	if (lastPropertyFilterStatus == "Applicable") {
		applicableFilter = "checked";
	}
	if (lastPropertyFilterStatus == "InheritedApplicable") {
		applicableFilter = "checked";
		inheritedFilter = "checked";
	}
	if (lastPropertyFilterStatus == "Visible") {
		visibleFilter = "checked";
	}
	if (lastPropertyFilterStatus == "InheritedVisible") {
		visibleFilter = "checked";
		inheritedFilter = "checked";
	}
	if (lastPropertyFilterStatus == "Imported") {
		importedFilter = "checked";
	}
	if (lastPropertyFilterStatus == "InheritedImported") {
		importedFilter = "checked";
		inheritedFilter = "checked";
	}

	if (props.length > 0){
		props.sort(properties_order);

		frame.document.write('<BR/>');
		frame.document.write('<FORM name="propform">');

		frame.document.write('<TABLE class="property" WIDTH=95% ALIGN=CENTER>');
		frame.document.write('<TR>');
		frame.document.write('<TD COLSPAN=4 class="proptitle"><FONT size=+2><center>');
		frame.document.write(languages.keywords.partchar + '</center></FONT>');
		frame.document.write('<TABLE WIDTH="100%">');

		if (displayGeneralConfigurationData['full_display']) {

			frame.document.write('<TR>');
			frame.document.write('<TD COLSPAN=4>');
			frame.document.write('<INPUT type="radio" name="prop_typ" value="applicable" '
				+ applicableFilter + ' onClick="UpdatePropertiesApplicable(this.form);">'
				+ languages.keywords.applicable_selector + '&nbsp;&nbsp;');
			frame.document.write('<INPUT type="radio" name="prop_typ" value="imported" '
				+ importedFilter + ' onClick="UpdatePropertiesImported(this.form);">'
				+ languages.keywords.imported_selector + '&nbsp;&nbsp;');
			frame.document.write('<INPUT type="radio" name="prop_typ" value="visible" '
				+ visibleFilter + ' onClick="UpdatePropertiesVisible(this.form)">'
				+ languages.keywords.visible_selector +'&nbsp;&nbsp;');
			frame.document.write('</TD>');
			frame.document.write('<TD COLSPAN=1>');
			frame.document.write('<INPUT type="checkbox" id="ProLev" name="prop_level" value="inherited" '
				+ inheritedFilter + ' onClick="UpdateProperties(this.form)"> '
				+ languages.keywords.inherited_selector + '<br>');
			frame.document.write('</TD>');
			frame.document.write('</TR>');

		}
		
		frame.document.write('</TABLE>');

		frame.document.write('</TD>');
		frame.document.write('</TR>');
		frame.document.write('<TR>');

		frame.document.write('<TD width=' + col + '1>');

		frame.document.write('<CENTER>');
		frame.document.write('<TABLE>');
//ES
		frame.document.write('<TR><TD>');

		frame.document.write('<SELECT id="SelM2" name="SelectMenu2" size="7" onChange="DisplayProperty(self,this.form,true)">');

		var is_first = true;
		var propBSU = "";
		var status = propertyFilterStatus[lastPropertyFilterStatus];
        var selection = false;

		lastSelectedProperty = getCookie("lastSelectedProperty.cookie");

        for (var i = 0; i <= (props.length - 1); i++) {
			if (status.indexOf(props[i].level) != -1){
	            if (props[i].get_code_bsu() == lastSelectedProperty) {
    	            selection = true;
                }
            }
        }
		for (var i = 0; i <= (props.length - 1); i++) {
//ES
			if (status.indexOf(props[i].level) != -1) {
				propBSU = props[i].get_code_bsu() ;
				if ((lastSelectedProperty != null) && (selection == true)){
					if (propBSU == lastSelectedProperty) {
						frame.document.write('<option selected value="');
					} else {
						frame.document.write('<option value="');
					}
				} else {
					if (is_first) {
//                        alert("is first");
						frame.document.write('<option selected value="');
					} else {
						frame.document.write('<option value="');
					}
				}
				frame.document.write(propBSU);
				frame.document.write('">');
				str = props[i].get_preferred_name();
				frame.document.write(str);
				frame.document.write('</option>');
				is_first = false;
			}
		}

		frame.document.write('</SELECT>');
//ES
		frame.document.write('</TD>');

		if (isIE4up == false) {
			frame.document.write('<CENTER><INPUT Type="Button" Value="' + languages.keywords.advanced + '" onClick="SwitchAdvanced(self,this.form)"></CENTER>');
		}


		frame.document.write('</TABLE>');
		frame.document.write('</CENTER>');

		frame.document.write('</TD>');
		frame.document.writeln("<TD>");

		if (isIE4up) {
			frame.document.writeln('<TABLE id="propertycharacteristics" WIDTH=100% HEIGHT=95% BORDER=1></TABLE>');
			DisplayProperty(frame, document["propform"],false);
		} else {
			frame.document.writeln("<FORM name=propertycharacteristics>");
			frame.document.writeln("<TABLE WIDTH=95% HEIGHT=100% BORDER=1>");
			frame.document.writeln("<TR HEIGHT='33%'>");
			frame.document.writeln("<TD WIDTH='40%'><B>"+languages.keywords.code+" : </B><INPUT type='text' name=symbol size=17></TD>");
			frame.document.writeln("<TD WIDTH=30%><B>"+languages.keywords.unit+" : </B><INPUT type='text' name=unit size=12></TD>");
			frame.document.writeln("<TD WIDTH='30%'><B>"+languages.keywords.version+" : </B><INPUT type='text' name=version size=8></TD></TR>");
			frame.document.writeln("<TR HEIGHT='33%'><TD COLSPAN=3><B><A HREF='#' onClick='DataTypePopUp()'>"+languages.keywords.data+"</A> : </B><INPUT type='text' name=data size=40></TD></TR>");
			frame.document.writeln("<TR HEIGHT='33%'><TD COLSPAN=3>");
			frame.document.writeln("<B>"+languages.keywords.def+" : </B><TEXTAREA name=def cols=52 rows=2 READONLY WRAP=PHYSICAL></TEXTAREA></TD></TR></TABLE></TD></TR>");
			frame.document.writeln("<TR><TD COLSPAN=2><TABLE WIDTH=100% HEIGHT=100%><TR><TD WIDTH=50%>");
			frame.document.writeln("<B>"+languages.keywords.note+" : </B><TEXTAREA name=note cols=35 rows=2 READONLY WRAP=PHYSICAL></TEXTAREA></TD>");
			frame.document.writeln("<TD WIDTH=50%><B>"+languages.keywords.remark+" : </B><TEXTAREA name=remark cols=35 rows=2 READONLY WRAP=PHYSICAL></TEXTAREA></TD></TR>");
			frame.document.writeln("</FORM></TABLE>");
			DisplayProperty(frame,	document["propform"],false);
		}

		frame.document.writeln('</TD></TR></TABLE><BR>');

		if (isIE4up) {
			frame.document.writeln("<TABLE id='advancedproperties' class='advproperty' WIDTH=95%  ALIGN=CENTER>");
			frame.document.writeln("<TR><TD class='proptitle'><center>");
			frame.document.writeln("<A HREF='#' class='advproplink' onClick='SwitchAdvanced(self,document.propform)'>"
				+ languages.keywords.advchar + "</A>");
			frame.document.writeln("</center></TD></TR></TABLE> ");
			DisplayPropertiesPopUp(frame, frame.document.propform);
		}

		frame.document.writeln('<BR>');

		if (this.selectors.length>0){
			frame.document.write('<TABLE class="Selectors" WIDTH=95%  ALIGN=CENTER><TR><TD class="proptitle"><center>');
			frame.document.write("<A HREF='#' class='selproplink' onClick='SwitchSelectors(self,\"Selector\")'>"
				+ languages.keywords.clsel + "</A></center></TD></TR></TABLE>");
//			DisplaySelectorsPopUp(frame);
		}
		frame.document.writeln('<BR></FORM>');
//ES
		setPropertiesListDisplay(!is_first);
		setAdvancedPropertiesDisplay(!is_first);
	}
}

//------------------------------------------------------------------------
// Display or hide the properties list
//------------------------------------------------------------------------
//ES
function setPropertiesListDisplay(show) {
	var status;
	if (show) {
		status = "inline"
	} else {
		status = "none";
	}
	document.getElementById("SelM2").style.display = status;

}

//------------------------------------------------------------------------
// Display or hide the advanced properties table
//------------------------------------------------------------------------
//ES
function setAdvancedPropertiesDisplay(show) {
	var status;
	if (show) {
		status = "inline"
	} else {
		status = "none";
	}
	document.getElementById("advancedproperties").style.display = status;

}
//------------------------------------------------------------------------
// Go to the URL of the selected family
//------------------------------------------------------------------------

function AllerA(select, target){
	var id = select.selectedIndex;
	var URL = select.options[id].value;
	var target_frame;

	for(i = 0; i < top.frames.length; i++) {
		if(top.frames[i].name == target) {
			target_frame = top.frames[i];
		}
	}
	target_frame.location.href = URL;
}

//--------------------------------------------------------------------------//
//
//--------------------------------------------------------------------------//
function dictionaryFamily(classe,langue,frame){
	this.langue = (langue) ? langue : parseInt(getCookie("langage.cookie"));
	this.classe = (classe) ? classe : null;  // class is already a key word
	this.frame = (frame) ? (frame): self ;
	eval("dictionary=this;");
}
//--------------------------------------------------------------------------//
dictionaryFamily.prototype.get_class = getClass;
function getClass(){
	return this.classe ;
}
//--------------------------------------------------------------------------//
dictionaryFamily.prototype.get_frame = getFrame;
function getFrame(){
	return this.frame ;
}
//--------------------------------------------------------------------------//
dictionaryFamily.prototype.get_langue = getLangue;
function getLangue(){
	return this.langue ;
}

//--------------------------------------------------------------------------//
dictionaryFamily.prototype.display = Display;
function Display(){
	var myclass = this.get_class();
	var myframe = this.get_frame();
	myclass.display_class(myframe);
	if (WithProperties){
		myclass.display_properties(myframe);
	}
	activate_menu();
}
//--------------------------------------------------------------------------//

//------------------------------------------------------------------------
// Display a property information from a given property code
//------------------------------------------------------------------------

dictionaryFamily.prototype.set_displayed_property = setDisplayedProperty;
function setDisplayedProperty(code){
	var myclass = this.get_class();
	var frame = this.get_frame();
	var props = myclass.get_described_by();
	if (code == ""){
		return;
	}

	if (props.length>0) {
		var idx = 0;
		for (i = 1; i < props.length; i++) {
			if (props[i].code == code) {
				idx = i;
				break;
			}
		}
 		frame.propform.elements[0].options[idx].selected = true;
		DisplayProperty(frame,frame.propform, true);
	}
}


//------------------------------------------------------------------------
// Activate menus
//------------------------------------------------------------------------

function activate_menu(){
	if(top.frames['code'].MTMDisplayMenu != null) {
		top.frames['code'].MTMTrack = true;
//		top.frames['code'].resetHighlight(top.frames['code'].menu);
		setTimeout("top.frames['code'].MTMDisplayMenu()", 250);
	}
}
