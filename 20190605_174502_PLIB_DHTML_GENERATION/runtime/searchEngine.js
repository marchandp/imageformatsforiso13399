var SEARCHANY 	= 1;
var SEARCHALL 	= 2;
var searchType 	= '';
var currentMatch= 0;
var copyArray	= new Array();
//var docObj		= parent.frames[1].document;

var language = languages.get_language();
var searchProperty = true;
var searchClass = true;

function validate(e) {
	var entry = e.query.value;
    searchClass = e.classChk.checked;
    searchProperty = e.propertyChk.checked;

	if (entry.charAt(0) == "+") {
		entry = entry.substring(1,entry.length);
		searchType = SEARCHANY;
	} else {
			searchType = SEARCHALL;
	}
	while (entry.charAt(0) == ' ') {
		entry = entry.substring(1,entry.length);
		document.forms[0].query.value = entry;
	}
	while (entry.charAt(entry.length - 1) == ' ') {
		entry = entry.substring(0,entry.length - 1);
		document.forms[0].query.value = entry;
	}
	if (entry.length < 2) {
		alert("You can only search for a string of at least 2 characters .");
		document.forms[0].query.focus();
		return;
	}
	convertString(textToHtml(entry));
}

function convertString(reentry) {
	var searchArray = reentry.split(" ");
	if (searchType == SEARCHALL) {
		requireAll(searchArray); }
	else {
		allowAny(searchArray);
	}
}

function splitArray(joinedString, splitString) {
	var tempArray = new Array(0);
	var arrayCounter		= 0;
	var elementBoolean	= true;
	for (var i = 0; i < joinedString.length; i++) {
		if(elementBoolean) {
			tempArray[arrayCounter] = "";
			elementBoolean 		  = false;
		}
		if(joinedString.charAt(i) == splitString) {
			arrayCounter++;
			elementBoolean = true;
			continue;
		}
		tempArray[arrayCounter] += joinedString.charAt(i);
	}
	return tempArray;
}

function allowAny(t) {
	var findings = new Array(0);
	for (i = 0; i < searchenginedata.length; i++) {
		var compareElement  = searchenginedata[i].toUpperCase();
		var compareElementArray = compareElement.split('|');
		var refineElement  = compareElementArray[2];
        var lg = compareElementArray[6];
        var isProp = (compareElementArray[5] == 'PROPERTY');
        var isClass = (compareElementArray[5] == 'CLASS');

        if (((isProp && searchProperty) || (isClass && searchClass)) && (lg == language.toUpperCase())){
			for (j = 0; j < t.length; j++) {
				var compareString = t[j].toUpperCase();
				if (refineElement.indexOf(compareString) != -1) {
					findings[findings.length] = searchenginedata[i];
					break;
				}
			}
        }
	}
	verifyManage(findings);
}

function requireAll(t) {
	var findings = new Array(0);
	for (i = 0; i < searchenginedata.length; i++) {
		var allConfirmation = true;
		var allString = searchenginedata[i].toUpperCase();
		var compareElementArray = allString.split('|');
        var refineAllString = compareElementArray[2];
        var lg = compareElementArray[6];
        var isProp = (compareElementArray[5] == 'PROPERTY');
        var isClass = (compareElementArray[5] == 'CLASS');
        if (((isProp && searchProperty) || (isClass && searchClass)) && (lg == language.toUpperCase())){
			for (j = 0; j < t.length; j++) {
				var allElement = t[j].toUpperCase();
                var res = refineAllString.indexOf(allElement);
				if (res == -1) {
					allConfirmation = false;
					continue;
                }
			}
			if (allConfirmation) {
				findings[findings.length] = searchenginedata[i];
			}
        }
	}
	verifyManage(findings);
}

function verifyManage(resultSet) {
	if (resultSet.length == 0) {
		noMatch();
	}
	else {
		copyArray = resultSet;
		formatResults(copyArray, currentMatch, displayGeneralConfigurationData['showMatches']);
	}
}

function noMatch() {

	alert(window.frames.length);
	//var docObj = window.frames["main"].document;

	//docObj.open();
	//docObj.writeln('<HTML><HEAD><TITLE>Search Results</TITLE>'
	//	+ '</HEAD>'
	//	+ '<BODY background="./fond.gif" TEXT=BLACK>'
	//	+ '<TABLE WIDTH=90% BORDER=0 ALIGN=CENTER><TR><TD VALIGN=TOP><FONT FACE=Arial><B><DL>'
	//	+ '<HR NOSHADE WIDTH=100%>"'
	//	+ document.forms[0].query.value
	//	+ '" '
	//	+ languages.keywords.no_results
	//	+ '.<HR NOSHADE WIDTH=100%></TD></TR></TABLE></BODY></HTML>');
	//docObj.close();
	//document.forms[0].query.select();
}

function sortResults(results) {
    var max = results.length;
    var tmp = '';
	for (var i = 0; i < max; i++){
	    for (var j = i + 1; j < max; j++){
            if (results[i].split("|")[0] >= results[j].split("|")[0]){
                tmp = results[i];
                results[i] = results[j];
                results[j] = tmp;
            }
	    }
    }
    return(results);
}


// Results grouped by code (results[0])
function factorizeResults(results){
    var factres = sortResults(results);
    var res = new Array(0);
    var k = 0;
    var max = factres.length;
    var tmp ='';

	for (var i = 0; i < max; i++) {
        var key = factres[i].split("|")[0];
        if (tmp != key){
            res[k] = factres[i];
			tmp = key;
            k++;
        } else {
            res[k-1] = res[k-1] + "||" + factres[i];
        }
    }
    return(res);
}


function mayBeDisplayed(concept, attribute){
    var res = true;
    if (concept == 'CLASS'){
    	    if (attribute == 'SHORT_NAME') {
	           res = displayClassConfigurationData['shortName'];
       		} else if (attribute == 'DEFINITION'){
           		res = displayClassConfigurationData['definition'];
        	} else if (attribute == 'NOTE'){
           		res = displayClassConfigurationData['note'];
        	} else if (attribute == 'REMARK'){
           		res = displayClassConfigurationData['remark'];
        	} else if (attribute == 'CODE'){
				res = displayClassConfigurationData['code'];
        	} else if (attribute == 'SYNONYMOUS'){
				res = displayClassConfigurationData['synonymousNames'];
			}
    } else if (concept == 'PROPERTY'){
        	if (attribute == 'SHORT_NAME') {
           		res = displayPropConfigurationData['shortName'];
	        } else if (attribute == 'DEFINITION'){
    	       	res = displayPropConfigurationData['definition'];
	        } else if (attribute == 'NOTE'){
           		res = displayPropConfigurationData['note'];
    	    } else if (attribute == 'REMARK'){
           		res = displayPropConfigurationData['remark'];
        	} else if (attribute == 'DOMAIN'){
           		res = displayPropConfigurationData['domain'];
			} else if (attribute == 'CODE'){
				res = displayPropConfigurationData['code'];
			} else if (attribute == 'SYNONYMOUS'){
				res = displayPropConfigurationData['synonymousNames'];
			} else if (attribute == 'SYMBOL'){
				res = displayPropConfigurationData['symbol'];
			}
    }
	
    return(res);
}

function removeNonDisplayedResults(results){
    var max = results.length;
    var res = new Array(0);
    var k = 0;
    for (i = 0; i < max; i++) {
       //alert("res[" + i + "] = \n" + results[i]);
		var divide = splitArray(results[i], '|');
		if (mayBeDisplayed(divide[5], divide[4])) {
        	res[k] = results[i];
            k++;
        }
    }
    return(res);
}

//A class entry: "code|pref_name|string|location|attribute|CLASS|lg"

function formatResults(results, reference, offset) {
    var factorizedresults = removeNonDisplayedResults(results);
    var docObj = parent.frames[1].document

    if (factorizedresults.length == 0) {
       noMatch();
       return;
    }

	factorizedresults = factorizeResults(factorizedresults);
	var currentRecord = (factorizedresults.length < reference + offset ? factorizedresults.length : reference + offset);

	docObj.open();
	docObj.writeln('<HTML>\n<HEAD>\n<TITLE>Search Results</TITLE>\n'
        + '<SCRIPT>'
     	+ 'function setCookie(name, value) {'
       	+ '	var curCookie = name + "=" + escape(value) + ";";'
        + '	curCookie += "; path=/";'
        + '	document.cookie = curCookie;'
	    + '}'
	    + 'function updateCookies(propCode, propStatus){'
	    + '	if (propCode != ""){'
	    + '		setCookie("lastSelectedProperty.cookie", propCode);'
	    + '	}'
	    + '	if (propStatus != ""){'
	    + '	    if (propStatus == "A"){'
	    + '    		setCookie("lastPropertyFilterStatus.cookie", "Applicable");'
	    + '   	} else {'
	    + '    		setCookie("lastPropertyFilterStatus.cookie", "Visible");'
	    + '    	}'
	    + '	}'
        + ' '
	    + '}'
        + '</SCRIPT>'
		+ '</HEAD>'
		+ '<BODY background="./fond1.gif" TEXT=BLACK>'
		+ '<TABLE WIDTH=90% BORDER=0 ALIGN=CENTER CELLPADDING=3><TR><TD>'
		+ '<HR NOSHADE WIDTH=100%></TD></TR><TR><TD VALIGN=TOP><FONT FACE=Arial><B>'
		+ '</I><BR>\n'
        + languages.keywords.results
		+ ': <I>'
		+ (reference + 1) 
		+ ' - ' 
		+ currentRecord 
		+ ' '
		+ languages.keywords.of2
		+ ' '
		+ factorizedresults.length 
		+ '</I><BR><BR></FONT>' 
		+ '<FONT FACE=Arial SIZE=-1><B>' 
		+ '\n\n<!-- Begin result set //-->\n\n\t<DL>'
		);
	for (var i = reference; i < currentRecord; i++) {
		var divide1 = factorizedresults[i].split('||');
        for (var j = 0; j < divide1.length; j++) {
			var divide2 = splitArray(divide1[j], '|');
	        if (j == 0) {
            	docObj.writeln('<P>');
				docObj.write('\n\n\t<DT><IMG SRC="');
                if (divide2[5] == 'CLASS') {
                    docObj.write('class.gif');
                } else if (divide2[5] == 'PROPERTY') {
                    docObj.write('property.gif');
                }
                docObj.write('"/>&nbsp;');

                if (divide2[5] == 'CLASS') {
					docObj.writeln('<A HREF="' + divide2[3] + '" TARGET="text">' + divide2[1] + '</A>');
                } else if (divide2[5] == 'PROPERTY') {
                    var tmp = divide2[3].split('&');
					docObj.writeln('<A HREF="'
						+ tmp[0] 
						+ '" TARGET="text" onClick="updateCookies(\''
						+ tmp[1].substring(5, tmp[1].length)
						+ '\', \''
						+ tmp[2].substring(9, tmp[2].length)
						+ '\')">'
						+ divide2[1]
						+ '</A>');
                }
            }
            if (divide2[4] != 'PREFERRED_NAME'){
            	docObj.writeln('\t<DD>' + divide2[4] + ': <I>' + divide2[2] + '</I><P>');
            }
        }
	}

	docObj.writeln('\n\t</DL>\n\n<!-- End result set //-->\n\n');
	prevNextResults(factorizedresults.length, reference, offset);
	docObj.writeln('<HR NOSHADE WIDTH=100%>' +
		'</TD>\n</TR>\n</TABLE>\n</BODY>\n</HTML>');
	docObj.close();
	document.forms[0].query.select();
}

function prevNextResults(ceiling, reference, offset) {
	var docObj = parent.frames[1].document

	docObj.writeln('<CENTER><FORM>');
	if(reference > 0) {
		var prev = languages.keywords["prev"].replace("NN", offset).replace("SS", "s");
		docObj.writeln('<INPUT TYPE=BUTTON VALUE="'
			+ prev
			+ '" '
			+ 'onClick="parent.frames[0].formatResults(parent.frames[0].copyArray, '
			+ (reference - offset)
			+ ', '
			+ offset
			+ ')">');
	}
	if(reference >= 0 && reference + offset < ceiling) {
		var trueTop = ((ceiling - (offset + reference) < offset) ? ceiling - (reference + offset) : offset);
		var howMany = (trueTop > 1 ? "s" : "");
		var next = languages.keywords["next"].replace("NN", trueTop).replace("SS", howMany);
		docObj.writeln('<INPUT TYPE=BUTTON VALUE="'
			+ next
			+ '" ' 
			+ 'onClick="parent.frames[0].formatResults(parent.frames[0].copyArray, ' 
			+ (reference + offset) 
			+ ', '
			+ offset
			+ ')">');
	}
	docObj.writeln('</CENTER>');
}

function textToHtml(ch){
	var s = '';
	var result = '';
    var pattern = '/[���������������]/';

	for (var i = 0; i < ch.length; i++) {
//        alert('i = ' + i + '\nfound = ' + pattern.search(ch.charAt(i)) + '\ncode = ' + ch.charCodeAt(i));
        if (pattern.search(ch.charAt(i)) != -1){
            s = '&#' + (ch.charCodeAt(i)).toString() + ';';
        } else {
            s = ch.charAt(i);
        }
        result = result + s;
	}
	return(result);
}

