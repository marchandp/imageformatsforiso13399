/********************************************************************
* Attributes configuration data.                                    *
********************************************************************/

// property and class attributes display configuration

var displayPropConfigurationData = new displayPropertyConfiguration();
displayPropConfigurationData["formula"] = true;
displayPropConfigurationData["code"] = true;
displayPropConfigurationData["originalDefinition"] = true;
displayPropConfigurationData["propertyTypeClassification"] = true;
displayPropConfigurationData["note"] = true;
displayPropConfigurationData["version"] = true;
displayPropConfigurationData["definition"] = true;
displayPropConfigurationData["currentVersion"] = true;
displayPropConfigurationData["shortName"] = true;
displayPropConfigurationData["revision"] = true;
displayPropConfigurationData["synonymousSymbols"] = true;
displayPropConfigurationData["symbol"] = true;
displayPropConfigurationData["propScope"] = true;
displayPropConfigurationData["remark"] = true;
displayPropConfigurationData["synonymousNames"] = true;
displayPropConfigurationData["currentRevision"] = true;
displayPropConfigurationData["figure"] = true;
displayPropConfigurationData["domain"] = true;


var displayClassConfigurationData = new displayClassConfiguration();
displayClassConfigurationData["code"] = true;
displayClassConfigurationData["definition"] = true;
displayClassConfigurationData["currentRevision"] = true;
displayClassConfigurationData["remark"] = true;
displayClassConfigurationData["version"] = true;
displayClassConfigurationData["note"] = true;
displayClassConfigurationData["currentVersion"] = true;
displayClassConfigurationData["drawing"] = true;
displayClassConfigurationData["revision"] = true;
displayClassConfigurationData["shortName"] = true;
displayClassConfigurationData["synonymousNames"] = true;
displayClassConfigurationData["originalDefinition"] = true;


var displayGeneralConfigurationData = new displayGeneralConfiguration();
displayGeneralConfigurationData["download"] = true;
displayGeneralConfigurationData["full_display"] = true;
displayGeneralConfigurationData["showMatches"] = 5;
