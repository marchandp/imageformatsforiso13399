var langue="en";

// Class Selectors

var selection = new Array();

var selectors = new Array();

// supplier definition : x

var supplier = new Supplier("x");

// class definition : x.72C696D513B40-001

var code ="72C696D513B40"; 
var version ="001"; 
var revision="001"; 
var items = new Item_names("testimageformats", "x", "", new Array());
var dates = new Dates("", "", "" );
var definition = "x"; 
var note = ""; 
var remark = ""; 
var superclass = null; 
var subclasses = new Array(); 
var source_doc = "" ; 
var drawing =  "../external-items/DIR0000/FILE_0000_0003.jpg" ; 
var coded_name =  "" ; 
var url = "x.72C696D513B40-001.csv";
myclass = new Class(supplier, code, version, items, dates, revision, definition, note, remark, superclass, subclasses, selectors, source_doc, drawing, coded_name, url ); 

// property definition : x.72C696D513B40-001.72C696DFF8B21-001

var code = "72C696DFF8B21";
var version = "001";
var revision = "001" ;
var items = new Item_names("tif", "", "", new Array());
var domain = new Domain( "String", new Array("X 17")); 
var pref_symbol = "" ; 
var syn_symbols = new Array() ; 
var det_class = "" ; 
var definition = ""; 
var note = ""; 
var remark = ""; 
var condition = new Array() ; 
var dates = new Dates("", "", "");
var source_doc ="" ;
var figure ="../external-items/DIR0000/FILE_0000_0006.tif" ;
var formula = "" ;
var level = "3";
var prop_scope = "testimageformats";
var class_id = "x_72C696D513B40_001";
var prop_scopeCode = "72C696D513B40";
var isExternal = false;
new Property(myclass, code, version, items, domain, revision, pref_symbol, syn_symbols, det_class, definition, note, remark, condition, dates, source_doc, figure, formula, level, prop_scope, class_id, prop_scopeCode, isExternal );

// property definition : x.72C696D513B40-001.72C696E02938A-001

var code = "72C696E02938A";
var version = "001";
var revision = "001" ;
var items = new Item_names("svg", "", "", new Array());
var domain = new Domain( "String", new Array("X 17")); 
var pref_symbol = "" ; 
var syn_symbols = new Array() ; 
var det_class = "" ; 
var definition = ""; 
var note = ""; 
var remark = ""; 
var condition = new Array() ; 
var dates = new Dates("", "", "");
var source_doc ="" ;
var figure ="../external-items/DIR0000/FILE_0000_0009" ;
var formula = "" ;
var level = "3";
var prop_scope = "testimageformats";
var class_id = "x_72C696D513B40_001";
var prop_scopeCode = "72C696D513B40";
var isExternal = false;
new Property(myclass, code, version, items, domain, revision, pref_symbol, syn_symbols, det_class, definition, note, remark, condition, dates, source_doc, figure, formula, level, prop_scope, class_id, prop_scopeCode, isExternal );

// property definition : x.72C696D513B40-001.72C696E015CA4-001

var code = "72C696E015CA4";
var version = "001";
var revision = "001" ;
var items = new Item_names("png", "", "", new Array());
var domain = new Domain( "String", new Array("X 17")); 
var pref_symbol = "" ; 
var syn_symbols = new Array() ; 
var det_class = "" ; 
var definition = ""; 
var note = ""; 
var remark = ""; 
var condition = new Array() ; 
var dates = new Dates("", "", "");
var source_doc ="" ;
var figure ="../external-items/DIR0000/FILE_0000_0012.png" ;
var formula = "" ;
var level = "3";
var prop_scope = "testimageformats";
var class_id = "x_72C696D513B40_001";
var prop_scopeCode = "72C696D513B40";
var isExternal = false;
new Property(myclass, code, version, items, domain, revision, pref_symbol, syn_symbols, det_class, definition, note, remark, condition, dates, source_doc, figure, formula, level, prop_scope, class_id, prop_scopeCode, isExternal );

// property definition : x.72C696D513B40-001.72C696DFE5F42-001

var code = "72C696DFE5F42";
var version = "001";
var revision = "001" ;
var items = new Item_names("jpeg", "", "", new Array());
var domain = new Domain( "String", new Array("X 17")); 
var pref_symbol = "" ; 
var syn_symbols = new Array() ; 
var det_class = "" ; 
var definition = ""; 
var note = ""; 
var remark = ""; 
var condition = new Array() ; 
var dates = new Dates("", "", "");
var source_doc ="" ;
var figure ="../external-items/DIR0000/FILE_0000_0011.jpg" ;
var formula = "" ;
var level = "3";
var prop_scope = "testimageformats";
var class_id = "x_72C696D513B40_001";
var prop_scopeCode = "72C696D513B40";
var isExternal = false;
new Property(myclass, code, version, items, domain, revision, pref_symbol, syn_symbols, det_class, definition, note, remark, condition, dates, source_doc, figure, formula, level, prop_scope, class_id, prop_scopeCode, isExternal );

// property definition : x.72C696D513B40-001.72C696D53472E-001

var code = "72C696D53472E";
var version = "001";
var revision = "001" ;
var items = new Item_names("gif", "d", "", new Array());
var domain = new Domain( "Real Measure", new Array("", "NR2 S..3.3")); 
var pref_symbol = "" ; 
var syn_symbols = new Array() ; 
var det_class = "" ; 
var definition = "Not defined..."; 
var note = ""; 
var remark = ""; 
var condition = new Array() ; 
var dates = new Dates("", "", "");
var source_doc ="" ;
var figure ="../external-items/DIR0000/FILE_0000_0004.gif" ;
var formula = "" ;
var level = "3";
var prop_scope = "testimageformats";
var class_id = "x_72C696D513B40_001";
var prop_scopeCode = "72C696D513B40";
var isExternal = false;
new Property(myclass, code, version, items, domain, revision, pref_symbol, syn_symbols, det_class, definition, note, remark, condition, dates, source_doc, figure, formula, level, prop_scope, class_id, prop_scopeCode, isExternal );

// property definition : x.72C696D513B40-001.72C696E471FDD-001

var code = "72C696E471FDD";
var version = "001";
var revision = "001" ;
var items = new Item_names("dxf", "", "", new Array());
var domain = new Domain( "String", new Array("X 17")); 
var pref_symbol = "" ; 
var syn_symbols = new Array() ; 
var det_class = "" ; 
var definition = "Not defined..."; 
var note = ""; 
var remark = ""; 
var condition = new Array() ; 
var dates = new Dates("", "", "");
var source_doc ="" ;
var figure ="../external-items/DIR0000/FILE_0000_0010.dxf" ;
var formula = "" ;
var level = "3";
var prop_scope = "testimageformats";
var class_id = "x_72C696D513B40_001";
var prop_scopeCode = "72C696D513B40";
var isExternal = false;
new Property(myclass, code, version, items, domain, revision, pref_symbol, syn_symbols, det_class, definition, note, remark, condition, dates, source_doc, figure, formula, level, prop_scope, class_id, prop_scopeCode, isExternal );

var the_family = new dictionaryFamily(myclass, "en", self);
