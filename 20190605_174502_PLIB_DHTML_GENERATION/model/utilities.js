//--------------------------------------------------------------------------//
/*              plib utilities
 * Copyright (c) 1999 CRCFAO. All Rights Reserved.
 * Permission to use, copy, modify, and distribute this software
 * and its documentation without prior written permission
 * from LISI/ENSMA is not allowed.
 *
 * T�l�port 2, 1 avenue Cl�ment Ader , BP40109
 *        86960 FUTUROSCOPE
 *
 */

 var bkgdimg='fond1.jpg';

//--------------------------------------------------------------------------//
//        Fonction d'extraction des param�tres
//--------------------------------------------------------------------------//

 function extract_params_url() {
	url = window.location.href;
	var exp=new RegExp("[&?]+","g");
	var exp2=new RegExp("[=]+","g");
	var tabNom=url.split(exp);
	var	tabParam=new Array();
	if (tabNom!=null) {
		for (var i=1;i<tabNom.length;i++){
			var tabTemp=tabNom[i].split(exp2);
			tabParam[tabTemp[0]]=tabTemp[1];
		}
	}
	return tabParam;
 }
	
//--------------------------------------------------------------------------//
//        Fonction de chargement de donn�es
//--------------------------------------------------------------------------//


function load_script_datas(family,extension) {
	if (extension=="") {
		document.write("<SCRIPT language='JavaScript' src='../"+family+".js'>");
	} else {
		document.write("<SCRIPT language='JavaScript' src='../"+family+"_"+extension+".js'>");
	}
	document.write("</SCRIPT>");
}
	
//--------------------------------------------------------------------------//
//        Fonction de creation de frame set
//--------------------------------------------------------------------------//

 function load_frameset_with_content(row,family, langue, link) {
	document.write('<FRAMESET ROWS="'+row+'%,*">');
	document.write('  <FRAME SRC="dictionary.html?family='+family+'"  NAME="dico">');
	document.write('  <FRAME SRC="content.html?family='+family+'&Withlink='+link+'"  NAME="content">');
	document.write('</FRAMESET>');
	}


//--------------------------------------------------------------------------//
function load_frameset_with_dictionary(family, langue) {
	document.write('<FRAMESET ROWS="100%,*">');
	document.write('  <FRAME SRC="dictionary.html?family='+family+'"  NAME="dico">');
	document.write('  <FRAME SRC="content_empty.html" NAME="content">');
	document.write('</FRAMESET>');
	}

//--------------------------------------------------------------------------//

function check_is_loaded(){
//	la page family.html met � true Is_load.text qd elle est charg�e
	if (!top.heading.is_loaded.value) {
		setTimeout('check_is_loaded()',200);
	} else {
		top.frames['search'].location=top.frames['search'].document.location;
	}
}
//--------------------------------------------------------------------------//

function switch_language(lang){
	languages.set_language(lang);

	if (!top.filter.is_loaded) {
//			top.history.go(0);
		// Mise � jour de la frame d'ent�te
		top.frames['heading'].location=top.frames['heading'].document.location;
		// Rechargement des donn�es dans la frame principale
		top.frames['text'].location=top.frames['text'].document.location;
		// Mise � jour de l'arbre de s�lection
		top.frames['code'].MTMDisplayMenu();
		// Mise � jour de la frame des services (exporter dico)
		top.frames['search'].location=top.frames['search'].document.location;

	} else {
//		window.is_loaded.value=false;
		// la description de la famille doit d'abord etre charg�e
		// dans la nouvelle langue pour pouvoir recreer le moteur de recherche
		top.frames['text'].location=top.frames['text'].document.location;
		// une fois ceci fait, on recharge les frames restantes
		top.frames['search'].location=top.frames['search'].document.location;
//			setTimeout('check_is_loaded()',200);

	}
}

//--------------------------------------------------------------------------//
function apply_constraints()
{
	top.filter.apply_constraints();
}

//--------------------------------------------------------------------------//
function reset_constraints()
{
	top.filter.reset_constraints();

}

//--------------------------------------------------------------------------//
function HTML_contact() {

    var file = "./contacter.html"
//    alert(" file loaded :"+ file);
    var winFeatures = "width=600,height=250,resizable=yes,menubar=yes,scrollbars=no";
    this.win=window.open(file,"Contacts",winFeatures);
}
//--------------------------------------------------------------------------//

function HTML_choice() {
    var langue = languages.get_language();
    var file = "./pagaie_"+langue+".html"
//    alert(" file loaded :"+ file);
    var winFeatures = "width=900,height=550,resizable=yes,menubar=yes,scrollbars=yes";
    this.win=window.open(file,"Choice",winFeatures);
}
//--------------------------------------------------------------------------//
function HTML_kind() {
    var langue = languages.get_language();
    var file = "./rivieres_"+langue+".html"
//    alert(" file loaded :"+ file);
    var winFeatures = "width=900,height=550,resizable=yes,menubar=yes,scrollbars=yes";
    this.win=window.open(file,"Kind",winFeatures);
}